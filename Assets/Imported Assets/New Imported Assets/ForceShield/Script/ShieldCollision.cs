﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldCollision : MonoBehaviour
{

    [SerializeField] string[] _collisionTag;
    float hitTime;
    Material mat;

    private float pushForce;
    private Vector3 pushDir;
    public float gravity = 10.0f;

    public float force = 10f; //Force 10000f
    public float stunTime = 0.5f;
    private Vector3 hitDir;

    void Start()
    {
        if (GetComponent<Renderer>())
        {
            mat = GetComponent<Renderer>().sharedMaterial;
        }

    }

    void Update()
    {

        if (hitTime > 0)
        {
            float myTime = Time.fixedDeltaTime * 1000;
            hitTime -= myTime;
            if (hitTime < 0)
            {
                hitTime = 0;
            }
            mat.SetFloat("_HitTime", hitTime);
        }

    }

    void OnCollisionEnter(Collision collision)
    {
        for (int i = 0; i < _collisionTag.Length; i++)
        {

            if (_collisionTag.Length > 0 || collision.transform.CompareTag(_collisionTag[i]))
            {
                //Debug.Log("hit");
                ContactPoint[] _contacts = collision.contacts;
                for (int i2 = 0; i2 < _contacts.Length; i2++)
                {
                    mat.SetVector("_HitPosition", transform.InverseTransformPoint(_contacts[i2].point));
                    hitTime = 500;
                    mat.SetFloat("_HitTime", hitTime);

                    //hitDir = _contacts[i2].normal;

                    ////HitPlayer(-hitDir * force, stunTime);

                    //collision.gameObject.GetComponent<Rigidbody>().velocity = -hitDir * force;

                    //pushForce = (-hitDir * force).magnitude;
                    //pushDir = Vector3.Normalize((-hitDir * force));
                    //StartCoroutine(Decrease((-hitDir * force).magnitude, stunTime, collision));
                    //return;
                }
            }
        }
    }

    //public void HitPlayer(Vector3 velocityF, float time)
    //{
    //    rb.velocity = velocityF;

    //    pushForce = velocityF.magnitude;
    //    pushDir = Vector3.Normalize(velocityF);
    //    StartCoroutine(Decrease(velocityF.magnitude, time));
    //}

    //private IEnumerator Decrease(float value, float duration, Collision rb)
    //{
    //    float delta = 0;
    //    delta = value / duration;

    //    for (float t = 0; t < duration; t += Time.deltaTime)
    //    {
    //        yield return null;
    //        //if (!slide) //Reduce the force if the ground isnt slide
    //        //{
    //            pushForce = pushForce - Time.deltaTime * delta;
    //            pushForce = pushForce < 0 ? 0 : pushForce;
    //            //Debug.Log(pushForce);
    //        //}
    //        rb.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, -gravity * GetComponent<Rigidbody>().mass, 0)); //Add gravity
    //    }

    //}
}

