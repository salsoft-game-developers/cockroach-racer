﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class RewardedAd : MonoBehaviour, IUnityAdsLoadListener, IUnityAdsShowListener
{
    //public Button _showAdButton;
    
    [SerializeField] string _androidAdUnitId = "Rewarded_Android";
    [SerializeField] string _iOSAdUnitId = "Rewarded_iOS";
    string _adUnitId = null;

    // Get the Ad Unit ID for the current platform:
    // This will remain null for unsupported platforms

    void Awake()
    {
#if UNITY_IOS
		    _adUnitId = _iOSAdUnitId;
#elif UNITY_ANDROID
        _adUnitId = _androidAdUnitId;
#endif
        //Disable button until ad is ready to show
        //_showAdButton.interactable = false;

    }

    private void Start()
    {
        //_showAdButton.gameObject.SetActive(false);

        if (AdsManager.Instance.adsDisabledIndex == 1)
        {
            AdsManager.Instance.adsDisabled = true;
            //_showAdButton.gameObject.SetActive(false);
        }
        else 
        { 
            AdsManager.Instance.adsDisabled = false; 
        }
        
        LoadAd();
    }

    // Load content to the Ad Unit:
    public void LoadAd()
    {
        
        if (AdsManager.Instance.adsDisabled == false)
        {
            // IMPORTANT! Only load content AFTER initialization (in this example, initialization is handled in a different script).
            Debug.Log("Loading Ad: " + _adUnitId);
            Advertisement.Load(_adUnitId, this);
        }
        else
        {
            return;
        }
    }

    // If the ad successfully loads, add a listener to the button and enable it:
    public void OnUnityAdsAdLoaded(string adUnitId)
    {
        Debug.Log("Ad Loaded: " + adUnitId);

        if (adUnitId.Equals(_adUnitId))
        {
            // Configure the button to call the ShowAd() method when clicked:
            //_showAdButton.onClick.AddListener(ShowRewardedAd);
            // Enable the button for users to click:
            //_showAdButton.interactable = true;
            //_showAdButton.gameObject.SetActive(true);

        }
    }

    // Implement a method to execute when the user clicks the button.
    public void ShowRewardedAd()
    {
        if (AdsManager.Instance.adsDisabled == false)
        {
            // Disable the button: 
            //_showAdButton.interactable = false;
            //_showAdButton.gameObject.SetActive(false);
            // Then show the ad:
            Advertisement.Show(_adUnitId, this);
            LoadAd();
            Debug.Log("Rewarded ad show");

            UIManager.Instance.reviveBtn.SetActive(false);
        }
        else
        {
            return;
        }
    }
    
    // Implement the Show Listener's OnUnityAdsShowComplete callback method to determine if the user gets a reward:
    public void OnUnityAdsShowComplete(string adUnitId, UnityAdsShowCompletionState showCompletionState)
    {
        Debug.Log("Unity Ads Rewarded Ad before Completed");
        if (adUnitId.Equals(_adUnitId) && showCompletionState.Equals(UnityAdsShowCompletionState.COMPLETED))
        {
            Debug.Log("Unity Ads Rewarded Ad Completed");
            // Grant a reward.

            //UIManager.Instance.RewardText.text = "+ $" + UIManager.Instance.GameOverCash.ToString("0");

            //UIManager.Instance.GameOverCash += UIManager.Instance.GameOverCash;
            
            // TempScore = UIManager.Instance.GameOverCash;
            // TempScore *= 2;

            //UIManager.Instance.cashTxt.text = "$" + UIManager.Instance.GameOverCash.ToString("0");
            //UIManager.Instance.rewardAnimation.SetTrigger("Reward");
            //UIManager.Instance.totalCash = UIManager.Instance.totalCash + (UIManager.Instance.GameOverCash / 2);
            
            //UIManager.Instance.totalMoney = UIManager.Instance.totalMoney + 100;

            //PlayerPrefs.SetFloat("Money", UIManager.Instance.totalMoney);
            //UIManager.Instance.UpdateAllCoinsUIText();
            
            //SoundManager.Instance.buySound.Play();

            Debug.Log("Rewarded Ad");

            //Reviving Reward
            //GameManager.Instance.collisionDetectorScript.ReviveBtnPressed();

            GameManager.Instance.isReviveBtnPressed = true;
            // Load another ad:
            Advertisement.Load(_adUnitId, this);
        }
    }

    // Implement Load and Show Listener error callbacks:
    public void OnUnityAdsFailedToLoad(string adUnitId, UnityAdsLoadError error, string message)
    {
        Debug.Log($"Error loading Ad Unit {adUnitId}: {error.ToString()} - {message}");
        // Use the error details to determine whether to try to load another ad.
    }

    public void OnUnityAdsShowFailure(string adUnitId, UnityAdsShowError error, string message)
    {
        Debug.Log($"Error showing Ad Unit {adUnitId}: {error.ToString()} - {message}");
        // Use the error details to determine whether to try to load another ad.
    }

    public void OnUnityAdsShowStart(string adUnitId)
    {
        Debug.Log("start ad");
    }
    public void OnUnityAdsShowClick(string adUnitId) { }

    void OnDestroy()
    {
        // Clean up the button listeners:
        //_showAdButton.onClick.RemoveAllListeners();
        //UIManager.Instance.reviveBtn.GetComponent<Button>().onClick.RemoveAllListeners();

    }
}

