﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour
{
    [SerializeField] string _androidGameId;
    [SerializeField] string _iOSGameId;
    
    [SerializeField] bool _testMode = true;

    public BannerAd bannerScript;
    public InterstitialAd interstitialScript;
    public RewardedAd rewardedScript;

    public bool adsDisabled;
    
    public int adsDisabledIndex;
    
    private string _gameId;

    #region Singlton:AdsManager

    public static AdsManager Instance;

    void Awake()
    {
        adsDisabledIndex =  PlayerPrefs.GetInt("Ads Removed", 0);
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        if(adsDisabledIndex == 1) { adsDisabled = true; } else { adsDisabled = false; }

        if(adsDisabled == false) { InitializeAds(); }
        
    }

    #endregion

    //void Awake()
    //{
    //    InitializeAds();
    //}

    public void InitializeAds()
    {
        //_gameId = (Application.platform == RuntimePlatform.IPhonePlayer)
        //    ? _iOSGameId
        //    : _androidGameId;
        //Advertisement.Initialize(_gameId, _testMode);

#if UNITY_IOS
        _gameId = _iOSGameId;
#elif UNITY_ANDROID
        _gameId = _androidGameId;
#endif
        Advertisement.Initialize(_gameId, _testMode);
    }

    public void OnInitializationComplete()
    {
        Debug.Log("Unity Ads initialization complete.");
    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        Debug.Log($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
    }
}

