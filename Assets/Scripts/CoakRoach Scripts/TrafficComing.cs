﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficComing : MonoBehaviour
{
    public float speed = 1.0f;
    public float damping = 6.0f;
    
    public int current;
    
    public bool rightLane, isTrain;

    public Rigidbody rb;

    public GameObject comingWayPoint;
    
    public List<GameObject> comingWayPoints;

    public BikeControl bikeScript;
    
    void Start()
    {
        bikeScript = GameManager.Instance.playerGameObject.GetComponent<BikeControl>();

        if (rightLane == true)
        {
            comingWayPoint = GameObject.FindGameObjectWithTag("ComingRight");
        }
        else if (rightLane == false && !isTrain)
        {
            comingWayPoint = GameObject.FindGameObjectWithTag("ComingLeft");
        }
        else if (isTrain)
        {
            comingWayPoints.Add(GameObject.FindGameObjectWithTag("ComingRight"));
            comingWayPoints.Add(GameObject.FindGameObjectWithTag("ComingLeft"));
            comingWayPoints.Add(GameObject.FindGameObjectWithTag("ComingNewRight"));
            comingWayPoints.Add(GameObject.FindGameObjectWithTag("ComingNewLeft"));

            for(int i = 0; i < comingWayPoints.Count; i++)
            {
                if (this.transform.position.x == comingWayPoints[i].transform.position.x)
                {
                    comingWayPoint = comingWayPoints[i];
                }
            }
            
        }

        //scraped
        /*if (GameManager.Instance.trafficGeneratorScript.enteredExpandedLane == false)
        {
            if (rightLane == true)
            {
                comingWayPoints = GameObject.FindGameObjectWithTag("ComingRight");
            }
            else if (rightLane == false)
            {
                comingWayPoints = GameObject.FindGameObjectWithTag("ComingLeft");
            }
        }
        else
        {
            if (rightLane == true)
            {
                comingWayPoints = GameObject.FindGameObjectWithTag("ComingRight");
            }
            else if (rightLane == false)
            {
                comingWayPoints = GameObject.FindGameObjectWithTag("ComingLeft");
            }
        }*/
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (GameManager.Instance.gamePlaying)
        //{
            if (transform.position != comingWayPoint.transform.position && GameManager.Instance.playerCrashed == false /* bikeScript.crash == false*/ /*&& !GameManager.Instance.playerInDiffuseBombSection*/)
            {
                Vector4 position = Vector3.MoveTowards(transform.position, comingWayPoint.transform.position, speed * Time.deltaTime);
                GetComponent<Rigidbody>().MovePosition(position);
                var rotation = Quaternion.LookRotation(comingWayPoint.transform.position - transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
            }
        //}
       
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("NPC") || collision.gameObject.CompareTag("NPC2") && GameManager.Instance.playerCrashed == false)
        {
            Destroy(this.gameObject);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Sewerage Start") /*|| other.CompareTag("Train Section Start") || (other.CompareTag("Train Section End") && this.CompareTag("NPC2"))*/)
        {
            Destroy(this.gameObject);
        }

        //Scrap
        /*if (other.CompareTag("ExpandLaneEnter"))
        {
            if (rightLane == true)
            {
                comingWayPoints = GameObject.FindGameObjectWithTag("ComingNewRight");
            }
            else if (rightLane == false)
            {
                comingWayPoints = GameObject.FindGameObjectWithTag("ComingNewLeft");
            }
        }
        
        if (other.CompareTag("ExpandLaneExit"))
        {
            if (rightLane == true)
            {
                comingWayPoints = GameObject.FindGameObjectWithTag("ComingRight");
            }
            else if (rightLane == false)
            {
                comingWayPoints = GameObject.FindGameObjectWithTag("ComingLeft");
            }
        }*/
    }

    /*public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("ExpandLaneExit"))
        {
            if (rightLane == true)
            {
                comingWayPoints = GameObject.FindGameObjectWithTag("ComingRight");
            }
            else if (rightLane == false)
            {
                comingWayPoints = GameObject.FindGameObjectWithTag("ComingLeft");
            }
        }
    }*/
}
