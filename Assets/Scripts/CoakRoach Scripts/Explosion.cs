﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Explosion : MonoBehaviour
{
    public Text statusTxt;
    public GameObject explosioneffect;
    public float radius = 5;
    public float force = 500;

    public bool explodeCar;
    float explosionTimer;
    GameObject player;

    public GameObject objectMaterialToChange;
    public Material initialMat;
    Material newMaterial;
    Material midMat;

    bool startBlink = true;
    bool startTimer = true;

    [HideInInspector]
    public bool stopBlinking;

    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.Instance.playerGameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(explodeCar == true && GameManager.Instance.playerInDiffuseBombSection && this.gameObject.CompareTag("NPC"))
        {
            if(startTimer == true)
            {
                explosionTimer = GameManager.Instance.bombTimer;
                GameManager.Instance.bombTimer = GameManager.Instance.bombTimer + 10f;
                startTimer = false;
            }
            //statusTxt.transform.parent.parent.LookAt(player.transform);

            if (explosionTimer < 0/*explosionTimer < 0*/)
            {
                this.Explode();
            }
            else
            {
                explosionTimer -= 1 * Time.deltaTime;
                statusTxt.text = "" + (int)explosionTimer;
                if(startBlink == true)
                {
                    StartCoroutine(BombBlinkIndicator());
                }
            }
        }
        
    }

    public void OnCollisionEnter(Collision collision)
    {
        //if (!collision.gameObject.CompareTag("HeroProjectile"))
        //{
        //   // Explode();

        //}

        if (!this.gameObject.CompareTag("NPC") && !GameManager.Instance.mosquitoTutorial)
        {
            Explode();
            if (collision.gameObject.CompareTag("NPC") || collision.gameObject.CompareTag("NPC2"))
            {
                if (collision.gameObject.GetComponent<TrafficComing>() != null)
                {
                    collision.gameObject.GetComponent<TrafficComing>().enabled = false;
                }
                else
                {
                    collision.gameObject.GetComponent<TrafficGoing>().enabled = false;
                }

                Destroy(collision.gameObject, 3f);
            }
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (/*GameManager.Instance.playerInDiffuseBombSection == false &&*/ this.gameObject.CompareTag("NPC") && other.CompareTag("DiffuseBombExit") && GameManager.Instance.playerInDiffuseBombSection)
        {
            statusTxt.text = null;
            stopBlinking = false;
            this.Explode();
        }
    }

    public void Explode()
    {
        GameObject explosion = Instantiate(explosioneffect, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach(Collider nearbyobjects in colliders)
        {
            Rigidbody rb = nearbyobjects.GetComponent<Rigidbody>();
            if(rb != null)
            {
                // explosion for Player
                if (rb.gameObject.CompareTag("Player " + GameManager.Instance.playerIndex) /*&& !rb.GetComponent<CollisionDetector>().hasShieldActivated*/)
                {
                    rb.GetComponent<CollisionDetector>().PlayerDeathByExplosion(force, transform, radius);
                }
                // normal explosion for environment
                else
                {
                    rb.AddExplosionForce(force, transform.position, radius);
                }
            }
        }

        Destroy(gameObject);
        Destroy(explosion, 2f);
    }


    // for exploding the mosquitos on it position without affecting the player.
    public void OnShootExplode()
    {
        GameObject explosion = Instantiate(explosioneffect, transform.position, transform.rotation);
        Destroy(gameObject);
        Destroy(explosion, 2f);
    }

    IEnumerator BombBlinkIndicator()
    {
        startBlink = false;
        if(!stopBlinking)
        {
            Color existngColor;
            existngColor = objectMaterialToChange.GetComponent<Renderer>().material.color;
            initialMat = objectMaterialToChange.GetComponent<Renderer>().material;
            objectMaterialToChange.GetComponent<Renderer>().material.color = Color.red;
            /*midMat = initialMat;
            initialMat = newMaterial;*/

            //this.GetComponent<Material>().color = Color.red;
            yield return new WaitForSeconds(1.5f);

            objectMaterialToChange.GetComponent<Renderer>().material = initialMat;
            objectMaterialToChange.GetComponent<Renderer>().material.color = existngColor;

            //this.GetComponent<Material>().color = Color.white;
            yield return new WaitForSeconds(1.5f);
            StartCoroutine(BombBlinkIndicator());
        }
        else
        {
            yield return null;
        }
        

    }

    
}
