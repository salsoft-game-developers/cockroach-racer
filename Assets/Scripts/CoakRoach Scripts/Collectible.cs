﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collectible : MonoBehaviour
{
    public enum Collectibles
    {
        Fuel = 0,
        Money = 1,
        Shield = 3,
        Headstart = 4
    }

    public Collectibles collectibles;
    
    public int collectibleAmount;

    bool showShieldTutorial = true;
    
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player " + GameManager.Instance.playerIndex))
        {
            SoundManager.Instance.collectibleSound.Play();

            if (this.collectibles == Collectibles.Fuel)
            {
                if (/*other.GetComponent<BikeControl>().powerShift*/ GameManager.Instance.fuelAmount < (100.00f - collectibleAmount))
                {
                    /*other.GetComponent<BikeControl>().powerShift*/ GameManager.Instance.fuelAmount += collectibleAmount;
                    UIManager.Instance.rewardText.GetComponent<Text>().text = "Got fuel X" + 1;

                }
                else
                {
                    UIManager.Instance.rewardText.GetComponent<Text>().text = "fuel is Maxed!!!";
                }
                UIManager.Instance.rewardText.GetComponent<Animator>().SetTrigger("Collectible");
            }
            else if (this.collectibles == Collectibles.Money)
            {
                if (UIManager.Instance.cashBagIcon.activeSelf == false)
                {
                    UIManager.Instance.cashBagIcon.SetActive(true);
                }
                
                UIManager.Instance.moneyInBags += collectibleAmount;
                
                GameManager.Instance.cashBagAmount++;
                
                UIManager.Instance.cashBagInGameTxt.text = "X" + GameManager.Instance.cashBagAmount.ToString("");

                UIManager.Instance.rewardText.GetComponent<Animator>().SetTrigger("Collectible");
                UIManager.Instance.rewardText.GetComponent<Text>().text = "Got MoneyBag X" + 1;

            }
            else if (this.collectibles == Collectibles.Shield)
            {
                GameManager.Instance.shieldAmount += collectibleAmount;
                if(UIManager.Instance.shieldIcon.activeSelf == false)
                {
                    UIManager.Instance.shieldIcon.SetActive(true);

                    if(showShieldTutorial == true && GameManager.Instance.shieldTutorial)
                    {
                        showShieldTutorial = false;
                        UIManager.Instance.shieldTutorialPnl.SetActive(true);
                        Time.timeScale = 0;
                    }
                }

                UIManager.Instance.shieldInGameTxt.text = "X" + GameManager.Instance.shieldAmount.ToString("");
                PlayerPrefs.SetInt("Shield", GameManager.Instance.shieldAmount);

                UIManager.Instance.rewardText.GetComponent<Animator>().SetTrigger("Collectible");
                UIManager.Instance.rewardText.GetComponent<Text>().text = "Got Shield X" + 1;

            }
            else if (this.collectibles == Collectibles.Headstart)
            {
                GameManager.Instance.headStartAmount += collectibleAmount;
                UIManager.Instance.headStartInGameTxt.text = "X" + GameManager.Instance.headStartAmount.ToString("");
                PlayerPrefs.SetInt("HeadStart", GameManager.Instance.headStartAmount);

                UIManager.Instance.rewardText.GetComponent<Animator>().SetTrigger("Collectible");
                UIManager.Instance.rewardText.GetComponent<Text>().text = "Got HeadStart X" + 1;
            }

            Destroy(this.gameObject);
        }
    }
}
