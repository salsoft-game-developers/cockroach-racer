﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public float smooth = 0.3f;
    public float distance = 5.0f;
    public float height = 1.0f;
    public float Angle = 20;

    public LayerMask lineOfSightMask = 0;

    public Transform HolderTarget;
    public Transform camTransform;
    public Transform BikerManRagdoll;
    public Transform BikerManAnimateddoll;

    public List<Transform> cameraSwitchView;

    public Vector3 Offset;

    public float SmoothTime = 0.3f;
    public float minusValue;
    public int Switch;

    private Vector3 velocity = Vector3.zero;
    private Vector3 temporaryVector;

    private Quaternion temporaryRotation;

    private BikeControl bikeScript;

    float cameraHeight;

    public void CameraSwitch()
    {
        Switch++;
        if (Switch > cameraSwitchView.Count) { Switch = 0; }
    }

    private void Start()
    {
        height = transform.position.y / 2;
        cameraHeight = height;

        temporaryVector = transform.position;
        temporaryRotation = transform.rotation;

        ChangeCameraTargets();

        //BikerMan = bikeScript.bikeSetting.bikerMan;
        
    }

    private void LateUpdate()
    {
        
        temporaryVector.z = HolderTarget.position.z - minusValue;
        
        temporaryVector.x = GameManager.Instance.playerGameObject.transform.position.x;
        
        transform.position = temporaryVector;
        transform.rotation = temporaryRotation;

        //Vector3 targetPosition = Target.position + Offset;
        //camTransform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);

        //if (!GameManager.Instance.gamePlaying)
        //{
        //    // policeSiren.Play();
        //    return;
        //}
        //else
        //{
        //    //update position
        //    //StartCoroutine(FocusDelay());
        //    Vector3 targetPosition = Target.position + Offset;
        //    camTransform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);

        //    //update rotation
        //    //transform.LookAt(Target);
        //}

        if (/*!bikeScript.crash*/ !GameManager.Instance.playerCrashed)
        {
            if (Switch == 0)
            {
                var direction = transform.rotation * -Vector3.forward;
                var targetDistance = AdjustLineOfSight(BikerManAnimateddoll.position + new Vector3(0, height, 0), direction);

                transform.position = BikerManAnimateddoll.position + new Vector3(0, height, 0) + direction * targetDistance;

                temporaryVector.x = transform.position.x;
                temporaryVector.y = transform.position.y;
                temporaryVector.z = HolderTarget.position.z - minusValue;
                transform.position = temporaryVector;

                

            }
            else
            {

                transform.position = cameraSwitchView[Switch - 1].position;
                transform.rotation = cameraSwitchView[Switch - 1].rotation;/*Quaternion.Lerp(transform.rotation, cameraSwitchView[Switch - 1].rotation, Time.deltaTime * 5.0f);*/

            }
        }
        else
        {
            /*HolderTarget = BikerManRagdoll;
            Vector3 look = BikerManRagdoll.position - transform.position;
            transform.rotation = Quaternion.LookRotation(look);*/

            BikerManRagdoll = GameObject.FindGameObjectWithTag("Ragdoll").transform.GetChild(4);
            HolderTarget = BikerManRagdoll;
            Vector3 look = BikerManRagdoll.position - transform.position;
            transform.rotation = Quaternion.LookRotation(look);
        }

    }

    public void ResetCameraPosition()
    {
        height = cameraHeight;

        HolderTarget = GameManager.Instance.playerHolder.transform;
        temporaryVector.z = HolderTarget.position.z - minusValue;
        temporaryVector.x = GameManager.Instance.playerGameObject.transform.position.x;


        transform.position = temporaryVector;
        transform.rotation = temporaryRotation;

    }

    public void ChangeCameraTargets()
    {
        //HolderTarget = GameObject.FindGameObjectWithTag("Player Holder").transform;
        
        HolderTarget = GameManager.Instance.playerHolder.transform;
        
        bikeScript = GameManager.Instance.playerGameObject.GetComponent<BikeControl>();
        //BikerManRagdoll = GameManager.Instance.Player().GetComponent<CollisionDetector>().ragdollModel.transform.GetChild(4);
        BikerManAnimateddoll = GameManager.Instance.playerGameObject.transform;
        cameraSwitchView = bikeScript.bikeSetting.cameraSwitchView;
    }

    //IEnumerator FocusDelay()
    //{
    //    yield return new WaitForSeconds(1f);
    //    Vector3 targetPosition = Target.position + Offset;
    //    camTransform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);

    //}

    float AdjustLineOfSight(Vector3 target, Vector3 direction)
    {

        RaycastHit hit;

        if (Physics.Raycast(target, direction, out hit, distance, lineOfSightMask.value))
            return hit.distance;
        else
            return distance;

    }
}
