﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneMovement : MonoBehaviour
{
    public GameObject bombPrefab;
    public ParticleSystem explosion;
    Transform bombPos;
    Transform bombPlacement;
    Transform playerPoint;

    public Transform[] wings;
    int wingsIndex;
    int speed = 20;
    GameObject bomb;


    //public GameObject[] trafficpoints;
    //public List<GameObject> trafficpoints;
    public List<Transform> trafficpoints = new List<Transform>();
    public int trafficIndex;

    // Start is called before the first frame update
    void Start()
    {
        bombPos = transform.GetChild(0);

        //error not instantiating boms
        bomb = Instantiate(bombPrefab, bombPos.position, Quaternion.identity, bombPos);
        bombPlacement = GameManager.Instance.bombPos;
        playerPoint = GameManager.Instance.playerPoint;

        //trafficpoints = GameObject.FindGameObjectsWithTag("NPC");

        /*if(GameManager.Instance.goingTrafficObjects.GetComponentsInChildren<GameObject>() != null)
        {
            trafficpoints.AddRange(GameManager.Instance.goingTrafficObjects.GetComponentsInChildren<GameObject>());
        }*/

        //foreach (Transform tr in GameManager.Instance.goingTrafficObjects.GetComponentsInChildren<Transform>()) trafficpoints.Add(tr);


        for(int i = 0; i < GameManager.Instance.goingTrafficObjects.transform.childCount; i++)
        {
            trafficpoints.Add(GameManager.Instance.goingTrafficObjects.transform.GetChild(i));
        }

    }

    // Update is called once per frame
    void Update()
    {
        RotateWings();

        if (GameManager.Instance.playerInSpeedSection)
        {
            TargetPlayer();
        }

        if (GameManager.Instance.playerInDiffuseBombSection)
        {
            TargetTraffic();
        }

    }

    private  void TargetTraffic()
    {
        if (trafficpoints[trafficIndex] == null && trafficIndex < trafficpoints.Count)
        {
            trafficIndex++;
        }
        else if (trafficIndex == trafficpoints.Count /*- 1*/)
        {
            explosion.Play();
            Destroy(this.gameObject);
        }

        if (transform.position != trafficpoints[trafficIndex]./*transform.*/position && trafficpoints[trafficIndex] != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, trafficpoints[trafficIndex]./*transform.*/position, 2.5f/*Random.Range(0.1f,0.59f)*/);

        }

        else if(transform.position == trafficpoints[trafficIndex]./*transform.*/position || trafficpoints[trafficIndex] == null)
        {
            //Destroy(trafficpoints[trafficIndex]);
            trafficIndex++;
        }

        if (trafficIndex == trafficpoints.Count /*- 1*/)
        {
            explosion.Play();
            Destroy(this.gameObject);
        }


    }

    public void TargetPlayer()
    {
        if (transform.position != playerPoint.transform.position && !GameManager.Instance.isPaused)
        {
            //Vector3 pos = Vector3.MoveTowards(transform.position, playerPoint[playerPointIndex].transform.position, speed /** Time.deltaTime*/);
            //transform.position = pos;

            transform.position = Vector3.MoveTowards(transform.position, playerPoint.transform.position, 0.5f/*Random.Range(0.1f,0.59f)*/);
            //var rotation = Quaternion.LookRotation(PlayerTransform.position - transform.position);
            //transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * speed);
        }

        if (transform.position == playerPoint.transform.position && !GameManager.Instance.isPaused)
        {
            //transform.SetParent(playerPoint);
            Debug.Log("droping bomb");

            //error not dropping any bombs 
            //bomb.GetComponent<Rigidbody>().isKinematic = false;

            bomb.transform.position = Vector3.MoveTowards(bomb.transform.position, bombPlacement.transform.position, 0.25f);
            //bomb.GetComponent<Rigidbody>().MovePosition(position);

        }

        if (bomb.transform.position == bombPlacement.position && !GameManager.Instance.isPaused)
        {
            bomb.transform.SetParent(bombPlacement);
            bomb.SetActive(false);
            //transform.position = Vector3.MoveTowards(transform.position, GameManager.Instance.mosquitoSpawner.mSpawnPoints[Random.Range(0, GameManager.Instance.mosquitoSpawner.mSpawnPoints.Length)].transform.position, 0.5f);
            explosion.Play();
            Destroy(gameObject, 1f);
        }
    }

    public void RotateWings()
    {
        if(wingsIndex < wings.Length)
        {
            wings[wingsIndex].transform.Rotate(0f, 0f, 100 * Time.deltaTime / 0.01f, Space.Self);
            wingsIndex++;
        }
        else
        {
            wingsIndex = 0;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("NPC"))
        {
            other.GetComponent<Explosion>().explodeCar = true; 
        }
    }

}
