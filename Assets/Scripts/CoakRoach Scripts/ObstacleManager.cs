﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    //[SerializeField] GameObject collectibleParent;

    public enum ObstacleSection
    {
        mosquitoSection,
        speedSection,
        diffuseSection,
        normalSection
    }

    public GameObject[] childrenObstacles;
    public ObstacleSection obstacleSection;

    // Start is called before the first frame update
    void Start()
    {
        if ((GameManager.Instance.speedTutorial && obstacleSection == ObstacleSection.speedSection) 
            || (GameManager.Instance.diffuseTutorial && obstacleSection == ObstacleSection.diffuseSection) 
            || (GameManager.Instance.mosquitoTutorial && GameManager.Instance.playerInSewerage && obstacleSection == ObstacleSection.mosquitoSection) 
            || (GameManager.Instance.mosquitoTutorial && obstacleSection == ObstacleSection.mosquitoSection))
        {
            return;
        }
        else
        {
            childrenObstacles = new GameObject[transform.childCount];
            for (int i = 0; i < transform.childCount; i++)
            {
                childrenObstacles[i] = transform.GetChild(i).gameObject;
            }

            for (int i = 0; i < childrenObstacles.Length; i++)
            {
                int index = Random.Range(0, childrenObstacles.Length);
                if (!childrenObstacles[index].activeSelf)
                {
                    childrenObstacles[index].SetActive(true);
                }

            }
        }
        
    }
}
