﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MosquitoMovement : MonoBehaviour
{

    public GameObject[] playerPoint;
    public float speed;

    public GameObject bombPrefab;
    public Transform placementOfBomb;

    Transform PlayerTransform;
    public int playerPointIndex = 0;

    public GameObject bomb;
    bool hasBombInstantiated;

    //Start is called before the first frame update
    void Start()
    {
        PlayerTransform = GameManager.Instance.playerGameObject.transform;

        //playerPoint = GameObject.FindGameObjectsWithTag("PlayerPoint");
        
        playerPoint = GameManager.Instance.mosquitoSpawner.playerpoints;
        playerPointIndex = Random.Range(0, playerPoint.Length);

        StartCoroutine(DestroyMosquito());
    }

    // Update is called once per frame
    void Update()
    {
        speed = PlayerTransform.GetComponent<BikeControl>().speed * Time.deltaTime;
       
        if(GameManager.Instance.mosquitoSpawn == true)
        {
            if (transform.position != playerPoint[playerPointIndex].transform.position)
            {
                //Vector3 pos = Vector3.MoveTowards(transform.position, playerPoint[playerPointIndex].transform.position, speed /** Time.deltaTime*/);
                //transform.position = pos;

                transform.position = Vector3.MoveTowards(transform.position, playerPoint[playerPointIndex].transform.position, 0.25f/*Random.Range(0.1f,0.59f)*/);
                var rotation = Quaternion.LookRotation(PlayerTransform.position - transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * speed);
            }

            else if (transform.position == playerPoint[playerPointIndex].transform.position)
            {
                playerPointIndex = Random.Range(0, playerPoint.Length);
            }

            if (hasBombInstantiated == false)
            {
                transform.GetComponentInChildren<Animator>().SetBool("HasBomb", false);

                bomb = Instantiate(bombPrefab, placementOfBomb.position, Quaternion.identity, placementOfBomb);
                bomb.GetComponent<Rigidbody>().isKinematic = true;
                hasBombInstantiated = true;
            }

            else if( bomb == null)
            {
                //hasBombInstantiated = false;
                transform.GetComponentInChildren<Animator>().SetBool("HasBomb", false);

                transform.position = Vector3.MoveTowards(transform.position, GameManager.Instance.mosquitoSpawner.mSpawnPoints[playerPointIndex].transform.position, 1f);
                var rotation = Quaternion.LookRotation(GameManager.Instance.mosquitoSpawner.mSpawnPoints[playerPointIndex].transform.position - transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * speed);
            }
        }

        else if (GameManager.Instance.mosquitoSpawn == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, GameManager.Instance.mosquitoSpawner.mSpawnPoints[playerPointIndex].transform.position, 1f);
            var rotation = Quaternion.LookRotation(GameManager.Instance.mosquitoSpawner.mSpawnPoints[playerPointIndex].transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * speed);
            
        }

        //if(mosquitoDestroy == true) { DestroyMosquito(); mosquitoDestroy = false; }

    }

    //void OnMouseDown()
    //{
    //    if (EventSystem.current.IsPointerOverGameObject())
    //        return;
    //    Destroy(this.gameObject);
    //    //GameManager.Instance.mosquitoSpawner.spawnedMosquitos.Remove(GameManager.Instance.mosquitoSpawner.spawnedMosquitos[GameManager.Instance.mosquitoSpawner.mosquitoIndex]);
    //    GameManager.Instance.mosquitoSpawner.spawnedMosquitos.Remove(gameObject.name);

    //}

    IEnumerator DestroyMosquito()
    {
        //if(hasBombInstantiated == false)
        //{
        //    bomb = Instantiate(sphere, placementOfBomb.position, Quaternion.identity);
        //    bomb.GetComponent<Rigidbody>().isKinematic = true;
        //    hasBombInstantiated = true;
        //}

        if (transform.position == GameManager.Instance.mosquitoSpawner.mSpawnPoints[playerPointIndex].transform.position || /*PlayerTransform.GetComponent<BikeControl>().crash*/ GameManager.Instance.playerCrashed)
        {
            Destroy(this.gameObject);
            GameManager.Instance.mosquitoSpawner.spawnedMosquitos.Remove(gameObject.name);
        }

        yield return new WaitForSeconds(1f);
        StartCoroutine(DestroyMosquito());

        //if (transform.position == GameManager.Instance.mosquitoSpawner.mSpawnPoints[playerPointIndex].transform.position)
        //{
        //    Destroy(this.gameObject);
        //    GameManager.Instance.mosquitoSpawner.spawnedMosquitos.Remove(gameObject.name);
        //}
        //else 
        //{

        //    yield return null;
        //}

        //StartCoroutine(DestroyMosquito());


    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player " + GameManager.Instance.playerIndex) && hasBombInstantiated == true && /*!PlayerTransform.GetComponent<BikeControl>().crash*/ !GameManager.Instance.playerCrashed && !GameManager.Instance.mosquitoTutorial)
        {
            //Attack Player
            //GameObject mosquitoProjectile = Instantiate(sphere, transform.position, Quaternion.identity);
            //mosquitoProjectile.GetComponent<Rigidbody>().AddForce(-Vector3.forward * 3000, ForceMode.Force);
            //Destroy(mosquitoProjectile,3f);
            //instOb.transform.position = Vector3.MoveTowards(transform.position, /*PlayerTransform.transform.position*/ new Vector3(PlayerTransform.transform.position.x, PlayerTransform.transform.position.y,-100f), 1f);

            transform.GetComponentInChildren<Animator>().SetTrigger("DropBomb");
            if (bomb != null)
            {
                bomb.transform.SetParent(transform.parent);
                bomb.GetComponent<Rigidbody>().isKinematic = false;
            }
        }

        if (other.gameObject.CompareTag("HeroProjectile"))
        {
            //OnMosquitoShot();
        }
    }

    public void OnMosquitoShot()
    {
        Debug.Log("mosquito killed");
        //transform.GetComponentInChildren<Animator>().SetTrigger("Death");
        //transform.gameObject.AddComponent<Rigidbody>();
        if(bomb != null)
        {
            GameManager.Instance.mosquitoSpawner.spawnedMosquitos.Remove(gameObject.name);

            if (GameManager.Instance.mosquitoTutorial)
            {
                Debug.Log("mosquito has been shot inside  tutorials");
                //StartCoroutine(GameManager.Instance.mosquitoSpawner.EndTutorial());
                GameManager.Instance.mosquitoSpawner.EndTutorial();
            }

            bomb.GetComponent<Explosion>().OnShootExplode();
            Destroy(gameObject);
        }
    }

    /*IEnumerator EndTutorial()
    {
        Time.timeScale = 1;
        
        yield return new WaitForSeconds(1f);
        
        GameManager.Instance.mosquitoTutorial = false;
        GameManager.Instance.mosquitoTutorialIndex = 1;

        PlayerPrefs.SetInt("Mosquito Tutorial", GameManager.Instance.mosquitoTutorialIndex);

        Debug.Log("am i called after killing mosquito");
    }*/

}
