﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficGoing : MonoBehaviour
{
    public float speed = 1.0f;
    public float damping = 6.0f;
    
    public int current;

    public bool rightLane;
    public bool enteredExpandedLane, enteredNonTrafficSection, enterOnce = true;

    public Rigidbody rb;

    public GameObject goingWayPoint;

    public BikeControl bikeScript;

    void Start()
    {
        bikeScript = GameManager.Instance.playerGameObject.GetComponent<BikeControl>();
        
        if (rightLane == true)
        {
            goingWayPoint = GameObject.FindGameObjectWithTag("GoingRight");
        }
        else if (rightLane == false)
        {
            goingWayPoint = GameObject.FindGameObjectWithTag("GoingLeft");
        }
        
        //Scraped
        /*if (GameManager.Instance.trafficGeneratorScript.enteredExpandedLane == false)
        {
            if (rightLane == true)
            {
                goingWayPoint = GameObject.FindGameObjectWithTag("GoingRight");
            }
            else if (rightLane == false)
            {
                goingWayPoint = GameObject.FindGameObjectWithTag("GoingLeft");
            }
        }
        else
        {
            if (rightLane == true)
            {
                goingWayPoint = GameObject.FindGameObjectWithTag("GoingNewRight");
            }
            else if (rightLane == false)
            {
                goingWayPoint = GameObject.FindGameObjectWithTag("GoingNewLeft");
            }
        }*/
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (GameManager.Instance.gamePlaying)
        //{
            if (transform.position != goingWayPoint.transform.position && GameManager.Instance.playerCrashed == false && !enteredNonTrafficSection/*bikeScript.crash == false*/ /*&& !enteredExpandedLane*/)
            {
                Vector4 position = Vector3.MoveTowards(transform.position, goingWayPoint.transform.position, speed * Time.deltaTime);
                GetComponent<Rigidbody>().MovePosition(position);
                var rotation = Quaternion.LookRotation(goingWayPoint.transform.position - transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
            }

            /*if (enteredExpandedLane)
            {
                enteredExpandedLane = false;

            }*/
        //}

    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("NPC") || collision.gameObject.CompareTag("NPC2") && GameManager.Instance.playerCrashed == false)
        {
            Destroy(this.gameObject);
        }
    }

    public void OnTriggerEnter(Collider other)
    {

        //Scraped
        /*if (other.CompareTag("ExpandLaneEnter"))
        {
            if (rightLane == true)
            {
                goingWayPoint = GameObject.FindGameObjectWithTag("GoingNewRight");
            }
            else if (rightLane == false)
            {
                goingWayPoint = GameObject.FindGameObjectWithTag("GoingNewLeft");
            }
        }
        
        if (other.CompareTag("ExpandLaneExit"))
        {
            if (rightLane == true)
            {
                goingWayPoint = GameObject.FindGameObjectWithTag("GoingRight");
            }
            else if (rightLane == false)
            {
                goingWayPoint = GameObject.FindGameObjectWithTag("GoingLeft");
            }
        }*/

        if (other.CompareTag("Sewerage Start") || other.CompareTag("Train Path"))
        {
            enteredNonTrafficSection = true;

            rb.isKinematic = true;
            rb.useGravity = false;

            if (enterOnce)
            {
                enterOnce = false;

                if (rightLane)
                {
                    GameObject animatedTraffic = Instantiate(GameManager.Instance.rightLaneForwardPrefab, transform.position, Quaternion.identity, other.transform);
                    transform.SetParent(animatedTraffic.transform);

                    animatedTraffic.GetComponent<Animator>().SetTrigger(other.tag);
                    //Destroy(animatedTraffic, 2);


                }

                else if (!rightLane)
                {
                    GameObject animatedTraffic = Instantiate(GameManager.Instance.leftLaneForwardPrefab, transform.position, Quaternion.identity, other.transform);
                    transform.SetParent(animatedTraffic.transform);

                    animatedTraffic.GetComponent<Animator>().SetTrigger(other.tag);
                    //Destroy(animatedTraffic, 2);

                }
            }
        }
    }

}
