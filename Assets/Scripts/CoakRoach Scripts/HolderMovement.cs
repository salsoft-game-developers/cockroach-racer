﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolderMovement : MonoBehaviour
{
    [SerializeField] BikeControl bikeControlScript;

    Vector3 temporaryVector;

    public Transform bike;

    public Transform bikerMan;
   
    //public Transform newBackRightTarget, newBackLeftTarget, newFrontRightTarget, newFrontLeftTarget;

    //Transform BackRightTarget, BackLeftTarget, FrontRightTarget, FrontLeftTarget;
    //Transform preBackRightTarget, preBackLeftTarget, preFrontRightTarget, preFrontLeftTarget;

    // Start is called before the first frame update
    void Start()
    {

        ResetHolderTarget();

        //Scraped
        /*BackRightTarget = GameObject.FindGameObjectWithTag("ComingRight").transform;
        BackLeftTarget = GameObject.FindGameObjectWithTag("ComingLeft").transform;
        FrontRightTarget = GameObject.FindGameObjectWithTag("GoingRight").transform;
        FrontLeftTarget = GameObject.FindGameObjectWithTag("GoingRight").transform;

        preBackRightTarget = BackRightTarget;
        preBackLeftTarget = BackLeftTarget;
        preFrontRightTarget = FrontRightTarget;
        preFrontLeftTarget = FrontLeftTarget;*/
    }

    
    void LateUpdate()
    {
        if (/*!bikeControlScript.crash*/ !GameManager.Instance.playerCrashed)
        {
            temporaryVector.x = transform.position.x;
            temporaryVector.y = transform.position.y;
            temporaryVector.z = bike.position.z;
            transform.position = temporaryVector;
        }
        else
        {
            transform.position = bikerMan.position;
        }
        

        //transform.position = new Vector3(transform.position.x, transform.position.y, biker.position.z);
        
    }

    public void ResetPosition()
    {
        temporaryVector.x = 0;
        temporaryVector.y = 0;
        temporaryVector.z = bike.position.z;
        transform.position = temporaryVector;
    }

    public void OnTriggerEnter(Collider other)
    {
        //Scraped
        /*if (other.CompareTag("ExpandLaneEnter"))
        {
            BackRightTarget.position = new Vector3(newBackRightTarget.position.x, BackRightTarget.position.y, BackRightTarget.position.z);
            BackLeftTarget.position = new Vector3(newBackLeftTarget.position.x, BackLeftTarget.position.y, BackLeftTarget.position.z);
            FrontRightTarget.position = new Vector3(newFrontRightTarget.position.x, FrontRightTarget.position.y, FrontRightTarget.position.z);
            FrontLeftTarget.position = new Vector3(newFrontLeftTarget.position.x, FrontLeftTarget.position.y, FrontLeftTarget.position.z);
        }*/
    }
    public void OnTriggerExit(Collider other)
    {
        //Scraped
        /*if (other.CompareTag("ExpandLaneExit"))
        {
            BackRightTarget.position = new Vector3(preBackRightTarget.position.x, BackRightTarget.position.y, BackRightTarget.position.z);
            BackLeftTarget.position = new Vector3(preBackLeftTarget.position.x, BackLeftTarget.position.y, BackLeftTarget.position.z);
            FrontRightTarget.position = new Vector3(preFrontRightTarget.position.x, FrontRightTarget.position.y, FrontRightTarget.position.z);
            FrontLeftTarget.position = new Vector3(preFrontLeftTarget.position.x, FrontLeftTarget.position.y, FrontLeftTarget.position.z);
        }*/
    }

    public void ResetHolderTarget()
    {
        bike = GameManager.Instance.playerGameObject.transform;
        bikeControlScript = bike.GetComponent<BikeControl>();

    }
}
