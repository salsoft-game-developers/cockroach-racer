﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTrigger : MonoBehaviour
{

	//GameManager manager;

	public GameObject bikeParent;
	// Score given to player
	public int awardedScore = 100;
	// target speed of player to earn score
	public float scoreSpeed = 43f;
	
	bool entered;
	float diffuseTimer = 3f;


	
	void Start()
	{
		entered = false;
		//manager = GameObject.FindObjectOfType<GameManager>();
	}

	// just give one time score
	
	float scoreTemp;

	void OnTriggerEnter(Collider other)
	{

		if (!entered && !GameManager.Instance.playerInDiffuseBombSection)
		{
			if (other.gameObject.CompareTag("BikeCollider"))
			{
				//bikeParent = other.gameObject.GetComponentInParent<Transform>().gameObject.GetComponentInParent<Transform>();

				bikeParent = other.transform.parent.parent.gameObject;
				
				Debug.Log("Player " + GameManager.Instance.playerIndex);
				if (bikeParent.GetComponent<BikeControl>().speed >= scoreSpeed)
				{
					//Debug.Log("inside if");

					if (bikeParent.GetComponent<BikeControl>().Wheelie > 0)
                    {
						//scoreTemp = awardedScore + ((int)bikeParent.GetComponent<BikeControl>().score) * 4;
						scoreTemp = awardedScore /*+ (UIManager.Instance.score * 4)*/;
					}
                    else 
					{
						Debug.Log("inside else");
						//scoreTemp = awardedScore + ((int)bikeParent.GetComponent<BikeControl>().score) * 2;
						scoreTemp = awardedScore /*+ (UIManager.Instance.score * 2)*/;
					}

					UIManager.Instance.rewardText.GetComponent<Animator>().SetTrigger("Collectible");
					UIManager.Instance.rewardText.GetComponent<Text>().text = "Bonus " + scoreTemp.ToString("");

					//manager.AddScore(scoreTemp);
					entered = true;
					//other.GetComponent<CollisionController>().SpawnScorePrefab(scoreTemp);
					UIManager.Instance.AddScore(scoreTemp);
				}
			}
		}
	}

    public void OnTriggerStay(Collider other)
    {
        if (this.transform.parent.CompareTag("NPC") && other.CompareTag("Player " + GameManager.Instance.playerIndex))
        {
			if (this.GetComponentInParent<Explosion>().explodeCar && GameManager.Instance.playerInDiffuseBombSection)
			{
				diffuseTimer -= 1 * Time.deltaTime;
				if (diffuseTimer < 0)
				{
					//Color originalColor = this.GetComponentInParent<Explosion>().objectMaterialToChange.GetComponent<Renderer>().material.color;

					this.GetComponentInParent<Explosion>().explodeCar = false;
					this.GetComponentInParent<Explosion>().stopBlinking = true;
					this.GetComponentInParent<Explosion>().statusTxt.text = "Diffused ";
					this.GetComponentInParent<Explosion>().objectMaterialToChange.GetComponent<Renderer>().material.color = Color.green;
					//GameManager.Instance.bombTimer += 5f;
				}
			}
		}
    }
	
	public void OnTriggerExit(Collider other)
    {
		if (this.transform.parent.CompareTag("NPC") && other.CompareTag("Player " + GameManager.Instance.playerIndex))
        {
			if (this.GetComponentInParent<Explosion>().explodeCar && GameManager.Instance.playerInDiffuseBombSection)
			{
				diffuseTimer = 3;
			}
		}
			
    }

}
