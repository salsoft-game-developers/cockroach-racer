﻿using UnityEngine;

[ExecuteInEditMode]
public class WorldCurver : MonoBehaviour
{
	[Range(-0.0005f, 0.0005f)]
	public float curveStrengthX = 0, curveStrengthY = 0;

    int m_CurveStrengthIDX, m_CurveStrengthIDY;

    private void OnEnable()
    {
        m_CurveStrengthIDX = Shader.PropertyToID("_CurveStrengthX");
        m_CurveStrengthIDY = Shader.PropertyToID("_CurveStrengthY");
    }

	void Update()
	{
		//Shader.SetGlobalFloat(m_CurveStrengthID, curveStrength);
		Shader.SetGlobalFloat(m_CurveStrengthIDX, curveStrengthX);
		Shader.SetGlobalFloat(m_CurveStrengthIDY, curveStrengthY);
	}
}
