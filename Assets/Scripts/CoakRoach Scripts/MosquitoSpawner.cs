﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MosquitoSpawner : MonoBehaviour
{
    public GameObject mosquitoPrefab;

    public GameObject[] mSpawnPoints;
    public GameObject[] playerpoints;
    public List<string> spawnedMosquitos;

    public int mosquitoIndex;
    public bool mosquitoSpawned;

    private bool tutorial = true;
    
    // Start is called before the first frame update
    void Start()
    {
       //mosquitoIndex = Random.Range(0, mSpawnPoints.Length);
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (GameManager.Instance.timer > 0 /*&& spawnedMosquitos.Count < playerpoints.Length*/)
    //    {
    //        GameManager.Instance.timer -= 1 * Time.deltaTime;
    //        if (spawnedMosquitos.Count < playerpoints.Length)
    //        {
    //            Debug.Log("mosquitos count: " + spawnedMosquitos.Count + " < " + " playerpoint length: " + playerpoints.Length);
    //            SpawnMosquitos();
    //        }
    //        Debug.Log("Time: " + GameManager.Instance.timer);

    //    }
    //    else if (GameManager.Instance.timer <= 0)
    //    {
    //        return;
    //    }
    //}

    void FixedUpdate()
    {
        //if (GameManager.Instance.timer > 0 /*&& spawnedMosquitos.Count < playerpoints.Length*/)
        //{
        //    GameManager.Instance.timer -= 1 * Time.deltaTime;
        //    if (spawnedMosquitos.Count < playerpoints.Length)
        //    {
        //        Debug.Log("mosquitos count: " + spawnedMosquitos.Count + " < " + " playerpoint length: " + playerpoints.Length);
        //        SpawnMosquitos();
        //    }
        //    Debug.Log("Time: " + GameManager.Instance.timer);

        //}
        //else if (GameManager.Instance.timer <= 0)
        //{
        //    return;
        //}
                        
        if (GameManager.Instance.mosquitoSpawn == true /*&& !GameManager.Instance.playerCrashed*/ /*!GameManager.Instance.playerGameObject.GetComponent<BikeControl>().crash*/)
        {
            if (GameManager.Instance.playerCrashed == false)
            {

                Debug.Log("outside of if bodies.");

                if (spawnedMosquitos.Count < playerpoints.Length && !GameManager.Instance.mosquitoTutorial)
                {
                    //Debug.Log("mosquitos count: " + spawnedMosquitos.Count + " < " + " playerpoint length: " + playerpoints.Length);
                    SpawnMosquitos();
                    Debug.Log("spawning mosquitoes");
                }

                /*if (GameManager.Instance.mosquitoTutorial && tutorial)
                {
                    SpawnMosquitos();
                    Debug.Log("Mosquito tutorial");
                }*/
            }
        }

    }

    public void SpawnMosquitos()
    {
        //mosquitoIndex = Random.Range(0, mSpawnPoints.Length);
        GameObject spawnedMosquito = Instantiate(mosquitoPrefab, mSpawnPoints[mosquitoIndex].transform.position, Quaternion.Euler(0, 180, 0), transform);
        //spawnedMosquito.tag = "Mosquito";
       
        spawnedMosquito.name = "Mosquito " + mosquitoIndex;
        Debug.Log(spawnedMosquito.name);
        spawnedMosquitos.Add(spawnedMosquito.name);
       
        mosquitoIndex++;

        if(mosquitoIndex == mSpawnPoints.Length)
        {
            mosquitoIndex = 0;
        }

        /*if(spawnedMosquitos[0] != null && tutorial == true)
        {
            tutorial = false;
        }*/

        /*if (tutorial)
        {
            tutorial = false;
            Debug.Log("hello there");
        }*/
    }

    public void SpawnTutorialMosquito(int index)
    {
        Debug.Log("spawn please");
        GameObject spawnedMosquito = Instantiate(mosquitoPrefab, mSpawnPoints[index].transform.position, Quaternion.Euler(0, 180, 0), transform);

        spawnedMosquito.name = "Mosquito " + mosquitoIndex;
        
        spawnedMosquitos.Add(spawnedMosquito.name);

        mosquitoIndex++;
    }

    public void EndTutorial()
    {
        UIManager.Instance.mosquitoEndTutorialPnl.SetActive(true);
    }
    


    /*public IEnumerator EndTutorial()
    {
        Time.timeScale = 1;
        Debug.Log("am i called after killing mosquito 0");

        yield return new WaitForSeconds(0.5f);

        GameManager.Instance.mosquitoTutorial = false;
        GameManager.Instance.mosquitoTutorialIndex = 1;

        PlayerPrefs.SetInt("Mosquito Tutorial", GameManager.Instance.mosquitoTutorialIndex);

        Debug.Log("am i called after killing mosquito 1");
    }*/
}
