﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathCalculator : MonoBehaviour
{
    [SerializeField] private Transform endLineTransform;
   
    private Transform playerTransform;
    private Vector3 endLinePosition;
    private float fullDistance;

    public bool sectionEntered;

    private float GetDistance()
    {
        return (endLinePosition - playerTransform.position).sqrMagnitude;
    }

    private void UpdateProgressFill(float value)
    {
        UIManager.Instance.uiFillImg.fillAmount = value;
    }

    private void Update()
    {
        if (sectionEntered)
        {
            playerTransform = GameManager.Instance.playerGameObject.transform;
            endLinePosition = endLineTransform.position;
            fullDistance = GetDistance();
            sectionEntered = false;
        }

        if (GameManager.Instance.playerInDiffuseBombSection)
        {
            if(playerTransform.position.z <= endLinePosition.z) 
            {
                Debug.Log("endline z pos " + endLinePosition.z);
                float newDistance = GetDistance();
                float progressValue = Mathf.InverseLerp(fullDistance, 0f, newDistance);

                UpdateProgressFill(progressValue);
            }
        }
    }
}
