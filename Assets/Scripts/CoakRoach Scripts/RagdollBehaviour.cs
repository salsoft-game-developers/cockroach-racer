﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollBehaviour : MonoBehaviour
{
    public AudioSource crashSound;

    public Transform player;
    
    private CollisionDetector collisionDetectorScript;

    // Start is called before the first frame update
    void Start()
    {
        collisionDetectorScript = GameManager.Instance.playerGameObject.GetComponent<CollisionDetector>();
        crashSound = GameManager.Instance.playerGameObject.GetComponent<CollisionDetector>().crashSound;
        player = this.transform;

    }

    // Update is called once per frame
    void Update()
    {
        RagdollMethod();
    }

    void RagdollMethod()
    {
        if (collisionDetectorScript.collisionOccured && !GameManager.Instance.playerCrashed /*== true && intiateGameOver*/)
        {
            collisionDetectorScript.collisionOccured = false;

            GameManager.Instance.playerCrashed = true;

            if (player.parent != null)
            {
                crashSound.Play();
                player.parent = null;
            }

            UIManager.Instance.GameOver();

        }
    }
}
