﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficDestroyer : MonoBehaviour
{

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("NPC") || other.CompareTag("NPC2"))
        {

            Destroy(other.gameObject);

        }
    }

    /*public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("NPC") || other.CompareTag("NPC2"))
        {

            Destroy(other.gameObject);

        }
    }*/
}
