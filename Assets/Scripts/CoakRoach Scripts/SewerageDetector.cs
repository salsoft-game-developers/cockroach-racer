﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SewerageDetector : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Sewerage Start") /*&& GameManager.Instance.hasHeadStartActivated == false*/)
        {
            GameManager.Instance.playerGameObject.GetComponent<CollisionDetector>().triggerIndex++;
            
            if(GameManager.Instance.playerGameObject.GetComponent<CollisionDetector>().triggerIndex == 1)
            {
                GameManager.Instance.playerInSewerage = true;

                StartCoroutine(ShowStraightRoadSign());
            }
        }

        if (other.CompareTag("Train Section Start"))
        {
            /*GameManager.Instance.Player().GetComponent<CollisionDetector>().triggerIndex++;

            if (GameManager.Instance.Player().GetComponent<CollisionDetector>().triggerIndex == 1)
            {
                GameManager.Instance.playerInTrainSection = true;
                AddToList();
                StartCoroutine(GameManager.Instance.trafficGeneratorScript.TrainComing());
            }*/

            GameManager.Instance.playerInTrainSection = true;
            AddToList();
            StartCoroutine(GameManager.Instance.trafficGeneratorScript.TrainComing());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        /*if (other.CompareTag("Train Section Start"))
        {
            GameManager.Instance.Player().GetComponent<CollisionDetector>().triggerIndex++;

            if (GameManager.Instance.Player().GetComponent<CollisionDetector>().triggerIndex == 1)
            {
                GameManager.Instance.playerInTrainSection = true;
                AddToList();
                StartCoroutine(GameManager.Instance.trafficGeneratorScript.TrainComing());
            }
        }*/

        /*if (other.CompareTag("Train Section End"))
        {
            if (GameManager.Instance.Player().GetComponent<CollisionDetector>().triggerIndex >= 0)
            {
                GameManager.Instance.playerInTrainSection = false;
                GameManager.Instance.Player().GetComponent<CollisionDetector>().triggerIndex = 0;
            }
        }*/
    }

    //create a function to call a straight sign and make it dissapear.
    IEnumerator ShowStraightRoadSign()
    {
        UIManager.Instance.straightSign.SetActive(true);
        yield return new WaitForSeconds(10);
        UIManager.Instance.straightSign.SetActive(false);

    }

    public void AddToList()
    {
        GameObject trainSpawnersParent = GameObject.FindGameObjectWithTag("Train Spawners");
        for (int i = 0; i < trainSpawnersParent.transform.childCount; i++)
        {
            GameManager.Instance.trafficGeneratorScript.trafficSpawnPoints.Add(trainSpawnersParent.transform.GetChild(i));
        }
    }
}
