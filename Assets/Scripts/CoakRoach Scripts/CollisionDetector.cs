﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Animations.Rigging;

public class CollisionDetector : MonoBehaviour
{
    public GameObject animatedModel;
    public GameObject ragdollModel;
    public GameObject ragdollPrefab;
    public GameObject shield;
    public GameObject headStartShield;
    public GameObject placedBomb;
    public GameObject dronePrefab;
    public GameObject BulletPrefab;
    public GameObject rigReference;

    public Transform playerHolder;
    public Transform hiter;

    public ParticleSystem muzzleEffect;
    public ParticleSystem bombExplosion;
    public ParticleSystem explosion;

    public AudioSource crashSound;

    public Animator stuntAnimator;

    public bool collisionOccured;

    public float newForwardSpeedLimit;

    [SerializeField] string[] _collisionTag;
    [SerializeField] string[] stuntAnimations;

    public int speedBombLimit;

    private bool mosquitoShot;

    [HideInInspector]
    public int triggerIndex;

    private const float doubleClickTime = 0.2f;
    private float lastClickTime;
    private float initialForwardSpeedLimit;
    private float storeBombTimer;
    private float tempShieldTimer;

    private bool explodeOnce;

    private Vector3 myPosition;
    private Quaternion myRotation;
    private Quaternion myBikeRotation;

    private GameObject instantiatedRagdoll;

    private Transform myBike;

    private BikeControl BikeScript;

    private HolderMovement playerHolderScript;

    private Rigidbody bikeRigidbody;

    private CameraFollow cameraFollowScript;

    private TwoBoneIKConstraint rig;


    //public List<Transform> transforms;
    
    //public GameObject muzzlePrefab;
    /*private float pushForce;
    private Vector3 pushDir;
    public float gravity = 10.0f;

    public float force = 10f; //Force 10000f
    public float stunTime = 0.5f;
    private Vector3 hitDir;*/

    public void Awake()
    {
        stuntAnimator.enabled = false;

        cameraFollowScript = Camera.main.GetComponent<CameraFollow>();

        myBike = transform;
        bikeRigidbody = myBike.GetComponent<Rigidbody>();

        myPosition = ragdollModel.transform.localPosition;
        myRotation = ragdollModel.transform.localRotation;
        myBikeRotation = myBike.localRotation;

        animatedModel.SetActive(true);
        
        //ragdollModel.SetActive(false);

        BikeScript = GetComponent<BikeControl>();
        playerHolderScript = playerHolder.GetComponent<HolderMovement>();

        initialForwardSpeedLimit = BikeScript.bikeSetting.LimitForwardSpeed;
        rig = rigReference.GetComponent<TwoBoneIKConstraint>();
    }

    // Start is called before the first frame update
    void Start()
    {
        tempShieldTimer = GameManager.Instance.shieldTimer;

        placedBomb.SetActive(false);
        collisionOccured = false;
        rig.weight = 1;
    }

    public IEnumerator StartBikeMovement()
    {
        BikeScript.accelFwd = 1;
        UIManager.Instance.gamePlayScreen.SetActive(true);

        UIManager.Instance.timerTxt.text = "3";
        yield return new WaitForSeconds(5/3);
        UIManager.Instance.timerTxt.text = "2";
        yield return new WaitForSeconds(5/3);
        UIManager.Instance.timerTxt.text = "1";
        yield return new WaitForSeconds(5/3);
        UIManager.Instance.timerTxt.text = "GO";
        yield return new WaitForSeconds(0.5f);
        UIManager.Instance.timerTxt.text = null;

        GameManager.Instance.hasGameStarted = true;

        BikeScript.accelFwd = 0;

        UIManager.Instance.AfterStartGame();

    }

    // Update is called once per frame
    void Update()
    {

        //Activating shield on double tap
        if(GameManager.Instance.shieldAmount > 0)
        {
            if (Input.touchCount > 0 && !GameManager.Instance.isPaused)
            {
                Touch touch = Input.GetTouch(0);
                if (EventSystem.current.currentSelectedGameObject == null)
                {
                    float timeSinceLastTouch = Time.time - lastClickTime;
                    if (touch.phase == TouchPhase.Began)
                    {
                        if (timeSinceLastTouch <= doubleClickTime)
                        {
                            ActivateShield(GameManager.Instance.shieldTimer);
                            GameManager.Instance.shieldAmount--;
                            
                            UIManager.Instance.shieldInGameTxt.text = "X" + GameManager.Instance.shieldAmount.ToString("");

                            PlayerPrefs.SetInt("Shield", GameManager.Instance.shieldAmount);
                        }
                    }
                    lastClickTime = Time.time;
                }
            }
        }

        //for activating shiel on S key.
        if (Input.GetKeyDown(KeyCode.S) && GameManager.Instance.shieldAmount > 0)
        {
            /*GameManager.Instance.shieldTimer = 10;
            GameManager.Instance.hasShieldActivated = true;
            GameManager.Instance.shieldAmount--;
            
            UIManager.Instance.shieldInGameTxt.text = "X" + GameManager.Instance.shieldAmount.ToString("");

            if (GameManager.Instance.shieldAmount == 0)
            {
                UIManager.Instance.shieldIcon.SetActive(false);
            }

            PlayerPrefs.SetInt("Shield", GameManager.Instance.shieldAmount);*/

            ActivateShield(GameManager.Instance.shieldTimer);
            
            GameManager.Instance.shieldAmount--;

            UIManager.Instance.shieldInGameTxt.text = "X" + GameManager.Instance.shieldAmount.ToString("");

            PlayerPrefs.SetInt("Shield", GameManager.Instance.shieldAmount);
        }

        //If shield is activated it will be available for a certain period of time.
        if (GameManager.Instance.hasShieldActivated == true)
        {
            GameManager.Instance.shieldTimer -= 1 * Time.deltaTime;

            if (!GameManager.Instance.hasHeadStartActivated)
            {
                shield.SetActive(true);
            }

            //Shield dissappears after time limit ends.
            if (GameManager.Instance.shieldTimer <= 0)
            {
                shield.SetActive(false);
                
                GameManager.Instance.hasShieldActivated = false;
                GameManager.Instance.shieldTimer = tempShieldTimer;
            }
        }

        //Avtivating headstart
        if(GameManager.Instance.hasHeadStartActivated == true)
        {
            Time.timeScale = 4;
            
            GameManager.Instance.headStartTimer -= 1 * Time.deltaTime;
            GameManager.Instance.hasShieldActivated = true;

            //GetComponent<BikeControl>().accelFwd = 1;
            
            BikeScript.accelFwd = 1;
            
            headStartShield.SetActive(true);
            if (GameManager.Instance.headStartTimer < 0)
            {
                headStartShield.SetActive(false);
                
                Time.timeScale = 1;
                
                GameManager.Instance.hasHeadStartActivated = false;
                GameManager.Instance.hasShieldActivated = false;
                
                //GetComponent<BikeControl>().accelFwd = 0;
                
                BikeScript.accelFwd = 0;
            }
        }
    }

    private void FixedUpdate()
    {
        //for exploding player if speed decreases below certain limit.
        if (GameManager.Instance.playerInSpeedSection == true && collisionOccured == false)
        {
            if (/*GetComponent<BikeControl>().speed*/ BikeScript.speed < speedBombLimit && explodeOnce == true)
            {
                explodeOnce = false;
                Debug.Log("explode");
                
                ParticleSystem bombParticle = Instantiate(bombExplosion, transform.position, transform.rotation, transform);
                Destroy(bombParticle.gameObject, 2f);

                PlayerDeathByExplosion(1000f, transform, 10f);
            }
        }
    }

    public void ShootBtn()
    {
        //muzzleEffect.Play();
        //Debug.Log("shooting");
        
        animatedModel.GetComponent<Animator>().SetTrigger("Shoot");

        GameObject projectile = Instantiate(muzzleEffect.gameObject, hiter.position, Quaternion.identity, this.transform);

        //projectile.GetComponent<MeshRenderer>().enabled = false;
        //Instantiate(muzzleEffect.gameObject, projectile.transform.position, Quaternion.identity, projectile.transform);


        projectile.GetComponent<Rigidbody>().AddForce(Vector3.forward * 5000 /** GetComponent<Transform>().position.z*/, ForceMode.Acceleration);
        projectile.GetComponent<Rigidbody>().AddForce(Vector3.up * 10000, ForceMode.Acceleration);
        Destroy(projectile, 5f);
       
    }

    public void OnShootDown()
    {
        Debug.Log("Button Pressed");
        mosquitoShot = true;
    }

    public void OnShootUp()
    {
        Debug.Log("Button Released");
        mosquitoShot = false;
    }

    //This function is for activating shield. 
    public void ActivateShield(float Amount)
    {
        GameManager.Instance.shieldTimer = Amount;
        GameManager.Instance.hasShieldActivated = true;
    }

    //This function is called when collided with traffic and obstacles.
    public void PlayerDeath()
    {
        collisionOccured = true;
        animatedModel.SetActive(false);
        
        //ragdollModel.SetActive(true);

        //ragdollModel.GetComponent<BikeAnimation>().StoreRagdollTransform();

        //Activating Ragdoll then adding force to it.
        /*Component[] Rigidbodys = ragdollModel.GetComponentsInChildren(typeof(Rigidbody));

        foreach (Rigidbody RigidbodyChild in Rigidbodys)
        {
            //RigidbodyChild.AddForce(Vector3.up * GetComponent<BikeControl>().speed * 10);
            //transforms.Add(RigidbodyChild.transform);
            
            RigidbodyChild.AddForce(Vector3.up * BikeScript.speed * 10);
        }*/

        //instantitaing Ragdoll then adding force to it.
        Component[] Rigidbodys = InstantiatedRagdoll().GetComponentsInChildren(typeof(Rigidbody));

        GameManager.Instance.ChangeSkin(PlayerPrefs.GetInt("SkinSelected"));

        foreach (Rigidbody RigidbodyChild in Rigidbodys)
        {
            //RigidbodyChild.AddForce(Vector3.up * GetComponent<BikeControl>().speed * 10);
            //transforms.Add(RigidbodyChild.transform);
            
            RigidbodyChild.AddForce(10 * BikeScript.speed * Vector3.up);
        }

        UIManager.Instance.gamePlayScreen.SetActive(false);
        Debug.Log("Collision Occured");
    }

    //This function is called when player collids with explosives.
    public void PlayerDeathByExplosion(float force, Transform bomb, float radius)
    {
        collisionOccured = true;
        animatedModel.SetActive(false);
        
        //ragdollModel.SetActive(true);

        /*Component[] Rigidbodys = ragdollModel.GetComponentsInChildren(typeof(Rigidbody));

        foreach (Rigidbody RigidbodyChild in Rigidbodys)
        {
            RigidbodyChild.AddExplosionForce(force, bomb.position, radius);
        }*/
        
        Component[] Rigidbodys = InstantiatedRagdoll().GetComponentsInChildren(typeof(Rigidbody));

        GameManager.Instance.ChangeSkin(PlayerPrefs.GetInt("SkinSelected"));

        foreach (Rigidbody RigidbodyChild in Rigidbodys)
        {
            RigidbodyChild.AddExplosionForce(force, bomb.position, radius);
        }

        UIManager.Instance.gamePlayScreen.SetActive(false);
    }

    public GameObject InstantiatedRagdoll()
    {
        Debug.Log("Ragdoll Instantiated");

        instantiatedRagdoll = Instantiate(ragdollPrefab, ragdollModel.transform.position, ragdollModel.transform.rotation, ragdollModel.transform);
        
        return instantiatedRagdoll;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("NPC") || collision.gameObject.CompareTag("NPC2") || collision.gameObject.CompareTag("Obstacles") )
        {
            if (!collisionOccured && !GameManager.Instance.hasShieldActivated && /*!this.GetComponent<BikeControl>().crash*/ !GameManager.Instance.playerCrashed) 
            { 
                PlayerDeath(); 
                if(collision.rigidbody != null)
                {
                    collision.rigidbody.isKinematic = true;
                }
            }
            
        }


        /*if (hasShieldActivated)
        {
            if (collision.gameObject.CompareTag("Obstacles"))
            {
                Instantiate(explosion, collision.gameObject.transform.position, collision.gameObject.transform.rotation, collision.gameObject.transform);
                Destroy(collision.gameObject);
            }
            Instantiate(explosion, collision.gameObject.transform.position, collision.gameObject.transform.rotation, collision.gameObject.transform);
            //Destroy(collision.gameObject, 2f);
            if (collision.gameObject.GetComponent<Rigidbody>() != null)
            {
                if (collision.gameObject.GetComponent<TrafficComing>() != null || collision.gameObject.GetComponent<TrafficComing>() != null)
                {
                    collision.gameObject.GetComponent<TrafficComing>().enabled = false;
                }
                else
                {
                    collision.gameObject.GetComponent<TrafficGoing>().enabled = false;
                }
                //collision.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.forward, ForceMode.Force);
                foreach (ContactPoint contact in collision.contacts)
                {
                    Debug.DrawRay(contact.point, contact.normal, Color.white);

                    hitDir = contact.normal;
                    collision.gameObject.GetComponent<Rigidbody>().velocity = -hitDir * force;

                    pushForce = (-hitDir * force).magnitude;
                    pushDir = Vector3.Normalize((-hitDir * force));
                    StartCoroutine(Decrease((-hitDir * force).magnitude, stunTime, collision));
                    return;
                    Instantiate(explosion, collision.gameObject.transform.position, collision.gameObject.transform.rotation, collision.gameObject.transform);
                    Destroy(collision.gameObject, 2f);
                }
            }
        }*/

    }

    //private IEnumerator Decrease(float value, float duration, Collision rb)
    //{
    //    float delta = 0;
    //    delta = value / duration;

    //    for (float t = 0; t < duration; t += Time.deltaTime)
    //    {
    //        yield return null;
    //        pushForce = pushForce - Time.deltaTime * delta;
    //        pushForce = pushForce < 0 ? 0 : pushForce;
    //        rb.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, -gravity * GetComponent<Rigidbody>().mass, 0)); //Add gravity
    //    }

    //}

    public void OnTriggerEnter(Collider other)
    {
        //For Starting Mosquito Section
        if (other.gameObject.CompareTag("MosquitoTriggerEnter") /*&& UIManager.Instance.highScore > 5000*/)
        {

            triggerIndex++;

            if (triggerIndex == 1)
            {
                rig.weight = 0;

                //Mosquito wave incoming && shootng button appears.
                GameManager.Instance.mosquitoSpawn = true;

                animatedModel.GetComponent<Animator>().SetBool("Mosquito Section", true);

                UIManager.Instance.shootBtn.SetActive(true);
                UIManager.Instance.mosquitoSign.SetActive(true);

                if (GameManager.Instance.mosquitoTutorial)
                {
                    //StartCoroutine(StartMosquitoTutorial());
                    StartMosquitoTutorial();
                }
            }
        }

        //For Shooting Mosquitoes
        if (other.gameObject.CompareTag("Mosquito") && mosquitoShot == true)
        {
            other.GetComponent<MosquitoMovement>().OnMosquitoShot();
        }
        
        //For Activating Shield
        if (GameManager.Instance.hasShieldActivated)
        {
            for(int i = 0; i < _collisionTag.Length; i++)
            {
                if(other.CompareTag(_collisionTag[i]))
                {
                    ParticleSystem ExplosionParticle;
                    ExplosionParticle = Instantiate(explosion, other.gameObject.transform.position, Quaternion.identity);
                    Destroy(other.gameObject);
                    Destroy(ExplosionParticle.gameObject, 3f);
                }
            }
        }

        if (other.gameObject.CompareTag("DiffuseBombEnter") && GameManager.Instance.hasHeadStartActivated == false /*&& UIManager.Instance.highScore > 10000*/)
        {
            //Diffuse Bomb functionality
            Debug.Log("Entered diffuse bomb section");
            triggerIndex++;
            UIManager.Instance.uiFillImg.transform.parent.gameObject.SetActive(true);
            
            if(triggerIndex == 1) 
            { 
                other.GetComponentInParent<PathCalculator>().sectionEntered = true;
                UIManager.Instance.droneSign.SetActive(true);
            }

            StartCoroutine(Slowmo());
        }

        //For Entering Speed Section
        if (other.gameObject.CompareTag("SpeedTriggerEnter") && GameManager.Instance.hasHeadStartActivated == false /*&& UIManager.Instance.highScore > 15000*/)
        {
            //speed functionality
            
            triggerIndex++;
            
            Debug.Log("entered speed section");
            StartCoroutine(SpeedSectionStartingDelay());
        }

        //For Entering Sewerage Section
        if (other.gameObject.CompareTag("Sewerage Start") && GameManager.Instance.hasHeadStartActivated == false)
        {
            /*triggerIndex++;

            if(triggerIndex == 1)
            {
                Debug.Log("start sewerage");
                GameManager.Instance.playerInSewerage = true;
            }*/
        }
        //Expanded Lane stuff got scraped
        /*if (other.gameObject.CompareTag("ExpandLaneEnter"))
        {
            //speed functionality
            
            triggerIndex++;
            if (triggerIndex == 1) 
            { 
                GameManager.Instance.trafficGeneratorScript.enteredExpandedLane = true; 
                GameManager.Instance.trafficGeneratorScript.exitedExpandedLane = false; 
                GameManager.Instance.trafficGeneratorScript.isActive = true; 
            }

        }*/

        if(other.gameObject.CompareTag("Train Section Start") && GameManager.Instance.hasHeadStartActivated == false)
        {
            triggerIndex++;
            if (triggerIndex == 1)
            {
                if(transform.position.y >= 13)
                {
                    stuntAnimator.enabled = true;

                    string randomAnimation = stuntAnimations[Random.Range(0, stuntAnimations.Length)];
                    stuntAnimator.Play(randomAnimation);

                    StartCoroutine(DisableAnimator());
                }
                
            }
        }
    }
    
    IEnumerator DisableAnimator()
    {
        yield return new WaitForSeconds(1);

        stuntAnimator.enabled = false;
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("MosquitoTriggerExit") && GameManager.Instance.hasHeadStartActivated == false)
        {
            EndMosquitoSection();
        }

        if (other.gameObject.CompareTag("SpeedTriggerExit") && GameManager.Instance.hasHeadStartActivated == false)
        {
            EndSpeedSection();
        }

        if (other.gameObject.CompareTag("DiffuseBombExit") && GameManager.Instance.hasHeadStartActivated == false)
        {
            EndDiffuseBombSection();
        }

        //For Exiting Sewerage Section
        if (other.gameObject.CompareTag("Sewerage End") /*&& GameManager.Instance.hasHeadStartActivated == false*/)
        {
            if (triggerIndex >= 0)
            {
                EndSeverageSection();
            }
        }

        //For Exiting Train section
        if (other.gameObject.CompareTag("Train Section End") /*&& GameManager.Instance.hasHeadStartActivated == false*/)
        {
            if (triggerIndex >= 0)
            {
                EndTrainSection();
                GameManager.Instance.trafficGeneratorScript.ClearTrainSpawnersList();
            }
        }

        //Expanded Lane stuff got scraped
        /*if (other.gameObject.CompareTag("ExpandLaneEnter"))
        {
            triggerIndex = 0;

            if (triggerIndex == 0)
            {
                GameManager.Instance.trafficGeneratorScript.enteredExpandedLane = false;
                GameManager.Instance.trafficGeneratorScript.exitedExpandedLane = true;
                GameManager.Instance.trafficGeneratorScript.isActive = true;
            }

        }*/
    }

    /*IEnumerator StartMosquitoTutorial()
    {
        UIManager.Instance.mosquitoTutorialPnl.SetActive(true);
        Time.timeScale = 0.5f;

        yield return new WaitForSeconds(0.5f);

        UIManager.Instance.mosquitoTutorialPnl.SetActive(false);
        Time.timeScale = 1f;
    }*/
    
    public void StartMosquitoTutorial()
    {
        UIManager.Instance.mosquitoStartTutorialPnl.SetActive(true);
        Time.timeScale = 0f;
        
    }

    /*IEnumerator StartSpeedSectionTutorial()
    {
        UIManager.Instance.speedTutorialPnl.SetActive(true);
        Time.timeScale = 0.5f;

        yield return new WaitForSeconds(0.5f);

        UIManager.Instance.speedTutorialPnl.SetActive(false);
        Time.timeScale = 1f;

        GameManager.Instance.speedTutorial = false;

        PlayerPrefs.SetInt("Speed Tutorial", System.Convert.ToInt32(GameManager.Instance.speedTutorial));

    }*/

    public void StartSpeedSectionTutorial()
    {
        UIManager.Instance.speedTutorialPnl.SetActive(true);
        Time.timeScale = 0f;
    }

    IEnumerator StartDiffuseSectionTutorial()
    {
        UIManager.Instance.bombDiffuseTutorialPnl.SetActive(true);
        //Time.timeScale = 0.5f;

        yield return new WaitForSeconds(1);

        UIManager.Instance.bombDiffuseTutorialPnl.SetActive(false);
        //Time.timeScale = 0.5f;
    }
    IEnumerator SpeedSectionStartingDelay()
    {
        if (triggerIndex == 1)
        {
            if(explodeOnce == false)
            {
                explodeOnce = true;
            }

            UIManager.Instance.dangerSign.SetActive(true);

            if (GameManager.Instance.speedTutorial)
            {
                //StartCoroutine(StartSpeedSectionTutorial());
                StartSpeedSectionTutorial();
            }

            speedBombLimit = (int)BikeScript.speed - 15;

            UIManager.Instance.warningText.text = "Maintain speed above " + speedBombLimit + " or blow up!!!.";

            yield return new WaitForSeconds(3);

            Instantiate(dronePrefab, GameManager.Instance.mosquitoSpawner.mSpawnPoints[Random.Range(0, GameManager.Instance.mosquitoSpawner.mSpawnPoints.Length)].transform.position, Quaternion.identity, transform);

            UIManager.Instance.warningText.text = null;
            GameManager.Instance.playerInSpeedSection = true;

            placedBomb.SetActive(true);
        }
        //error instantiates 3 drones
    }

    IEnumerator SpeedSectionEndingDelay()
    {
        yield return new WaitForSeconds(Random.Range(0, 7));

        GameManager.Instance.playerInSpeedSection = false;

        UIManager.Instance.dangerSign.SetActive(false);
        UIManager.Instance.warningText.text = "Bomb has been diffused!!!.";

        placedBomb.SetActive(false);

        GameObject DiffusedBomb;

        if (GameManager.Instance.bombPos.GetChild(0) != null)
        {
            DiffusedBomb = GameManager.Instance.bombPos.GetChild(0).gameObject;
            DiffusedBomb.SetActive(true);
            DiffusedBomb.transform.SetParent(null);
            DiffusedBomb.GetComponent<CapsuleCollider>().enabled = true;
            DiffusedBomb.AddComponent<Rigidbody>();
            Destroy(DiffusedBomb, 7f);
        }


        yield return new WaitForSeconds(2.5f);

        UIManager.Instance.warningText.text = null;
    }

    IEnumerator Slowmo()
    {
        if (triggerIndex == 1)
        {
            storeBombTimer = GameManager.Instance.bombTimer;

            if (/*GetComponent<BikeControl>().speed*/ BikeScript.speed > newForwardSpeedLimit)
            {
                //GetComponent<Rigidbody>().velocity = Vector3.forward  * 35;
                bikeRigidbody.velocity = Vector3.forward * 35;

                //GetComponent<BikeControl>().currentGear = 1;
                BikeScript.currentGear = 1;
            }
            //GetComponent<Rigidbody>().Sleep();

            //GetComponent<BikeControl>().speed = GetComponent<Rigidbody>().velocity.magnitude / 2.7f;
            UIManager.Instance.bikeCameraScript.BikeHandBrake(true);
            UIManager.Instance.gamePlayScreen.SetActive(false);

            //GetComponent<BikeControl>().bikeSetting.LimitForwardSpeed = newForwardSpeedLimit;
            BikeScript.bikeSetting.LimitForwardSpeed = newForwardSpeedLimit;
            //GetComponent<BikeControl>().accelFwd = 0;

            GameManager.Instance.playerInDiffuseBombSection = true;
            GameObject DR = Instantiate(dronePrefab, GameManager.Instance.mosquitoSpawner.mSpawnPoints[Random.Range(0, GameManager.Instance.mosquitoSpawner.mSpawnPoints.Length)].transform.position, Quaternion.identity);

            Time.timeScale = 0.15f;

            if (GameManager.Instance.diffuseTutorial)
            {
                UIManager.Instance.bombDiffuseTutorialPnl.SetActive(true);
            }

            yield return new WaitForSeconds(0.5f);

            /*if (GameManager.Instance.diffuseTutorial)
            {
                UIManager.Instance.bombDiffuseTutorialPnl.SetActive(false);
                GameManager.Instance.diffuseTutorial = false;

                PlayerPrefs.SetInt("Diffuse Tutorial", System.Convert.ToInt32(GameManager.Instance.diffuseTutorial));

            }*/

            if (!GameManager.Instance.diffuseTutorial)
            {
                Time.timeScale = 1;
            }

            UIManager.Instance.bikeCameraScript.BikeHandBrake(false);
            UIManager.Instance.gamePlayScreen.SetActive(true);


        }
    }
    public void EndSpeedSection()
    {
        if(GameManager.Instance.playerInSpeedSection == true && GameManager.Instance.playerCrashed == false)
        {
            StartCoroutine(SpeedSectionEndingDelay());
            triggerIndex = 0;
        }
        else if (GameManager.Instance.playerInSpeedSection == true && GameManager.Instance.playerCrashed == true)
        {
            triggerIndex = 0;

            GameManager.Instance.playerInSpeedSection = false;

            UIManager.Instance.dangerSign.SetActive(false);
            UIManager.Instance.warningText.text = "Bomb has been diffused!!!.";

            placedBomb.SetActive(false);

            GameObject DiffusedBomb;

            if (GameManager.Instance.bombPos.GetChild(0) != null)
            {
                DiffusedBomb = GameManager.Instance.bombPos.GetChild(0).gameObject;
                DiffusedBomb.SetActive(true);
                DiffusedBomb.transform.SetParent(null);
                DiffusedBomb.GetComponent<CapsuleCollider>().enabled = true;
                DiffusedBomb.AddComponent<Rigidbody>();
                Destroy(DiffusedBomb, 7f);
            }
            
            UIManager.Instance.warningText.text = null;
        }
        
    }

    public void EndMosquitoSection()
    {
        triggerIndex = 0;

        GameManager.Instance.mosquitoSpawn = false;

        UIManager.Instance.mosquitoSign.SetActive(false);

        animatedModel.GetComponent<Animator>().SetBool("Mosquito Section", false);

        UIManager.Instance.shootBtn.SetActive(false);

        rig.weight = 1;
    }

    public void EndDiffuseBombSection()
    {
        UIManager.Instance.uiFillImg.transform.parent.gameObject.SetActive(false);

        GameManager.Instance.playerInDiffuseBombSection = false;

        UIManager.Instance.droneSign.SetActive(false);

        triggerIndex = 0;

        //GetComponent<BikeControl>().bikeSetting.LimitForwardSpeed = initialForwardSpeedLimit;
        BikeScript.bikeSetting.LimitForwardSpeed = initialForwardSpeedLimit;

        GameManager.Instance.bombTimer = storeBombTimer;
    }

    public void EndSeverageSection()
    {
        Debug.Log("end sewerage");
        triggerIndex = 0;
        GameManager.Instance.playerInSewerage = false;
    }
    
    public void EndTrainSection()
    {
        triggerIndex = 0;
        GameManager.Instance.playerInTrainSection = false;
    }

    public void ReviveBtnPressed()
    {

        SoundManager.Instance.btnSound.Play();
        
        ResetPlayerSettings();
        ActivateShield(5);
    }

    public void ResetPlayerSettings()
    {

        UIManager.Instance.gameOverScreen.SetActive(false);
        UIManager.Instance.gamePlayScreen.SetActive(true);

        UIManager.Instance.ResetRewardText();


        collisionOccured = false;
        GameManager.Instance.playerCrashed = false;

        ragdollModel.transform.parent = BikeScript.bikeSetting.MainBody.transform;

        Debug.Log("position " + myPosition);

        ragdollModel.transform.localPosition = myPosition;
        ragdollModel.transform.localRotation = myRotation;
        myBike.localRotation = myBikeRotation;

        animatedModel.SetActive(true);

        Destroy(instantiatedRagdoll);

        //ragdollModel.SetActive(false);
        
        playerHolderScript.ResetPosition();

        cameraFollowScript.ResetCameraPosition();
    }

   
}
