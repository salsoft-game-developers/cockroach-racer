﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectiblesManager : MonoBehaviour
{
    [SerializeField] GameObject collectibleParent;

    public Transform[] childrenTransforms;

    // Start is called before the first frame update
    void Start()
    {
        childrenTransforms = new Transform[transform.childCount];
        for(int i = 0; i < transform.childCount; i++)
        {
            childrenTransforms[i] = transform.GetChild(i);
            
        }

        foreach(Transform childTransform in childrenTransforms)
        {
            GameObject Collectible = Instantiate(GameManager.Instance.collectiblePrefabs[Random.Range(0, GameManager.Instance.collectiblePrefabs.Length)],
            /*childrenTransforms[Random.Range(0, childrenTransforms.Length)].position,*/
            childTransform.position,
            Quaternion.identity,
            collectibleParent.transform) as GameObject;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
