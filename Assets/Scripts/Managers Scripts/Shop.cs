﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [System.Serializable]
    public class ShopItem
    {
        public float Price;
        public float PowerUpsPrice;
        public float barValue;
        public bool IsPurchased = false;
        public GameObject selectButton;
    }

    public enum ShopSection 
    { 
        bikeSection = 1, 
        powerUpsSection = 2, 
        skinSections = 3 
    }

    public ShopSection shopSection;

    public List<ShopItem> ShopItemsList;
    public List<GameObject> priceTemplate;

    public GameObject sBtn;
    public GameObject removeAdsBtn;
    public GameObject buyIAPItemBtn;
    public GameObject watchToGetItemBtn;

    public GameObject[] selectBtns;

    public Button btnReward;

    public float rewardedCash = 5;

    public int templateIndex;
    public int boughtItemIndex;
    public int IAPItemIndex;
    public int adsItemIndex;

    //public Animator rewardAnimation;
    public Animator WarningAnim;

    //public Text RewardText;
    public Text warningText;

    [SerializeField] GameObject ItemTemplate;

    GameObject go;
    GameObject go1;

    Button buyBtn;
    Button upgradePowerUpsBtn;

    Image upgradeFillImg;
    

    void Start()
    {
        //?? if (AdManager.instance.adsDisabled == 1 /*PlayerPrefs.GetInt("Ads Removed", 0)*/)
        //{
        //	AdManager.instance.disableAds = true;

        //	btnReward.gameObject.SetActive(false);
        //	removeAdsBtn.SetActive(false);

        //}

        //if (AdsManager.Instance.adsDisabled == true /*PlayerPrefs.GetInt("Ads Removed", 0)*/)
        //{
        //    //	AdManager.instance.disableAds = true;

        //    //	btnReward.gameObject.SetActive(false);
        //    removeAdsBtn.SetActive(false);

        //}

        //These Buttons Are Not Available.
        //watchToGetCarBtn.SetActive(false);
        //btnReward.gameObject.SetActive(false);
        //buyIAPCarBtn.SetActive(false);

        //int adsDisabled = PlayerPrefs.GetInt("Ads Removed");

        DefaultItemPurchased();

        priceTemplate = new List<GameObject>();
        //templateIndex = UIManager.Instance.carIndex;
        ItemTemplate.SetActive(false);

        int len = ShopItemsList.Count;
        for (int i = 0; i < len; i++)
        {

            go = Instantiate(ItemTemplate, this.transform);
            go.transform.GetChild(2).GetComponent<Text>().text = "$" + ShopItemsList[i].Price.ToString();
            buyBtn = go.transform.GetChild(1).GetComponent<Button>();

            if(shopSection == ShopSection.powerUpsSection)
            {
                ShopItemsList[i].PowerUpsPrice = PlayerPrefs.GetFloat("PowerUpsPrice " + i, ShopItemsList[i].PowerUpsPrice);
                go.transform.GetChild(3).GetChild(1).GetComponent<Text>().text = "$" + ShopItemsList[i].PowerUpsPrice.ToString();
                
                upgradePowerUpsBtn = go.transform.GetChild(3).GetChild(0).GetComponent<Button>();

                ShopItemsList[i].barValue = PlayerPrefs.GetFloat("PowerUpsUpgradeBar " + i, ShopItemsList[i].barValue);
                go.transform.GetChild(3).GetChild(2).GetChild(0).GetComponent<Image>().fillAmount = ShopItemsList[i].barValue;

                Debug.Log(go.transform.GetChild(3).GetChild(2).GetChild(0).name);

                upgradePowerUpsBtn.AddEventListener(i, UpgradePowerUpsBtn);
            }

            go.SetActive(false);
            //go.transform.SetParent(UIManager.Instance.shop.transform);
            priceTemplate.Add(go);

            buyBtn.AddEventListener(i, BuyBtn);
        }

        LoadPrefs();

        for (int j = 0; j < priceTemplate.Count; j++)
        {
            if (ShopItemsList[j].IsPurchased == true)
            {
                DisableBuyTemplate();
            }

            if (templateIndex == j)
            {
                priceTemplate[j].SetActive(true);
                if (ShopItemsList[j].IsPurchased == true)
                {
                    ShopItemsList[j].selectButton.SetActive(true);
                }
            }
            else
            {
                priceTemplate[j].SetActive(false);
                ShopItemsList[j].selectButton.SetActive(false);
            }
        }
    }

    public void ChangeTemplateForward()
    {
        SoundManager.Instance.btnSound.Play();

        priceTemplate[templateIndex].SetActive(false);
        ShopItemsList[templateIndex].selectButton.SetActive(false);
        templateIndex++;

        if (templateIndex == priceTemplate.Count)
        {
            templateIndex = 0;
        }

        priceTemplate[templateIndex].SetActive(true);

        if (ShopItemsList[templateIndex].IsPurchased == true)
        {
            ShopItemsList[templateIndex].selectButton.SetActive(true);
        }

        //For ads Car.
        //if(templateIndex == adsCarIndex && ShopItemsList[templateIndex].IsPurchased == false /*&& AdManager.instance.carRewardAd.IsLoaded()*/)
        //{
        //	watchToGetCarBtn.SetActive(true);
        //	watchToGetCarBtn.transform.parent = priceTemplate[adsCarIndex].transform;
        //	priceTemplate[adsCarIndex].transform.GetChild(1).GetComponent<Text>().text = "WATCH A VIDEO TO UNLOCK";
        //	priceTemplate[adsCarIndex].transform.GetChild(2).gameObject.SetActive(false);

        //}
        //else { watchToGetCarBtn.SetActive(false); }

        //For In App Purchase Car.
        if (templateIndex == IAPItemIndex && ShopItemsList[templateIndex].IsPurchased == false && shopSection == ShopSection.skinSections)
        {
            buyIAPItemBtn.SetActive(true);
            //buyIAPItemBtn.transform.parent = priceTemplate[IAPItemIndex].transform;
            buyIAPItemBtn.transform.SetParent(priceTemplate[IAPItemIndex].transform);
            priceTemplate[IAPItemIndex].transform.GetChild(2).GetComponent<Text>().text = "BUY FOR";
            priceTemplate[IAPItemIndex].transform.GetChild(1).gameObject.SetActive(false);

        }
    }

    public void ChangeTemplateBackward()
    {
        SoundManager.Instance.btnSound.Play();

        priceTemplate[templateIndex].SetActive(false);
        ShopItemsList[templateIndex].selectButton.SetActive(false);
        templateIndex--;

        if (templateIndex < 0)
        {
            templateIndex = priceTemplate.Count - 1;
        }

        priceTemplate[templateIndex].SetActive(true);
        if (ShopItemsList[templateIndex].IsPurchased == true)
        {
            ShopItemsList[templateIndex].selectButton.SetActive(true);

        }

        //if (templateIndex == adsCarIndex && ShopItemsList[templateIndex].IsPurchased == false /*&& AdManager.instance.carRewardAd.IsLoaded()*/)
        //{
        //	watchToGetCarBtn.SetActive(true);
        //	watchToGetCarBtn.transform.parent = priceTemplate[adsCarIndex].transform;
        //	priceTemplate[adsCarIndex].transform.GetChild(1).GetComponent<Text>().text = "WATCH A VIDEO TO UNLOCK";
        //	priceTemplate[adsCarIndex].transform.GetChild(2).gameObject.SetActive(false);

        //}
        //else { watchToGetCarBtn.SetActive(false); }

        if (templateIndex == IAPItemIndex && ShopItemsList[templateIndex].IsPurchased == false && shopSection == ShopSection.skinSections)
        {
            buyIAPItemBtn.SetActive(true);
            buyIAPItemBtn.transform.parent = priceTemplate[IAPItemIndex].transform;
            priceTemplate[IAPItemIndex].transform.GetChild(2).GetComponent<Text>().text = "SALE";
            priceTemplate[IAPItemIndex].transform.GetChild(1).gameObject.SetActive(false);
        }
    }

    public void BuyBtn(int itemIndex)
    {
        boughtItemIndex = itemIndex;
        if (UIManager.Instance.totalMoney >= ShopItemsList[boughtItemIndex].Price)
        {
            UIManager.Instance.totalMoney -= ShopItemsList[boughtItemIndex].Price;
            
            SoundManager.Instance.buySound.Play();
            
            PlayerPrefs.SetFloat("Money", UIManager.Instance.totalMoney);
            UIManager.Instance.UpdateAllCoinsUIText();

            if(shopSection == ShopSection.powerUpsSection)
            {
                BuyingPowerups();
            }
            else
            {
                ShopItemsList[boughtItemIndex].IsPurchased = true;
                DisableBuyTemplate();
                SavePrefs();
            }

            if (shopSection == ShopSection.skinSections)
            {
                UIManager.Instance.shopSkins[boughtItemIndex].GetComponent<Animator>().SetTrigger("GotBought");
            }
        }
        else
        {
            SoundManager.Instance.cantBuySound.Play();
            //warningText.text = "NOT ENOUGH CASH !!!";
            //WarningAnim.SetTrigger("Warning");
        }
    }

    public void BuyingPowerups()
    {
        if(boughtItemIndex == 0)
        {
            //for fuel
            if (GameManager.Instance.fuelAmount < (100.00f - 10))
            {
                GameManager.Instance.fuelAmount += 10;
                PlayerPrefs.SetFloat("Fuel", GameManager.Instance.fuelAmount);
            }
        }
        else if(boughtItemIndex == 1)
        {
            GameManager.Instance.shieldAmount++;
            PlayerPrefs.SetInt("Shield", GameManager.Instance.shieldAmount);
        }
        else if(boughtItemIndex == 2)
        {
            GameManager.Instance.headStartAmount++;
            PlayerPrefs.SetInt("HeadStart", GameManager.Instance.headStartAmount);
        }

        UIManager.Instance.UpdatePowerUpsText();
    }

    public void UpgradePowerUpsBtn(int itemIndex)
    {
        //Set limit afterwards for the the maximum duration of powerups
        if (UIManager.Instance.totalMoney >= ShopItemsList[itemIndex].PowerUpsPrice)
        {
            if(ShopItemsList[itemIndex].barValue < 1)
            {
                UIManager.Instance.totalMoney -= ShopItemsList[itemIndex].PowerUpsPrice;

                ShopItemsList[itemIndex].PowerUpsPrice *= 2;
                PlayerPrefs.SetFloat("PowerUpsPrice " + itemIndex, ShopItemsList[itemIndex].PowerUpsPrice);

                ShopItemsList[itemIndex].barValue += 0.1f;
                PlayerPrefs.SetFloat("PowerUpsUpgradeBar " + itemIndex, ShopItemsList[itemIndex].barValue);

                priceTemplate[itemIndex].transform.GetChild(3).GetChild(1).GetComponent<Text>().text = "$" + ShopItemsList[itemIndex].PowerUpsPrice.ToString();
                priceTemplate[itemIndex].transform.GetChild(3).GetChild(2).GetChild(0).GetComponent<Image>().fillAmount = ShopItemsList[itemIndex].barValue;

                PlayerPrefs.SetFloat("Money", UIManager.Instance.totalMoney);
                
                UIManager.Instance.UpdateAllCoinsUIText();

                if (itemIndex == 0)
                {
                    GameManager.Instance.fuelTimer /= 2;
                    PlayerPrefs.SetFloat("FuelTimer", GameManager.Instance.fuelTimer);
                }
                else if (itemIndex == 1)
                {
                    GameManager.Instance.shieldTimer *= 2;
                    PlayerPrefs.SetFloat("ShieldTimer", GameManager.Instance.shieldTimer);
                }
                else if (itemIndex == 2)
                {
                    GameManager.Instance.headStartTimer *= 2;
                    PlayerPrefs.SetFloat("HeadStartTimer", GameManager.Instance.headStartTimer);
                }
            }
            else
            {

            }
            
        }
        else
        {
            SoundManager.Instance.cantBuySound.Play();

            /*warningText.text = "NOT ENOUGH CASH !!!";
            WarningAnim.SetTrigger("Warning");*/
        }
    }

    public void DefaultItemPurchased()
    {
        if (shopSection == ShopSection.bikeSection || shopSection == ShopSection.skinSections)
        {
            ShopItemsList[0].IsPurchased = true;
        }
    }

    public void DisableBuyTemplate()
    {
        go1 = priceTemplate[boughtItemIndex];
        sBtn = ShopItemsList[boughtItemIndex].selectButton;
        if (ShopItemsList[boughtItemIndex].IsPurchased == true)
        {
            for (int i = 0; i < go1.transform.childCount; i++)
            {
                go1.transform.GetChild(i).gameObject.SetActive(false);
            }
            sBtn.SetActive(true);
        }
    }

    public void DisableBuyTemplate(int index)
    {
        go1 = priceTemplate[index];
        sBtn = ShopItemsList[index].selectButton;

        if (ShopItemsList[index].IsPurchased == true)
        {
            for (int i = 0; i < go1.transform.childCount; i++)
            {
                go1.transform.GetChild(i).gameObject.SetActive(false);
            };
            sBtn.SetActive(true);
        }
    }

    /*public void AddCash(int amount)
    {
        //rewardAnimation.SetTrigger("Reward");

        UIManager.Instance.RewardText.text = "+ $" + amount.ToString();
        UIManager.Instance.rewardAnimation.SetTrigger("Reward");

        UIManager.Instance.totalMoney += amount;

        PlayerPrefs.SetFloat("Money", UIManager.Instance.totalMoney);
        UIManager.Instance.UpdateAllCoinsUIText();
        SoundManager.Instance.buySound.Play();
    }*/

    /*public void RemoveAdds()
    {
        //? AdManager.instance.disableAds = true;

        AdsManager.Instance.adsDisabled = true;
        AdsManager.Instance.gameObject.GetComponent<RewardedAd>()._showAdButton.gameObject.SetActive(false);
        AdsManager.Instance.gameObject.GetComponent<BannerAd>().HideBannerAd();

        UIManager.Instance.RewardText.text = "ADS ARE DISABLED";
        UIManager.Instance.rewardAnimation.SetTrigger("Reward");

        //?		AdManager.instance.DestroyBanner();
        //?	AdManager.instance.DestroyInterstitial();

        btnReward.gameObject.SetActive(false);
        removeAdsBtn.SetActive(false);
        SoundManager.Instance.buySound.Play();

        PlayerPrefs.SetInt("Ads Removed", 1);

    }*/

    public void IAPItemBought()
    {
        buyIAPItemBtn.SetActive(false);
        boughtItemIndex = IAPItemIndex;
        ShopItemsList[boughtItemIndex].IsPurchased = true;

        DisableBuyTemplate();
        PlayerPrefs.SetInt("Bought Items " + boughtItemIndex, 1);
        //SoundManager.Instance.buySound.Play();
    }

    public void SavePrefs()
    {
        PlayerPrefs.SetInt("Bought Items " + boughtItemIndex, 1);
        //PlayerPrefs.Save();
    }

    public void LoadPrefs()
    {
        for (int k = 0; k < ShopItemsList.Count; k++)
        {
            int BoughtItem = PlayerPrefs.GetInt("Bought Items " + k, 0);
            if (BoughtItem == 1)
            {
                if(shopSection == ShopSection.bikeSection || shopSection == ShopSection.skinSections)
                {
                    ShopItemsList[k].IsPurchased = true;
                    DisableBuyTemplate(k);
                }
            }
        }
    }
}
