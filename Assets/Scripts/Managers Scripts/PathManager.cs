﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathManager : MonoBehaviour
{

    /*#region Singleton Class: PathManager

    public static PathManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    #endregion*/

    public Transform playerHolderTransform;

    public GameObject firstpath;

    [SerializeField] GameObject[] tunelPrefabs;
    [SerializeField] float spawnZ = -6f;
    [SerializeField] float tunelLength = 20f;
    [SerializeField] float safeZone = 15f;
    [SerializeField] int amountOfTunels = 3;

    int lastPrefabIndex = 0;
    public List<GameObject> activeTunel;

    //public PlayerMovement Player;

    // Start is called before the first frame update

    void Start()
    {
        playerHolderTransform = GameManager.Instance.playerHolder.transform;

        // Player = GetComponent<PlayerMovement>();
        //UpdatePlayerTrandform();
        activeTunel = new List<GameObject>();
        //playerTransform = playerTransform.gameObject.transform;
        //GameObject.FindGameObjectWithTag("Player").transform;

        SpawnFirstTunels();
    }

    void Update()
    {
        if (playerHolderTransform.position.z - safeZone > (spawnZ - amountOfTunels * tunelLength) && !GameManager.Instance.playerCrashed /*!playerTransform.GetComponent<BikeControl>().crash*/)
        {
            SpawnTunel();
            DeleteTunel();
        }
    }

    public void SpawnFirstTunels()
    {
        spawnZ = 0f;
        for (int i = 0; i < amountOfTunels; i++)
        {
            if (i < 2) { SpawnTunel(0); }
            else { SpawnTunel(); }
        }
    }

    //public void UpdatePosition() { }

    public void SpawnTunel(int prefabIndex = -1)
    {
        GameObject Go;
        if (prefabIndex == -1)
        {
            Go = Instantiate(tunelPrefabs[RandomPrefabIndex()]) as GameObject;
        }
        else
        {
            Go = Instantiate(tunelPrefabs[prefabIndex]) as GameObject;
        }

        Go.transform.SetParent(transform);
        Go.transform.position = Vector3.forward * spawnZ;
        spawnZ += tunelLength;
        activeTunel.Add(Go);

        //for (int i = 0; i <= Go.GetComponent<WayPoints>().wayPointArray.Length - 1; i++)
        //{
        //    Player.wayPointsPlayer.Add(Go.GetComponent<WayPoints>().wayPointArray[i]);
        //}
    }

    void DeleteTunel()
    {
        Destroy(activeTunel[0]);
        activeTunel.RemoveAt(0);
    }

    public void DeleteAllTunels()
    {
        for (int i = 0; i <= activeTunel.Count; i++)
        {
            Destroy(activeTunel[i].gameObject);
        }
    }

    private int RandomPrefabIndex()
    {
        if (tunelPrefabs.Length <= 1) { return 0; }

        int randomIndex = lastPrefabIndex;
        while (randomIndex == lastPrefabIndex)
        {
            randomIndex = Random.Range(0, tunelPrefabs.Length);
        }

        lastPrefabIndex = randomIndex;
        return randomIndex;
    }
}

