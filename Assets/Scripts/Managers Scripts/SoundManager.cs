﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{


    #region Singleton Class: SoundManager

    public static SoundManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    #endregion


    public AudioSource bgSound;
    public AudioSource shopBgSound;
    public AudioSource carSound;
    public AudioSource PoliceSound;
    public AudioSource policeSiren;
    public AudioSource screechSound;
    public AudioSource blastSound;
    public AudioSource btnSound;
    public AudioSource buySound;
    public AudioSource cantBuySound;
    public AudioSource collectibleSound;
   
}
