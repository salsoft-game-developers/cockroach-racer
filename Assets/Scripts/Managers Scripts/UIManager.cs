﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//using UnityEngine.Video;

public class UIManager : MonoBehaviour
{
    #region Singleton Class: UIManager

    public static UIManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    #endregion

    //    private UnityEngine.Video.VideoPlayer videoPlayer;
    //    public GameObject playGameScreen;
    //    public GameObject selectModeScreen;
    //    public GameObject selectLocationScreen;
    //    public GameObject gameQuitScreen;
    //    public GameObject cutSceneScreen;
    //    public GameObject Player;
    //    public GameObject gameOverScreen;
    //    public GameObject gamePlayScreen;
    // public GameObject pauseScreen;
    //    public GameObject ReturnToMainMenuScreen;
    //    public GameObject optionsScreen;
    //    public GameObject helpScreen;
    //    public GameObject soundOnBtn;
    //    public GameObject soundOffBtn;
    //    public GameObject shopScreen;
    //    public GameObject shop;
    //    public GameObject iapScreen;
    //    public GameObject lastScreen;
    //    public GameObject shopExitBtn;
    //    public GameObject doubleCashBtn;
    //    public GameObject restartBtn;
    //    public GameObject mainMenuBtn;

    //    public Text scoreTxt;
    //    public Text speedTxt;
    //    public Text distanceTxt;
    //    public Text highScoreTxt;
    //    public Text yourScoreTxt;
    //    public Text cashTxt;
    //    public Text totalCashTxt;
    //    public Text totalCashShopTxt;
    //    public Text RewardText;
    //    public Text newHighScoreTxt;
    //    public Text gameOverHighScoreTxt;

    //    public Animator rewardAnimation;
    //    public Animator newHighScoreAnimation;
    //    public Animator doubleRewardAnimation;

    //    public float score;
    //    public float highScore;
    //    public float minPercentage = 0.1f;
    //    public float maxPercentage = 0.2f;
    //    public float totalCash;

    //    float movingSpeed = 0.05f;
    //    float cash;

    //    GameObject playerHolder;
    //    GameObject policeHolder;

    //    [SerializeField] AudioListener cameraAudio;

    //    public GameObject tutorial1;
    //    public GameObject laneTutorial;
    //    public GameObject tutorial2;
    //    public GameObject BustedImg;

    //    public GameObject[] shopCars;
    //    public GameObject[] selectedCars;
    //    public GameObject[] selectBtns2;

    //    public Text[] allCashUIText;

    //    public int t;
    //    public int animatedVideoIndex;
    //    public int carIndex = 0;
    //    public int saveCarIndex;
    //    public int mapIndex;

    //    int selectedCarIndex;
    //    int soundState;
    //    int addIndex = 0;

    //    private GameObject gameCamera;

    //    int firstGameIndex = 0;
    //    bool isTabletOrIpad;

    //    public GameObject locationPanel;
    //    public RectTransform[] logos;

    //    // Start is called before the first frame update
    //    void Start()
    //    {

    //#if UNITY_IOS
    //        isTabletOrIpad = UnityEngine.iOS.Device.generation.ToString().Contains("iPad"); //Ipad

    //#elif UNITY_ANDROID
    //        //float aspectRatio = Mathf.Max(Screen.width, Screen.height) / Mathf.Min(Screen.width, Screen.height);
    //        //isTabletOrIpad = (DeviceDiagonalSizeInInches() > 6.5f && aspectRatio < 2f);
    //#endif
    //        if (isTabletOrIpad) //Tab
    //        {

    //            Vector3 pos1 = locationPanel.GetComponent<RectTransform>().localPosition;
    //            pos1 = new Vector3(0,-135,0);
    //            locationPanel.GetComponent<RectTransform>().localPosition = pos1;
    //            locationPanel.GetComponent<RectTransform>().localScale = new Vector3(0.7f,0.7f,0);

    //            for (int i = 0; i < logos.Length; i++)
    //            {

    //               logos[i].localPosition = new Vector3(0, logos[i].localPosition.y + 190f, 0);

    //            }
    //        }

    //        //doubleCashBtn.SetActive(true);


    //        playerHolder = GameObject.FindGameObjectWithTag("Player Holder");
    //        policeHolder = GameObject.FindGameObjectWithTag("Police Holder");
    //        gameCamera = GameObject.FindGameObjectWithTag("MainCamera");

    //        //Input.backButtonLeavesApp = true;
    //        //watchPlayBtn.SetActive(false);
    //        firstGameIndex = PlayerPrefs.GetInt("FirstGame", firstGameIndex);
    //        animatedVideoIndex = PlayerPrefs.GetInt("AnimatedVideo", 0);

    //        t = PlayerPrefs.GetInt("tutorial", 0);
    //        totalCash = PlayerPrefs.GetFloat("Money", totalCash);
    //        soundState = PlayerPrefs.GetInt("Sound State",0);
    //        saveCarIndex = PlayerPrefs.GetInt("CarSelected", 0);
    //        highScore = PlayerPrefs.GetFloat("HighScore");
    //        addIndex = PlayerPrefs.GetInt("Show Ads", addIndex);

    //        if (soundState == 0)
    //        {
    //            SoundOn();
    //        }
    //        else if(soundState == 1)
    //        {
    //            SoundOff();
    //        }

    //        if(saveCarIndex == PlayerPrefs.GetInt("CarSelected", selectedCarIndex))
    //        {
    //            DefaultCar(saveCarIndex);
    //        }

    //        else
    //        {
    //            DefaultCar(0);
    //        }

    //        //if (animatedVideoIndex == 0)
    //        //{
    //        //    watchPlayBtn.SetActive(false);
    //        //}
    //        //else { watchPlayBtn.SetActive(true); }

    //        videoPlayer = Player.GetComponent<UnityEngine.Video.VideoPlayer>();
    //        videoPlayer.url = Path.Combine(Application.streamingAssetsPath, "Movie 003-1.m4v");

    //        playGameScreen.SetActive(false);
    //        gameQuitScreen.SetActive(false);
    //        cutSceneScreen.SetActive(false);
    //        Player.SetActive(false);
    //        gameOverScreen.SetActive(false);
    //        gamePlayScreen.SetActive(false);
    //        pauseScreen.SetActive(false);
    //        ReturnToMainMenuScreen.SetActive(false);
    //        optionsScreen.SetActive(false);
    //        helpScreen.SetActive(false);
    //        tutorial1.SetActive(false);
    //        laneTutorial.SetActive(false);
    //        tutorial2.SetActive(false);
    //        shopScreen.SetActive(false);
    //        selectModeScreen.SetActive(false);
    //        selectLocationScreen.SetActive(false);

    //        playGameScreen.SetActive(true);

    //        selectedCars[selectedCarIndex].SetActive(true);
    //        UpdateAllCoinsUIText();

    //        highScoreTxt.text = "High Score: " + highScore.ToString("0");

    //        videoPlayer.loopPointReached += EndReached;

    //    }

    //    public void Update()
    //    {
    //        if(playGameScreen.active != false)
    //        {
    //            if (Input.GetKeyDown(KeyCode.Escape))
    //            {
    //                //ExitGame();
    //                playGameScreen.SetActive(false);
    //                gameQuitScreen.SetActive(true);
    //            }
    //        }

    //        if(shopScreen.active == true)
    //        {
    //            gameCamera.SetActive(false);
    //        }
    //        else { gameCamera.SetActive(true);  }
    //    }

    //    public void ConfirmQuitBtn()
    //    {
    //        ExitGame();
    //    }

    //    public void CancelQuitButton()
    //    {
    //        gameQuitScreen.SetActive(false);
    //        playGameScreen.SetActive(true);
    //    }

    //    public void UpdateAllCoinsUIText()
    //    {
    //        for (int i = 0; i < allCashUIText.Length; i++)
    //        {
    //            allCashUIText[i].text = "$ " + totalCash.ToString("0");
    //        }
    //    }

    //    void EndReached(UnityEngine.Video.VideoPlayer vp)
    //    {
    //        SkipCutScene();
    //    }

    //    public void PlayGameScreen()
    //    {
    //        //AdManager.instance.DestroyBanner();
    //        //playGameScreen.SetActive(false);

    //        //SoundManager.Instance.btnSound.Play();

    //        //if (animatedVideoIndex == 0)
    //        //{
    //        //    cutSceneScreen.SetActive(true);
    //        //    Player.SetActive(true);
    //        //}
    //        //else
    //        //{
    //        //    Player.SetActive(false);
    //        //    cutSceneScreen.SetActive(false);
    //        //    GameManager.Instance.gamePlaying = true;
    //        //    gamePlayScreen.SetActive(true);
    //        //    SoundManager.Instance.BgSound.Play();
    //        //    SoundManager.Instance.carSound.Play();
    //        //    SoundManager.Instance.policeSiren.Play();
    //        //}



    //        ShowSelectModeScreen();

    //        //GameManager.Instance.gamePlaying = true;

    //        //GameManager.Instance.gamePlaying = false;

    //        //playGameScreen.SetActive(false);
    //        //gamePlayScreen.SetActive(true);


    //        //if (t == 0)
    //        //{
    //        //    Debug.Log("is true");
    //        //    GameManager.Instance.tutorialPlaying = true;
    //        //    tutorial1.SetActive(true);
    //        //    laneTutorial.SetActive(true);
    //        //}

    //        //GameManager.Instance.tutorialPlaying = true;
    //        //tutorial1.SetActive(true);

    //        //SoundManager.Instance.BgSound.Play();
    //        //SoundManager.Instance.carSound.Play();
    //        //SoundManager.Instance.policeSiren.Play();

    //        //if (GameManager.Instance.gamePlaying)
    //        //{
    //        //    SoundManager.Instance.BgSound.Play();
    //        //    SoundManager.Instance.carSound.Play();
    //        //    SoundManager.Instance.policeSiren.Play();
    //        //}
    //        //else
    //        //{
    //        //    SoundManager.Instance.BgSound.Stop();
    //        //    SoundManager.Instance.carSound.Stop();
    //        //    SoundManager.Instance.policeSiren.Stop();
    //        //}

    //        //if (PlayerPrefs.GetInt("tutorial") == 0)
    //        //{

    //        //    tutorialImage.SetActive(true);
    //        //    tutorial1.SetActive(true);

    //        //}

    //    }

    //    public void ShowSelectModeScreen()
    //    {
    //        playGameScreen.SetActive(false);
    //        selectModeScreen.SetActive(true);
    //    }

    //    public void ExitSelectModeBtn()
    //    {
    //        selectModeScreen.SetActive(false);
    //        playGameScreen.SetActive(true);
    //    }

    //    public void SelectModeBtn(int i)
    //    {
    //        if (i == (int)GameManager.GameModes.OneWay)
    //        {
    //            GameManager.Instance.SelectMode(GameManager.GameModes.OneWay);
    //        }

    //        if(i == (int)GameManager.GameModes.TwoWay)
    //        {
    //            GameManager.Instance.SelectMode(GameManager.GameModes.TwoWay);
    //        }

    //        if(i == (int)GameManager.GameModes.WrongWay)
    //        {
    //            GameManager.Instance.SelectMode(GameManager.GameModes.WrongWay);
    //        }

    //        selectModeScreen.SetActive(false);
    //        selectLocationScreen.SetActive(true);

    //    }

    //    public void ExitSelectLocationBtn()
    //    {
    //        GameManager.Instance.spawnRightGoing.gameObject.SetActive(true);
    //        GameManager.Instance.spawnLeftComing.gameObject.SetActive(true);
    //        GameManager.Instance.spawnLeftGoing.gameObject.SetActive(true);
    //        GameManager.Instance.spawnRightComing.gameObject.SetActive(true);

    //        GameManager.Instance.rightGoingTarget.SetActive(true);
    //        GameManager.Instance.leftComingTarget.SetActive(true);
    //        GameManager.Instance.leftGoingTarget.SetActive(true);
    //        GameManager.Instance.rightComingTarget.SetActive(true);

    //        selectLocationScreen.SetActive(false);
    //        selectModeScreen.SetActive(true);
    //    }

    //    public void SelectLocationBtn(int i)
    //    {
    //        mapIndex = i;
    //        if (i == (int)GameManager.GameLocations.NormalMap)
    //        {
    //            GameManager.Instance.SelectLocations(GameManager.GameLocations.NormalMap);
    //        }

    //        if(i == (int)GameManager.GameLocations.SnowyMap)
    //        {
    //            GameManager.Instance.SelectLocations(GameManager.GameLocations.SnowyMap);
    //        }

    //        if(i == (int)GameManager.GameLocations.RainyMap)
    //        {
    //            GameManager.Instance.SelectLocations(GameManager.GameLocations.RainyMap);
    //        }

    //        if(i == (int)GameManager.GameLocations.BeachMap)
    //        {
    //            GameManager.Instance.SelectLocations(GameManager.GameLocations.BeachMap);
    //        }


    //        if(GameManager.Instance.isOneWay == true)
    //        {
    //            GameManager.Instance.TurnOneWayOn();
    //        }
    //        else if(GameManager.Instance.isTwoWay == true)
    //        {
    //            GameManager.Instance.TurnTwoWayOn();
    //        }
    //        else if(GameManager.Instance.isWrongWay == true)
    //        {
    //            GameManager.Instance.TurnWrongWayOn();
    //        }


    //      //?  AdManager.instance.DestroyBanner();
    //        selectLocationScreen.SetActive(false);

    //        SoundManager.Instance.btnSound.Play();

    //        if (animatedVideoIndex == 0)
    //        {
    //            cutSceneScreen.SetActive(true);
    //            Player.SetActive(true);
    //        }
    //        else
    //        {
    //            Player.SetActive(false);
    //            cutSceneScreen.SetActive(false);
    //            GameManager.Instance.gamePlaying = true;
    //            gamePlayScreen.SetActive(true);
    //            SoundManager.Instance.BgSound.Play();
    //            SoundManager.Instance.carSound.Play();
    //            SoundManager.Instance.policeSiren.Play();
    //        }

    //        //if(!AdManager.instance.doubleCashRewardAd.IsLoaded())
    //        //    AdManager.instance.CreateAndLoadDoubleCashRewardedAd();

    //    }

    //    //public void WatchAndPlay()
    //    //{
    //    //    AdManager.instance.DestroyBanner();
    //    //    playGameScreen.SetActive(false);
    //    //    SoundManager.Instance.btnSound.Play();
    //    //    cutSceneScreen.SetActive(true);
    //    //    Player.SetActive(true);
    //    //}

    //    public void SkipCutScene()
    //    {
    //        animatedVideoIndex = 1;
    //        PlayerPrefs.SetInt("AnimatedVideo", animatedVideoIndex);

    //        Player.SetActive(false);
    //        cutSceneScreen.SetActive(false);
    //        GameManager.Instance.gamePlaying = true;
    //        gamePlayScreen.SetActive(true);

    //        if (t == 0)
    //        {
    //            Debug.Log("is true");
    //            GameManager.Instance.tutorialPlaying = true;
    //            tutorial1.SetActive(true);
    //            laneTutorial.SetActive(true);
    //        }

    //        SoundManager.Instance.BgSound.Play();
    //        SoundManager.Instance.carSound.Play();
    //        SoundManager.Instance.policeSiren.Play();
    //    }

    //    public void Pause()
    //    {
    //        //?AdManager.instance.RequestBanner();

    //        GameManager.Instance.gamePlaying = false;
    //        SoundManager.Instance.carSound.Stop();
    //        SoundManager.Instance.policeSiren.Stop();

    //        gamePlayScreen.SetActive(false);
    //        pauseScreen.SetActive(true);

    //    }

    //    public void Resume()
    //    {
    //       //? AdManager.instance.DestroyBanner();

    //        pauseScreen.SetActive(false);

    //        SoundManager.Instance.btnSound.Play();

    //        gamePlayScreen.SetActive(true);

    //        SoundManager.Instance.carSound.Play();
    //        SoundManager.Instance.policeSiren.Play();
    //        GameManager.Instance.gamePlaying = true;
    //    }


    //    public void MainMenu()
    //    {
    //        pauseScreen.SetActive(false);
    //        SoundManager.Instance.btnSound.Play();
    //        ReturnToMainMenuScreen.SetActive(true);
    //    }

    //    public void ConfirmBtn()
    //    {
    //        SoundManager.Instance.btnSound.Play();
    //       //? AdManager.instance.DestroyBanner();
    //        SceneManager.LoadScene(0);
    //    }

    //    public void CancelBtn()
    //    {
    //        ReturnToMainMenuScreen.SetActive(false);
    //        SoundManager.Instance.btnSound.Play();
    //        pauseScreen.SetActive(true);
    //    }

    //    public void OptionsButton(GameObject LastScreen)
    //    {
    //        SoundManager.Instance.btnSound.Play();
    //        lastScreen = LastScreen;
    //        lastScreen.SetActive(false);
    //        optionsScreen.SetActive(true);
    //    }

    //    public void SoundOff()
    //    {
    //        SoundManager.Instance.btnSound.Play();
    //        soundOnBtn.SetActive(false);
    //        AudioListener.pause = true;
    //        soundOffBtn.SetActive(true);
    //        PlayerPrefs.SetInt("Sound State", 0);
    //    }

    //    public void SoundOn()
    //    {
    //        //SoundManager.Instance.btnSound.Play();
    //        soundOffBtn.SetActive(false);
    //        AudioListener.pause = false;
    //        soundOnBtn.SetActive(true);
    //        PlayerPrefs.SetInt("Sound State", 1);
    //    }

    //    public void HelpScreen()
    //    {
    //        SoundManager.Instance.btnSound.Play();
    //        optionsScreen.SetActive(false);
    //        helpScreen.SetActive(true);
    //    }

    //    public void ExitHelpScreen()
    //    {
    //        SoundManager.Instance.btnSound.Play();
    //        helpScreen.SetActive(false);
    //        optionsScreen.SetActive(true);
    //    }

    //    public void ExitOptions()
    //    {
    //        SoundManager.Instance.btnSound.Play();
    //        optionsScreen.SetActive(false);
    //        lastScreen.SetActive(true);
    //    }

    //    public void HighScoreFunction(float position)
    //    {

    //        score = (position) * movingSpeed;
    //        scoreTxt.text = "Score: " + score.ToString("0");

    //        if (highScore < score && firstGameIndex == 1)
    //        {
    //            newHighScoreTxt.text = "NEW HIGHSCORE!!!";
    //            newHighScoreAnimation.SetTrigger("Reward");
    //        }
    //    }

    //    public void DisplaySpeenAndDistance(float position, float speed)
    //    {
    //        float distanceCovered = position * 0.25f;
    //        distanceTxt.text = "DISTANCE: " + distanceCovered.ToString(".00") + " KM";

    //        float movingSpeed = speed;
    //        speedTxt.text = "SPEED: " + movingSpeed.ToString("00") + " KM/H";
    //    }

    //    public float CashConverter()
    //    {
    //        cash = Random.Range(minPercentage,maxPercentage) * score;
    //        return cash;
    //    }

    //    [HideInInspector] public float GameOverCash = 0; 

    //    public void GameOverScreen()
    //    {
    //        //?AdManager.instance.RequestBanner();
    //        SoundManager.Instance.BgSound.Stop();
    //        SoundManager.Instance.carSound.Stop();
    //        firstGameIndex = 1;
    //        PlayerPrefs.SetInt("FirstGame", firstGameIndex);
    //        if (highScore < score)
    //        {
    //            PlayerPrefs.SetFloat("HighScore", score);
    //            gameOverHighScoreTxt.text = "NEW HIGHSCORE !!!";
    //        }
    //        else { gameOverHighScoreTxt.text = "HighScore: " + highScore.ToString("0"); }

    //        gameOverScreen.SetActive(true);

    //        yourScoreTxt.text = "" + score.ToString("0");
    //        GameOverCash = CashConverter();
    //        cashTxt.text = "$" + GameOverCash.ToString("0");


    //        totalCash = totalCash + GameOverCash;
    //        PlayerPrefs.SetFloat("Money", totalCash);

    //        UpdateAllCoinsUIText();
    //        //doubleCashBtn.SetActive(true);// change

    //        //  if (CashConverter() > 0.99f && AdManager.instance.disableAds == false && AdManager.instance.doubleCashRewardAd.IsLoaded())
    //        //{
    //        //    doubleCashBtn.SetActive(true);
    //        //    StartCoroutine(DelayRestart());
    //        //}
    //        //else
    //        //{
    //        //    Debug.Log("gameover ad not loaded");

    //        //    //doubleCashBtn.SetActive(false); ??Mine

    //        //    //AdManager.instance.CreateAndLoadRewardedAd(AdManager.instance.RewardDouble_adUnitId);
    //        //   // AdManager.instance.CreateAndLoadDoubleCashRewardedAd();

    //        //}
    //    }

    //    public IEnumerator DelayRestart()
    //    {
    //        restartBtn.SetActive(false);
    //        mainMenuBtn.SetActive(false);
    //        yield return new WaitForSeconds(3f);
    //        restartBtn.SetActive(true);
    //        mainMenuBtn.SetActive(true);
    //    }

    //    public IEnumerator GameoverDelayNPC()
    //    {
    //        if (GameManager.Instance.gamePlaying == true)
    //        {
    //            GameManager.Instance.gamePlaying = false;
    //            gamePlayScreen.SetActive(false);
    //            SoundManager.Instance.blastSound.Play();

    //            yield return new WaitForSeconds(0.5f);

    //            BustedImg.SetActive(true);
    //            SoundManager.Instance.policeSiren.Stop();

    //            yield return new WaitForSeconds(0.75f);

    //            SoundManager.Instance.PoliceSound.Play();
    //            SoundManager.Instance.BgSound.Stop();
    //            SoundManager.Instance.carSound.Stop();

    //            yield return new WaitForSeconds(1.5f);

    //            BustedImg.SetActive(false);
    //            GameOverScreen();
    //        }
    //    }

    //    public IEnumerator GameoverDelayPolice()
    //    {
    //        if (GameManager.Instance.gamePlaying == true)
    //        {
    //            gamePlayScreen.SetActive(false);
    //            //SoundManager.Instance.blastSound.Play();

    //            yield return new WaitForSeconds(0.5f);

    //            SoundManager.Instance.blastSound.Play();
    //            GameManager.Instance.gamePlaying = false;

    //            yield return new WaitForSeconds(0.75f);

    //            BustedImg.SetActive(true);
    //            SoundManager.Instance.policeSiren.Stop();

    //            yield return new WaitForSeconds(0.75f);

    //            SoundManager.Instance.PoliceSound.Play();
    //            SoundManager.Instance.BgSound.Stop();
    //            SoundManager.Instance.carSound.Stop();

    //            yield return new WaitForSeconds(1.5f);

    //            BustedImg.SetActive(false);
    //            GameOverScreen();
    //        }
    //    }

    //    public void Shop(GameObject LastScreen)
    //    {
    //        SoundManager.Instance.btnSound.Play();
    //        lastScreen = LastScreen;
    //        lastScreen.SetActive(false);
    //        shopScreen.SetActive(true);
    //        UpdateAllCoinsUIText();

    //        for (int i = 0; i < shopCars.Length; i++)
    //        {
    //            if (carIndex == i)
    //            {
    //                shopCars[i].SetActive(true);
    //            }
    //            else
    //            {
    //                shopCars[i].SetActive(false);
    //            }

    //        }

    //        //if (AdManager.instance.rewardedAd.IsLoaded())
    //        //{
    //        //    //AdManager.instance.rewardedAd.Show();
    //        //    ShopController.Instance.btnReward.gameObject.SetActive(true);
    //        //    Debug.Log("loaded");
    //        //}
    //        //else
    //        //{
    //        //    ShopController.Instance.btnReward.gameObject.SetActive(false);
    //        //    //AdManager.instance.CreateAndLoadRewardedAd(AdManager.instance.Reward_adUnitId); ///?
    //        //    Debug.Log("not loaded");
    //        //}

    //        //if (ShopController.Instance.templateIndex == 1 && ShopController.Instance.ShopItemsList[ShopController.Instance.templateIndex].IsPurchased == false /* && AdManager.instance.carRewardAd.IsLoaded()*/)
    //        //{
    //        //    ShopController.Instance.watchToGetCarBtn.SetActive(true);
    //        //    ShopController.Instance.watchToGetCarBtn.transform.parent = ShopController.Instance.priceTemplate[1].transform;
    //        //    ShopController.Instance.priceTemplate[1].transform.GetChild(1).GetComponent<Text>().text = "WATCH A VIDEO TO UNLOCK";
    //        //    ShopController.Instance.priceTemplate[1].transform.GetChild(2).gameObject.SetActive(false);

    //        //}
    //        //else if(ShopController.Instance.ShopItemsList[ShopController.Instance.templateIndex].IsPurchased == false && !AdManager.instance.carRewardAd.IsLoaded())
    //        //{
    //        //    ShopController.Instance.watchToGetCarBtn.SetActive(false);
    //        //    //AdManager.instance.CreateAndLoadRewardedAd(AdManager.instance.RewardCar_adUnitId); ///?
    //        //}
    //        //else 
    //        //{
    //        //    ShopController.Instance.watchToGetCarBtn.SetActive(false);
    //        //}


    //    }

    //    public void InAppPurchase()
    //    {
    //        SoundManager.Instance.btnSound.Play();
    //        shop.SetActive(false);
    //        iapScreen.SetActive(true);
    //    }

    //    public void ExitIAPScreen()
    //    {
    //        SoundManager.Instance.btnSound.Play();
    //        iapScreen.SetActive(false);
    //        shop.SetActive(true);
    //    }

    //    public void NextCar()
    //    {
    //        SoundManager.Instance.btnSound.Play();
    //        ShopController.Instance.ChangeTemplateForward();
    //        shopCars[carIndex].SetActive(false);
    //        carIndex++;
    //        if (carIndex == shopCars.Length)
    //        {
    //            carIndex = 0;
    //        }
    //        shopCars[carIndex].SetActive(true);
    //    }

    //    public void PreviousCar()
    //    {
    //        SoundManager.Instance.btnSound.Play();
    //        ShopController.Instance.ChangeTemplateBackward();
    //        shopCars[carIndex].SetActive(false);
    //        carIndex--;

    //        if(carIndex < 0)
    //        {
    //            carIndex = shopCars.Length - 1;
    //        }

    //        shopCars[carIndex].SetActive(true);
    //    }

    //    public void SelectCar(int SelectedCarIndex)
    //    {
    //        if(lastScreen == playGameScreen) 
    //        {
    //            SoundManager.Instance.btnSound.Play();
    //            selectedCarIndex = SelectedCarIndex;

    //            for (int i = 0; i < selectedCars.Length; i++)
    //            {
    //                if (selectedCarIndex == i)
    //                {
    //                    selectBtns2[i].transform.GetChild(0).GetComponent<Text>().text = "SELECTED";
    //                    selectBtns2[i].GetComponent<Button>().interactable = false;
    //                    selectedCars[i].SetActive(true);
    //                    PlayerPrefs.SetInt("CarSelected", selectedCarIndex);

    //                }
    //                else
    //                {
    //                    selectBtns2[i].transform.GetChild(0).GetComponent<Text>().text = "SELECT";
    //                    selectBtns2[i].GetComponent<Button>().interactable = true;
    //                    selectedCars[i].SetActive(false);
    //                }
    //            }
    //        }
    //        else
    //        {
    //            SoundManager.Instance.cantBuySound.Play();
    //            ShopController.Instance.warningText.text = "CANNOT SELECT WHEN PLAYING !!!";
    //            ShopController.Instance.WarningAnim.SetTrigger("Warning");
    //        }

    //    }  

    //    private void DefaultCar(int SelectedCarIndex)
    //    {
    //        selectedCarIndex = SelectedCarIndex;

    //        for (int i = 0; i < selectedCars.Length; i++)
    //        {
    //            if (selectedCarIndex == i)
    //            {
    //                selectBtns2[i].transform.GetChild(0).GetComponent<Text>().text = "SELECTED";
    //                selectBtns2[i].GetComponent<Button>().interactable = false;
    //                selectedCars[i].SetActive(true);
    //                PlayerPrefs.SetInt("CarSelected", selectedCarIndex);

    //            }
    //            else
    //            {
    //                selectBtns2[i].transform.GetChild(0).GetComponent<Text>().text = "SELECT";
    //                selectBtns2[i].GetComponent<Button>().interactable = true;
    //                selectedCars[i].SetActive(false);
    //            }
    //        }

    //    } 

    //    public void ExitShop()
    //    {
    //        SoundManager.Instance.btnSound.Play();

    //        shopScreen.SetActive(false);
    //        lastScreen.SetActive(true);

    //        //AdManager.instance.DestroyRewardedAd();
    //        //AdManager.instance.DestroyCarRewardAd();
    //    }

    //    public void RestartGame()
    //    {

    //        Debug.Log("add Index " + addIndex);
    //        //  if (addIndex >= 2 && AdManager.instance.disableAds == false)
    //        //{
    //        //    AdManager.instance.forShowInterstialAd();
    //        //    addIndex = 0;
    //        //}
    //        //else
    //        //{
    //        //    AdManager.instance.DestroyBanner();
    //        //    AdManager.instance.DestroyInterstitial();
    //        //    SceneManager.LoadScene(0);
    //        //    addIndex++;
    //        //}

    //        //PlayerPrefs.SetInt("Show Ads", addIndex);

    //        //if (AdManager.instance.disableAds == true)
    //        //{
    //        //    AdManager.instance.DestroyBanner();
    //        //    AdManager.instance.DestroyInterstitial();
    //        //    SceneManager.LoadScene(0);
    //        //}   

    //        // loads current scene


    //        if (addIndex >= 2 && AdsManager.Instance.adsDisabled == false)
    //        {
    //            InterstitialAd.Instance.ShowAd();
    //            SceneManager.LoadScene(0);
    //            addIndex = 0;
    //        }
    //        else
    //        {

    //            SceneManager.LoadScene(0);
    //            if (AdsManager.Instance.adsDisabled == false)
    //            { addIndex++; }
    //        }

    //        PlayerPrefs.SetInt("Show Ads", addIndex);



    //    }

    //    public void RestartBtn()
    //    {
    //        //if (doubleCashBtn.GetComponent<Button>().interactable == false) 
    //        //{

    //        //    doubleCashBtn.GetComponent<Animator>().enabled = false;
    //        //    //doubleCashBtn.SetActive(true);
    //        //    doubleCashBtn.GetComponent<Button>().interactable = true;
    //        //    doubleCashBtn.GetComponentInChildren<Text>().text = "WATCH & DOUBLE CASH";
    //        //}


    //        PathManager Go; 

    //        Go = GameManager.Instance.pathManagers[mapIndex].GetComponent<PathManager>();
    //        playerHolder.transform.position = new Vector3(playerHolder.transform.position.x, playerHolder.transform.position.y, Go.activeTunel[0].transform.position.z);
    //        policeHolder.transform.position = new Vector3(playerHolder.transform.position.x, playerHolder.transform.position.y, Go.activeTunel[0].transform.position.z - 5f);

    //        GameObject[] gameObjects0 = GameObject.FindGameObjectsWithTag("NPC");
    //        GameObject[] gameObjects1 = GameObject.FindGameObjectsWithTag("NPC2");

    //        for (int j = 0; j < gameObjects0.Length; j++)
    //        {
    //            if(gameObjects0[j] != null)
    //            {
    //                Destroy(gameObjects0[j].gameObject);
    //            }

    //        }

    //        for (int k = 0; k < gameObjects1.Length; k++)
    //        {
    //            if (gameObjects1[k] != null)
    //            {
    //                Destroy(gameObjects1[k].gameObject);
    //            }

    //        }

    //        //? AdManager.instance.DestroyBanner();
    //        playerHolder.GetComponentInParent<Animator>().enabled = false;

    //        playerHolder.GetComponent<PlayerMovement>().leftSpeed = playerHolder.GetComponent<PlayerMovement>().initialSpeed;

    //        playerHolder.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 180, 0);
    //        policeHolder.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 0, 0);

    //        playerHolder.GetComponentInChildren<PlayerPosition>().carAnimation.SetTrigger("Restart");

    //        playerHolder.GetComponent<PlayerMovement>().score = 0f;
    //        policeHolder.GetComponentInChildren<PolicePosition>().testbool = true;

    //        GameManager.Instance.gamePlaying = true;
    //        SoundManager.Instance.BgSound.Play();

    //        gameOverScreen.SetActive(false);

    //        gamePlayScreen.SetActive(true);
    //    }

    //    public void ExitGame()
    //    {
    //        animatedVideoIndex = 0;
    //        PlayerPrefs.SetInt("AnimatedVideo", animatedVideoIndex);
    //        Application.Quit();
    //    }

    public BikeCamera bikeCameraScript;

    public GameObject mainMenuScreen;
    public GameObject shopScreen;
    public GameObject shopSubScreen;
    public GameObject iAPScreen;
    public GameObject gamePlayScreen;
    public GameObject gameSettingsScreen;
    public GameObject gamePauseScreen;
    public GameObject gameOverScreen;
    public GameObject shootBtn;
    public GameObject headStartBtn, shieldIcon, cashBagIcon, shiftBtn;
    public GameObject reviveBtn;
    public GameObject restoreBtn;
    public GameObject changeModeBtn;
    public GameObject bikeSection, powerUpsSection, skinSection;
    public GameObject rewardPanel;
    public GameObject rewardText;
    public GameObject mosquitoStartTutorialPnl, mosquitoEndTutorialPnl, speedTutorialPnl, bombDiffuseTutorialPnl , shieldTutorialPnl;
    public GameObject pedaDirection, boosterDirection, dangerSign, mosquitoSign, droneSign, straightSign;
    
    public GameObject[] soundBtns;
    public GameObject[] shopBikes;
    public GameObject[] shopSkins;
    public GameObject[] shopPowerUps;
    public GameObject[] selectedBikes;

    public Transform bikeCameraPos, powerupsCameraPos, SkinCameraPos;

    public Animator shutterAnimation;
    public Animator tutorialAnimator;

    public Image uiFillImg;

    public Camera mainCam;

    public Text timerTxt;
    public Text tutorialInstructionsTxt;
    public Text uiStartText;
    public Text uiEndText;
    public Text warningText;
    public Text scoreTxt;
    public Text gameOverHighScoreTxt;
    public Text gameOverScoreTxt;
    public Text receivedMoneyTxt;
    public Text fuelAmountTxt;
    public Text shieldAmountTxt;
    public Text headStartTxt;
    public Text shieldInGameTxt, headStartInGameTxt, cashBagInGameTxt, bagsObtainedTxt;

    public Text[] allCashUIText;

    public float pauseTimeScaleValue;
    public float resumeTimeScaleValue;
    public float score;
    public float highScore;
    public float totalMoney = 0f;
    public float money = 0f;
    public float moneyInBags = 0f;
    public float minPercentageMoney = 0.1f;
    public float maxPercentageMoney = 0.2f;

    public int bikeIndex = 0;
    public int skinIndex = 0;
    public int powerUpsIndex = 0;
    //public int selectedBikeIndex;

    public bool bikeCamera, powerupsCamera, SkinCamera;
    public bool soundStatus, modeStatus;

    private GameObject lastScreen;

    private Transform originalCamPos;
   
    Vector2 tempPos;
    
    private int addIndex = 0, reviveBtnIndex = 0;

    private bool firstSectionTutorial, secondSectionTutorial;

    public void Start()
    {
        //selectedBikeIndex = PlayerPrefs.GetInt("BikeSelected", 0);
        DefaultBike(PlayerPrefs.GetInt("Bike Selected"));
        DefaultSkin(PlayerPrefs.GetInt("SkinSelected"));

        addIndex = PlayerPrefs.GetInt("Show Ads", addIndex);
        reviveBtnIndex = PlayerPrefs.GetInt("Show Revive Btn", reviveBtnIndex);
        highScore = PlayerPrefs.GetFloat("HighScore");
        totalMoney = PlayerPrefs.GetFloat("Money", totalMoney);

        mainCam = Camera.main;
        originalCamPos = mainCam.transform;
        

        if (PlayerPrefs.GetInt("Sound State") == 1)
        {
            soundStatus = true;
            SoundStatus(soundStatus);
        }
        else
        {
            soundStatus = false;
            SoundStatus(soundStatus);
        }
        
        if (PlayerPrefs.GetInt("Game Mode") == 1)
        {
            modeStatus = true;
            SelectMode(modeStatus);
        }
        else
        {
            modeStatus = false;
            SelectMode(modeStatus);
        }

        mainMenuScreen.SetActive(true);
        shopScreen.SetActive(false);
        iAPScreen.SetActive(false);
        gamePlayScreen.SetActive(false);
        gameSettingsScreen.SetActive(false);
        gamePauseScreen.SetActive(false);
        gameOverScreen.SetActive(false);
        bikeSection.SetActive(false);
        powerUpsSection.SetActive(false);
        skinSection.SetActive(false);

        mosquitoStartTutorialPnl.SetActive(false);
        speedTutorialPnl.SetActive(false);
        bombDiffuseTutorialPnl.SetActive(false);
        shieldTutorialPnl.SetActive(false);

        shootBtn.SetActive(false);
        uiFillImg.transform.parent.gameObject.SetActive(false);

    }

    public void Update()
    {
        if (bikeCamera) 
        {
            mainCam.transform.position = Vector3.LerpUnclamped(mainCam.transform.position, bikeCameraPos.position, 1 * Time.deltaTime * 10);
            mainCam.transform.rotation = Quaternion.LerpUnclamped(mainCam.transform.rotation, bikeCameraPos.rotation, 1 * Time.deltaTime * 10);
            
            if (mainCam.transform.position == bikeCameraPos.position)
            {
                bikeCamera = false;
            }
        }
        else if(powerupsCamera)
        {
            mainCam.transform.position = Vector3.LerpUnclamped(mainCam.transform.position, powerupsCameraPos.position, 1 * Time.deltaTime * 10);
            mainCam.transform.rotation = Quaternion.LerpUnclamped(mainCam.transform.rotation, powerupsCameraPos.rotation, 1 * Time.deltaTime * 10);
            
            if (mainCam.transform.position == powerupsCameraPos.position)
            {
                powerupsCamera = false;
            }
        }
        else if (SkinCamera)
        {
            mainCam.transform.position = Vector3.LerpUnclamped(mainCam.transform.position, SkinCameraPos.position, 1 * Time.deltaTime * 10);
            mainCam.transform.rotation = Quaternion.LerpUnclamped(mainCam.transform.rotation, SkinCameraPos.rotation, 1 * Time.deltaTime * 10);
            
            if (mainCam.transform.position == SkinCameraPos.position)
            {
                SkinCamera = false;
            }
        }

        if (!GameManager.Instance.isPaused)
        {
            tempPos.x = GameManager.Instance.playerGameObject.transform.position.x;
            //tempPos.y = GameManager.Instance.Player().transform.position.y;
            rewardPanel.GetComponent<RectTransform>().anchoredPosition = tempPos;
        }
    }

    public void StartGame()
    {
        SoundManager.Instance.btnSound.Play();

        mainMenuScreen.SetActive(false);

        StartCoroutine(GameManager.Instance.playerGameObject.GetComponent<CollisionDetector>().StartBikeMovement());
    }

    public void AfterStartGame()
    {
        if (GameManager.Instance.firstTutorial)
        {
            shiftBtn.SetActive(false);

            firstSectionTutorial = true;

            tutorialInstructionsTxt.text = "Press On The Race Pedal To Increase Speed.";
            
            //tutorialAnimator.SetTrigger("ShowTutorialDown");
            
            tutorialAnimator.Play("Tutorial Down Animation");
            pedaDirection.SetActive(true);
        }

        else
        {
            shiftBtn.SetActive(true);
        }

        ShowHeadStartBtn();

        if (GameManager.Instance.shieldAmount > 0)
        {
            shieldIcon.SetActive(true);
            shieldInGameTxt.text = "X" + GameManager.Instance.shieldAmount.ToString("");

        }
        else
        {
            shieldIcon.SetActive(false);
        }

        if (GameManager.Instance.cashBagAmount > 0)
        {
            cashBagIcon.SetActive(true);
            cashBagInGameTxt.text = "X" + GameManager.Instance.cashBagAmount.ToString("");

        }
        else
        {
            cashBagIcon.SetActive(false);
        }

        AdsManager.Instance.bannerScript.HideBannerAd();
    }

    public IEnumerator MovementTutorialDelay()
    {
        if (GameManager.Instance.firstTutorial)
        {

            tutorialInstructionsTxt.text = "Good Job!!!";
            
            yield return new WaitForSeconds(0.5f);

            //tutorialAnimator.SetTrigger("ShowTurorialUp");


            tutorialAnimator.Play("Tutorial Up Animation");

            pedaDirection.SetActive(false);

            yield return new WaitForSeconds(1);

            tutorialInstructionsTxt.text = "Now Tilt Mobile Left Or Right To Change Lane.";
            
            //tutorialAnimator.SetTrigger("ShowTutorialDown");
            
            tutorialAnimator.Play("Tutorial Down Animation");
            GameManager.Instance.playerGameObject.GetComponent<BikeControl>().showTiltTutorialOnce = true;

            secondSectionTutorial = true;


        }
    }
    
    public IEnumerator BoosterTutorialDelay()
    {
        if (GameManager.Instance.firstTutorial && secondSectionTutorial)
        {

            tutorialInstructionsTxt.text = "Well Done!!!";
            //tutorialAnimator.SetTrigger("ShowTurorialUp");
            
            yield return new WaitForSeconds(0.5f);

            tutorialAnimator.Play("Tutorial Up Animation");

            Debug.Log(tutorialInstructionsTxt.GetComponentInParent<Animator>().name);

            yield return new WaitForSeconds(1);

            shiftBtn.SetActive(true);
            tutorialInstructionsTxt.text = "Press On The Nitro Icon to Boost.";

            //tutorialAnimator.SetTrigger("ShowTutorialDown");
            tutorialAnimator.Play("Tutorial Down Animation");

            boosterDirection.SetActive(true);
        }
    }
    
    public IEnumerator EndingTutorialDelay()
    {
        if (GameManager.Instance.firstTutorial)
        {
            boosterDirection.SetActive(false);

            tutorialInstructionsTxt.text = "Great You Are Ready To GO!!!";
            
            yield return new WaitForSeconds(0.5f);

            //tutorialAnimator.SetTrigger("ShowTurorialUp");
            tutorialAnimator.Play("Tutorial Up Animation");

            yield return new WaitForSeconds(1);

            tutorialInstructionsTxt.text = null;

            GameManager.Instance.firstTutorial = false;
            GameManager.Instance.firstTutorialIndex = 1;

            PlayerPrefs.SetInt("Gameplay Tutorial", GameManager.Instance.firstTutorialIndex);

        }
    }

    public void ShowSettings()
    {
        mainMenuScreen.SetActive(false);
        gameSettingsScreen.SetActive(true);
    }

    public void CloseSettings()
    {
        gameSettingsScreen.SetActive(false);
        mainMenuScreen.SetActive(true);
    }

    public void PauseGameBtn()
    {

        SoundManager.Instance.btnSound.Play();

        GameManager.Instance.isPaused = true;
        Time.timeScale = pauseTimeScaleValue;
        gamePlayScreen.SetActive(false);
        gamePauseScreen.SetActive(true);
        
        AdsManager.Instance.bannerScript.ShowBannerAd();
    }

    public void SoundBtnClicked()
    {
        if (soundStatus == true)
        {
            soundStatus = false;
        }
        else 
        {
            soundStatus = true;
        }

        SoundStatus(soundStatus);
        PlayerPrefs.SetInt("Sound State", System.Convert.ToInt32(soundStatus));
    }

    public void SoundStatus(bool soundOff)
    {
        if (soundOff == false)
        {
            for(int i = 0; i < soundBtns.Length; i++)
            {
                soundBtns[i].GetComponentInChildren<Text>().text = "Sound ON";
            }
            AudioListener.pause = soundOff;
        }
        else
        {
            for (int i = 0; i < soundBtns.Length; i++)
            {
                soundBtns[i].GetComponentInChildren<Text>().text = "Sound OFF";
            }
            AudioListener.pause = soundOff;
        }

    }

    public void SelectButtonClicked()
    {
        if (modeStatus == true)
        {
            modeStatus = false;
        }
        else
        {
            modeStatus = true;
        }

        SelectMode(modeStatus);
        PlayerPrefs.SetInt("Game Mode", System.Convert.ToInt32(modeStatus));
    }

    public void SelectMode(bool modeChange)
    {
        if (modeChange == false)
        {
            
            changeModeBtn.GetComponentInChildren<Text>().text = "Mode Accel";
            GameManager.Instance.playerGameObject.GetComponent<BikeControl>().controlMode = ControlBikeMode.accelerometer;
        }
        else
        {

            changeModeBtn.GetComponentInChildren<Text>().text = "Mode Btns";
            GameManager.Instance.playerGameObject.GetComponent<BikeControl>().controlMode = ControlBikeMode.touch;

        }

    }

    public void CloseTutorialPanel(GameObject TutorialScreen)
    {
        TutorialScreen.SetActive(false);
        gamePlayScreen.SetActive(true);

        Time.timeScale = 1f;

        if(TutorialScreen == speedTutorialPnl)
        {
            GameManager.Instance.speedTutorial = false;
            GameManager.Instance.speedTutorialIndex = 1;
            
            PlayerPrefs.SetInt("Speed Tutorial", GameManager.Instance.speedTutorialIndex);
        }

        else if (TutorialScreen == bombDiffuseTutorialPnl)
        {
            GameManager.Instance.diffuseTutorial = false;
            GameManager.Instance.diffuseTutorialIndex = 1;
            
            PlayerPrefs.SetInt("Diffuse Tutorial", GameManager.Instance.diffuseTutorialIndex);
        }

        else if(TutorialScreen == mosquitoStartTutorialPnl)
        {
            GameManager.Instance.mosquitoSpawner.SpawnTutorialMosquito(0);
        }

        else if(TutorialScreen == mosquitoEndTutorialPnl)
        {
            GameManager.Instance.mosquitoTutorial = false;
            GameManager.Instance.mosquitoTutorialIndex = 1;
            
            PlayerPrefs.SetInt("Mosquito Tutorial", GameManager.Instance.mosquitoTutorialIndex);
        }
        
        else if(TutorialScreen == shieldTutorialPnl)
        {
            GameManager.Instance.shieldTutorial = false;
            GameManager.Instance.shieldTutorialIndex = 1;
            
            PlayerPrefs.SetInt("Shield Tutorial", GameManager.Instance.shieldTutorialIndex);
        }
    }

    public void ResumeGameBtn()
    {
        SoundManager.Instance.btnSound.Play();

        GameManager.Instance.isPaused = false;
        Time.timeScale = resumeTimeScaleValue;
        gamePauseScreen.SetActive(false);
        gamePlayScreen.SetActive(true);

        AdsManager.Instance.bannerScript.HideBannerAd();
    }

    public void RestartGameBtn()
    {
        SoundManager.Instance.btnSound.Play();

        if (GameManager.Instance.isPaused)
        {
            GameManager.Instance.isPaused = false;
            Time.timeScale = resumeTimeScaleValue;
        }

        score = 0f;
        ResetRewardText();

        PathManager Go;

        Go = GameManager.Instance.pathManager.GetComponent<PathManager>();

        GameObject firstPathClone = Instantiate(Go.firstpath, Go.activeTunel[0].transform.position, Go.activeTunel[0].transform.rotation);
        GameObject originalFirstPath = Go.activeTunel[0];

        Go.activeTunel[0] = firstPathClone;

        Destroy(originalFirstPath);

        if (GameManager.Instance.playerInDiffuseBombSection)
            GameManager.Instance.playerGameObject.GetComponent<CollisionDetector>().EndDiffuseBombSection();

        if (GameManager.Instance.mosquitoSpawn)
            GameManager.Instance.playerGameObject.GetComponent<CollisionDetector>().EndMosquitoSection();

        if (GameManager.Instance.playerInSewerage)
            GameManager.Instance.playerGameObject.GetComponent<CollisionDetector>().EndSeverageSection();

        if (GameManager.Instance.playerInTrainSection)
        {
            GameManager.Instance.playerGameObject.GetComponent<CollisionDetector>().EndTrainSection();
            GameManager.Instance.trafficGeneratorScript.ClearTrainSpawnersList();
        }

        if (GameManager.Instance.playerInSpeedSection)
            GameManager.Instance.playerGameObject.GetComponent<CollisionDetector>().EndSpeedSection();

        GameManager.Instance.playerGameObject.GetComponent<CollisionDetector>().ResetPlayerSettings();
        GameManager.Instance.playerGameObject.transform.position = new Vector3(Go.activeTunel[0].transform.position.x, GameManager.Instance.playerGameObject.transform.position.y, Go.activeTunel[0].transform.position.z);

        GameObject[] gameObjects0 = GameObject.FindGameObjectsWithTag("NPC");
        GameObject[] gameObjects1 = GameObject.FindGameObjectsWithTag("NPC2");

        for (int j = 0; j < gameObjects0.Length; j++)
        {
            if (gameObjects0[j] != null)
            {
                Destroy(gameObjects0[j].gameObject);
            }
        }

        for (int k = 0; k < gameObjects1.Length; k++)
        {
            if (gameObjects1[k] != null)
            {
                Destroy(gameObjects1[k].gameObject);
            }
        }

        cashBagIcon.SetActive(false);

        ShowHeadStartBtn();

        AdsManager.Instance.bannerScript.HideBannerAd();
    }

    public void ResetRewardText()
    {
        rewardText.GetComponent<Text>().text = null;
    }

    public void RestartSceneBtn()
    {
        SoundManager.Instance.btnSound.Play();

        GameManager.Instance.isPaused = false;
        SceneManager.LoadScene(0);

        if (addIndex >= 2 && AdsManager.Instance.adsDisabled == false)
        {
            //InterstitialAd.Instance.ShowAd();

            AdsManager.Instance.interstitialScript.ShowAd();

            Debug.Log("interstitial ad called");
            
            SceneManager.LoadScene(0);
            Time.timeScale = resumeTimeScaleValue;

            addIndex = 0;
        }
        else
        {
            SceneManager.LoadScene(0);
            Time.timeScale = resumeTimeScaleValue;
            
            if (AdsManager.Instance.adsDisabled == false)
            {
                addIndex++; 
            }
        }

        PlayerPrefs.SetInt("Show Ads", addIndex);
    }

    public void GameOver()
    {
        StartCoroutine(GameOverDelay());
    }

    IEnumerator GameOverDelay()
    {
        yield return new WaitForSeconds(5f);

        if (gamePlayScreen.activeSelf == true)
        {
            gamePlayScreen.SetActive(false);
        }
        
        gameOverScreen.SetActive(true);

        if(reviveBtnIndex == 3 && AdsManager.Instance.adsDisabled == false)
        {
            Debug.Log("show revive btn");
            reviveBtn.SetActive(true);
            reviveBtnIndex = 0;
            PlayerPrefs.SetInt("Show Revive Btn", reviveBtnIndex);
        }
        else
        {
            Debug.Log("dont show revive btn: " + reviveBtnIndex);
            reviveBtn.SetActive(false);
            if (AdsManager.Instance.adsDisabled == false)
            {
                reviveBtnIndex++;
                PlayerPrefs.SetInt("Show Revive Btn", reviveBtnIndex);
            }
        }

        if (highScore < score)
        {
            PlayerPrefs.SetFloat("HighScore", score);
            gameOverHighScoreTxt.text = "NEW HIGHSCORE !!!";
        }
        else
        {
            gameOverHighScoreTxt.text = "HIGHSCORE: " + highScore.ToString("0");
        }

        money = CashConverter();
        totalMoney += money;

        gameOverScoreTxt.text = "Your Score: " + score.ToString("0");
        receivedMoneyTxt.text = "MONEY RECEIVED: $" + money.ToString("0");
        
        //PlayerPrefs.SetFloat("Money", totalMoney);
        UpdateAllCoinsUIText();

        bagsObtainedTxt.text = "Bags Obtained: " + GameManager.Instance.cashBagAmount.ToString("0");
        GameManager.Instance.cashBagAmount = 0;
        totalMoney += moneyInBags;
        moneyInBags = 0;
        
        UpdateAllCoinsUIText();
        PlayerPrefs.SetFloat("Money", totalMoney);

        AdsManager.Instance.bannerScript.ShowBannerAd();
    }

    public void ShowHeadStartBtn()
    {
        if (GameManager.Instance.headStartAmount > 0)
        {
            headStartBtn.SetActive(true);
            
            headStartInGameTxt.text = "X" + GameManager.Instance.headStartAmount.ToString("0");

            StartCoroutine(HeadStartDissappear());
        }
        else
        {
            headStartBtn.SetActive(false);
        }
    }

    IEnumerator HeadStartDissappear()
    {
        yield return new WaitForSeconds(5);
        headStartBtn.SetActive(false);
    }

    public void OpenShopBtn()
    {
        SoundManager.Instance.btnSound.Play();

        StartCoroutine(DelayShopOpening());

    }

    IEnumerator DelayShopOpening()
    {
        shutterAnimation.SetTrigger("ShutterDown");

        yield return new WaitForSeconds(0.75f);

        mainMenuScreen.SetActive(false);
        shopScreen.SetActive(true);

        UpdateAllCoinsUIText();

        lastScreen = bikeSection;
        bikeSection.SetActive(true);

        bikeCamera = true;

        mainCam.GetComponent<CameraFollow>().enabled = false;
    }

    public void OpenIAPScreen()
    {
        shopSubScreen.SetActive(false);
        lastScreen.SetActive(false);
        iAPScreen.SetActive(true);
    }

    public void CloseIAPScreen()
    {
        shopSubScreen.SetActive(true);
        lastScreen.SetActive(true);
        iAPScreen.SetActive(false);
    }

    public void ChangeShopSection(int sectionIndex)
    {
        SoundManager.Instance.btnSound.Play();

        if (sectionIndex == 0)
        {
            lastScreen = bikeSection;

            bikeCamera = true;
            powerupsCamera = false;
            SkinCamera = false;

            bikeSection.SetActive(true);
            powerUpsSection.SetActive(false);
            skinSection.SetActive(false);
        }
        else if (sectionIndex == 1)
        {
            lastScreen = powerUpsSection;

            bikeCamera = false;
            powerupsCamera = true;
            SkinCamera = false;

            bikeSection.SetActive(false);
            powerUpsSection.SetActive(true);
            skinSection.SetActive(false);

            UpdatePowerUpsText();
        }
        else if (sectionIndex == 2)
        {
            lastScreen = skinSection;

            bikeCamera = false;
            powerupsCamera = false;
            SkinCamera = true;

            bikeSection.SetActive(false);
            powerUpsSection.SetActive(false);
            skinSection.SetActive(true);
        }
    } 

    public void UpdatePowerUpsText()
    {
        fuelAmountTxt.text = "x" + GameManager.Instance.fuelAmount.ToString();
        shieldAmountTxt.text = "x" + GameManager.Instance.shieldAmount.ToString();
        headStartTxt.text = "x" + GameManager.Instance.headStartAmount.ToString();
    }

    public void NextBike()
    {
        if (Shop.ShopSection.bikeSection == ShopController.Instance.bikesShopScript.shopSection && bikeSection.activeSelf)
        {
            ShopController.Instance.bikesShopScript.ChangeTemplateForward();
            shopBikes[bikeIndex].SetActive(false);
            bikeIndex++;
            if (bikeIndex == shopBikes.Length)
            {
                bikeIndex = 0;
            }
            shopBikes[bikeIndex].SetActive(true);
        }
        else if(Shop.ShopSection.powerUpsSection == ShopController.Instance.powerUpsShopScript.shopSection && powerUpsSection.activeSelf)
        {

            ShopController.Instance.powerUpsShopScript.ChangeTemplateForward();
            shopPowerUps[powerUpsIndex].SetActive(false);
            powerUpsIndex++;
            if (powerUpsIndex == shopPowerUps.Length)
            {
                powerUpsIndex = 0;
            }
            shopPowerUps[powerUpsIndex].SetActive(true);

        }
        else if (Shop.ShopSection.skinSections == ShopController.Instance.skinsShopScript.shopSection && skinSection.activeSelf)
        {

            ShopController.Instance.skinsShopScript.ChangeTemplateForward();
            shopSkins[skinIndex].SetActive(false);
            skinIndex++;
            if (skinIndex == shopSkins.Length)
            {
                skinIndex = 0;
            }
            shopSkins[skinIndex].SetActive(true);
        }
    }

    public void PreviousBike()
    {
        if (ShopController.Instance.bikesShopScript.shopSection == Shop.ShopSection.bikeSection && bikeSection.activeSelf)
        {
            ShopController.Instance.bikesShopScript.ChangeTemplateBackward();
            shopBikes[bikeIndex].SetActive(false);
            bikeIndex--;
            if (bikeIndex < 0)
            {
                bikeIndex = shopBikes.Length - 1;
            }
            shopBikes[bikeIndex].SetActive(true);
        }
        else if (ShopController.Instance.powerUpsShopScript.shopSection == Shop.ShopSection.powerUpsSection && powerUpsSection.activeSelf)
        {
            Debug.Log("next item");
            ShopController.Instance.powerUpsShopScript.ChangeTemplateBackward();
            shopPowerUps[powerUpsIndex].SetActive(false);
            powerUpsIndex--;
            if (powerUpsIndex < 0)
            {
                powerUpsIndex = shopPowerUps.Length - 1;
            }
            shopPowerUps[powerUpsIndex].SetActive(true);

        }
        else if (ShopController.Instance.skinsShopScript.shopSection == Shop.ShopSection.skinSections && skinSection.activeSelf)
        {
            ShopController.Instance.skinsShopScript.ChangeTemplateBackward();
            Debug.Log("next skin");
            shopSkins[skinIndex].SetActive(false);
            skinIndex--;
            if (skinIndex < 0)
            {
                skinIndex = shopSkins.Length - 1;
            }
            shopSkins[skinIndex].SetActive(true);
        }
    }

    public void SelectItem(int SelectedItemIndex)
    {
        SoundManager.Instance.btnSound.Play();

        if (ShopController.Instance.bikesShopScript.shopSection == Shop.ShopSection.bikeSection && bikeSection.activeSelf)
        {
            for (int i = 0; i < selectedBikes.Length; i++)
            {
                if (i == SelectedItemIndex)
                {
                    
                    ShopController.Instance.bikesShopScript.selectBtns[i].transform.GetComponentInChildren<Text>().text = "SELECTED";
                    ShopController.Instance.bikesShopScript.selectBtns[i].GetComponent<Button>().interactable = false;
                    
                    selectedBikes[i].SetActive(true);
                    GameManager.Instance.Player(i);
                    GameManager.Instance.ChangePlayer();

                    Debug.Log(GameManager.Instance.Player(i).name);

                    GameManager.Instance.ChangeSkin(PlayerPrefs.GetInt("SkinSelected"));


                    PlayerPrefs.SetInt("Bike Selected", i);
                }
                else
                {
                    ShopController.Instance.bikesShopScript.selectBtns[i].transform.GetChild(0).GetComponent<Text>().text = "SELECT";
                    ShopController.Instance.bikesShopScript.selectBtns[i].GetComponent<Button>().interactable = true;
                    
                    selectedBikes[i].SetActive(false);
                }
            }
        }
        else if(ShopController.Instance.bikesShopScript.shopSection == Shop.ShopSection.powerUpsSection)
        {
            Debug.Log("selected");
        }
        else if(ShopController.Instance.skinsShopScript.shopSection == Shop.ShopSection.skinSections && skinSection.activeSelf)
        {
            for (int i = 0; i < shopSkins.Length; i++)
            {
                if (i == SelectedItemIndex)
                {
                    ShopController.Instance.skinsShopScript.selectBtns[i].transform.GetComponentInChildren<Text>().text = "SELECTED";
                    ShopController.Instance.skinsShopScript.selectBtns[i].GetComponent<Button>().interactable = false;
                    
                    shopSkins[i].GetComponent<Animator>().SetTrigger("GotSelected");
                    
                    GameManager.Instance.ChangeSkin(i);
                    
                    PlayerPrefs.SetInt("SkinSelected", i);
                }
                else
                {
                    ShopController.Instance.skinsShopScript.selectBtns[i].transform.GetChild(0).GetComponent<Text>().text = "SELECT";
                    ShopController.Instance.skinsShopScript.selectBtns[i].GetComponent<Button>().interactable = true;
                }
            }
        }
    }

    public void DefaultBike(int SelectedBikeIndex)
    {
        for (int i = 0; i < selectedBikes.Length; i++)
        {
            if (i == SelectedBikeIndex)
            {
                ShopController.Instance.bikesShopScript.selectBtns[i].transform.GetComponentInChildren<Text>().text = "SELECTED";
                ShopController.Instance.bikesShopScript.selectBtns[i].GetComponent<Button>().interactable = false;

                selectedBikes[i].SetActive(true);

                GameManager.Instance.ChangePlayer();
            }
            else
            {
                ShopController.Instance.bikesShopScript.selectBtns[i].transform.GetChild(0).GetComponent<Text>().text = "SELECT";
                ShopController.Instance.bikesShopScript.selectBtns[i].GetComponent<Button>().interactable = true;

                selectedBikes[i].SetActive(false);
            }
        }
    }

    public void DefaultSkin(int SelectedSkinIndex)
    {
        for (int i = 0; i < shopSkins.Length; i++)
        {
            if ( i == SelectedSkinIndex)
            {
                ShopController.Instance.skinsShopScript.selectBtns[i].transform.GetComponentInChildren<Text>().text = "SELECTED";
                ShopController.Instance.skinsShopScript.selectBtns[i].GetComponent<Button>().interactable = false;

                GameManager.Instance.ChangeSkin(i);
            }
            else
            {
                ShopController.Instance.skinsShopScript.selectBtns[i].transform.GetChild(0).GetComponent<Text>().text = "SELECT";
                ShopController.Instance.skinsShopScript.selectBtns[i].GetComponent<Button>().interactable = true;
            }
        }
    }

    public void ExitShopBtn()
    {
        SoundManager.Instance.btnSound.Play();

        StartCoroutine(DelayShopExit());
    }

    IEnumerator DelayShopExit()
    {
        shutterAnimation.SetTrigger("ShutterDown");
        
        yield return new WaitForSeconds(0.75f);
        
        shopScreen.SetActive(false);
        mainMenuScreen.SetActive(true);

        //bikeSection.SetActive(true);

        bikeCamera = false;
        powerupsCamera = false;
        SkinCamera = false;

        mainCam.transform.SetPositionAndRotation(originalCamPos.position, originalCamPos.rotation);
        mainCam.GetComponent<CameraFollow>().enabled = true;
    }

    public void HighScoreFunction()
    {
        score += .5f * Time.deltaTime;
        scoreTxt.text = "Score: " + (int)score;
        //if (highScore < score && firstGameIndex == 1)
        //{
        //    newHighScoreTxt.text = "NEW HIGHSCORE!!!";
        //    newHighScoreAnimation.SetTrigger("Reward");
        //}
    }

    public void AddScore(float bonusScore)
    {
        score += bonusScore;
        scoreTxt.text = "Score: " + (int)score;
    }

    public float CashConverter()
    {
        float money = Random.Range(minPercentageMoney, maxPercentageMoney) * score;
        return money;
    }

    public void UpdateAllCoinsUIText()
    {
        for (int i = 0; i < allCashUIText.Length; i++)
        {
            allCashUIText[i].text = "Total Money: $" + totalMoney.ToString("0");
        }
    }
}
