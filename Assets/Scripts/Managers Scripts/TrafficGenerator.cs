﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficGenerator : MonoBehaviour
{
    [SerializeField] GameObject[] onGoingRightPrefab;
    [SerializeField] GameObject[] onComingLeftPrefab;
    [SerializeField] GameObject[] onGoingLeftPrefab;
    [SerializeField] GameObject[] onComingRightPrefab;

    [SerializeField] GameObject trainPrefab;

    [SerializeField] Transform spawnGoingRight;
    [SerializeField] Transform spawnComingLeft;
    [SerializeField] Transform spawnGoingLeft;
    [SerializeField] Transform spawnComingRight;
    
    //[SerializeField] Transform newGoingRightTarget, newComingLeftTarget, newGoingLeftTarget, newComingRightTarget; ---- scraped
    
    //Transform oldGoingRightTarget, oldComingLeftTarget, oldGoingLeftTarget, oldComingRightTarget;  ---- scraped
    
    public Transform comingParent;
    public Transform goingParent;
    
    public bool hasGameStarted = true;
    
    public bool enteredExpandedLane;
    public bool exitedExpandedLane;

    //public bool isActive;

    public List<Transform> trafficSpawnPoints;

    //GameObject spawnedTraffic;
    
    //BikeControl bikeScript;

    // Start is called before the first frame update
    void Start()
    {
        /*trafficSpawnPoints.Add(spawnGoingRight.transform);
        trafficSpawnPoints.Add(spawnComingLeft.transform);
        trafficSpawnPoints.Add(spawnGoingLeft.transform);
        trafficSpawnPoints.Add(spawnComingRight.transform);*/
        

        //bikeScript = GameManager.Instance.playerGameObject.GetComponent<BikeControl>();

        //spawnComing.position = new Vector3(-6.5f,2.5f,90);
        //spawnGoing.position = new Vector3(0,2.5f,80);

        hasGameStarted = true;

        //GameManager.Instance.gameMode.OneWay

        StartCoroutine(ComingLeftTrafficGeneration());
        StartCoroutine(GoingRightTrafficGeneration());
        StartCoroutine(GoingLeftTrafficGeneration());
        StartCoroutine(ComingRightTrafficGeneration());

        //GameObject gameObject1 = Instantiate(onGoingPrefab[Random.Range(0, onGoingPrefab.Length)], spawnGoing.position, Quaternion.identity);
        
        //scraped
        /*oldGoingRightTarget.position = spawnGoingRight.transform.position;
        oldComingLeftTarget.position = spawnComingLeft.transform.position;
        oldGoingLeftTarget.position = spawnGoingLeft.transform.position;
        oldComingRightTarget.position = spawnComingRight.transform.position;*/
    }

    /*private void Update()
    {
        //Scraped
       if(enteredExpandedLane && isActive)
        {
            isActive = false;
            spawnGoingRight.transform.position = newGoingRightTarget.position;
            spawnComingLeft.transform.position = newComingLeftTarget.position;
            spawnGoingLeft.transform.position = newGoingLeftTarget.position;
            spawnComingRight.transform.position = newComingRightTarget.position;
        }
        else if(exitedExpandedLane && isActive)
        {
            isActive = false;
            spawnGoingRight.transform.position = oldGoingRightTarget.position;
            spawnComingLeft.transform.position = oldComingLeftTarget.position;
            spawnGoingLeft.transform.position = oldGoingLeftTarget.position;
            spawnComingRight.transform.position = oldComingRightTarget.position;
        }
    }*/

    void SpawnComingLeftTraffic()
    {
        //if (GameManager.Instance.tutorialPlaying)
        //{
        //    return;
        //}

        if (GameManager.Instance.hasGameStarted)
        {
            GameObject gameObject1 = Instantiate(onComingLeftPrefab[Random.Range(0, onComingLeftPrefab.Length)], 
                /*spawnComingLeft.position*/ new Vector3(spawnComingLeft.position.x, spawnComingLeft.position.y + 0.2f, spawnComingLeft.position.z), 
                Quaternion.Euler(0, 180, 0));
            if (spawnComingLeft.transform.position == gameObject1.transform.position)
            {
                Vector3 temp = gameObject1.transform.position;
                temp.z += 2;
                gameObject1.transform.position = temp;
            }
            gameObject1.transform.SetParent(comingParent);
            //Debug.Log("Helloo left here");
        }
        else
        {
            return;
        }
    }

    void SpawnComingRightTraffic()
    {
        //if (GameManager.Instance.tutorialPlaying)
        //{
        //    return;
        //}

        if (GameManager.Instance.hasGameStarted)
        {
            GameObject gameObject2 = Instantiate(
                onComingRightPrefab[Random.Range(0, onComingRightPrefab.Length)], 
                /*spawnComingRight.position*/new Vector3(spawnComingRight.position.x, spawnComingRight.position.y + 0.2f, spawnComingRight.position.z), 
                Quaternion.Euler(0, 180, 0));
            if (spawnComingRight.transform.position == gameObject2.transform.position)
            {
                Vector3 temp = gameObject2.transform.position;
                temp.z += 20;
                gameObject2.transform.position = temp;
            }
            gameObject2.transform.SetParent(comingParent);
        }
        else
        {
            return;
        }
    }

    void SpawnGoingRightTraffic()
    {
        /*if (GameManager.Instance.tutorialPlaying)
        {
            return;
        }*/

        if (GameManager.Instance.hasGameStarted)
        {

            GameObject gameObject3 = Instantiate(
                onGoingRightPrefab[Random.Range(0, onGoingRightPrefab.Length)],
                /*spawnGoingRight.position*/ new Vector3(spawnGoingRight.position.x, spawnGoingRight.position.y + 0.2f, spawnGoingRight.position.z),
                Quaternion.identity);

            //spawnedTraffic = GameObject.FindGameObjectWithTag("NPC");
            if(spawnGoingRight.transform.position == gameObject3.transform.position)
            {
                Vector3 temp = gameObject3.transform.position;
                temp.z += 20;
                gameObject3.transform.position = temp;
            }
            gameObject3.transform.SetParent(goingParent);
        }
        else
        {
            return;
        }
    }

    void SpawnGoingLeftTraffic()
    {
        //if (GameManager.Instance.tutorialPlaying)
        //{
        //    return;
        //}

        if (GameManager.Instance.hasGameStarted)
        {

            GameObject gameObject4 = Instantiate(
                onGoingLeftPrefab[Random.Range(0, onGoingLeftPrefab.Length)],
                /*spawnGoingLeft.position*/ new Vector3(spawnGoingLeft.position.x, spawnGoingLeft.position.y + 0.2f, spawnGoingLeft.position.z), 
                Quaternion.identity);

            //spawnedTraffic = GameObject.FindGameObjectWithTag("NPC");
            if(spawnGoingRight.transform.position == gameObject4.transform.position)
            {
                Vector3 temp = gameObject4.transform.position;
                temp.z += 20;
                gameObject4.transform.position = temp;
            }
            gameObject4.transform.SetParent(goingParent);

        }
        else
        {
            return;
        }
    }
    
    public IEnumerator ComingLeftTrafficGeneration()
    {
        if (!GameManager.Instance.firstTutorial && !GameManager.Instance.playerInDiffuseBombSection && !GameManager.Instance.playerCrashed && !GameManager.Instance.playerInSewerage && !GameManager.Instance.playerInTrainSection)
        {
            if (hasGameStarted == true)
            {
                yield return new WaitForSeconds(1f);
                hasGameStarted = false;
            }

            SpawnComingLeftTraffic();
        }
        
        yield return new WaitForSeconds(Random.Range(2, 7));
        StartCoroutine(ComingLeftTrafficGeneration());
    }

    public IEnumerator ComingRightTrafficGeneration()
    {
        if (!GameManager.Instance.firstTutorial && !GameManager.Instance.playerInDiffuseBombSection && !GameManager.Instance.playerCrashed && !GameManager.Instance.playerInSewerage && !GameManager.Instance.playerInTrainSection)
        {
            SpawnComingRightTraffic();
        }
            yield return new WaitForSeconds(Random.Range(4, 10));
            StartCoroutine(ComingRightTrafficGeneration());
    }

    public IEnumerator GoingLeftTrafficGeneration()
    {
        if (!GameManager.Instance.firstTutorial && !GameManager.Instance.playerInDiffuseBombSection && !GameManager.Instance.playerCrashed && !GameManager.Instance.playerInSewerage && !GameManager.Instance.playerInTrainSection)
        {
            if (/*GameManager.Instance.gamePlaying &&*/ hasGameStarted == true)
            {
                yield return new WaitForSeconds(1f);
                hasGameStarted = false;
            }

            SpawnGoingLeftTraffic();
        }
            yield return new WaitForSeconds(Random.Range(2, 7));
            StartCoroutine(GoingLeftTrafficGeneration());
    }

    public IEnumerator GoingRightTrafficGeneration()
    {
        if (!GameManager.Instance.firstTutorial && !GameManager.Instance.playerInDiffuseBombSection && !GameManager.Instance.playerCrashed && !GameManager.Instance.playerInSewerage && !GameManager.Instance.playerInTrainSection)
        {
            SpawnGoingRightTraffic();
        }
            yield return new WaitForSeconds(Random.Range(4, 10));
            StartCoroutine(GoingRightTrafficGeneration());
    }

    public IEnumerator TrainComing()
    {

        if (GameManager.Instance.playerInTrainSection && GameManager.Instance.playerCrashed == false)
        {
            int spawnIndex = Random.Range(0, trafficSpawnPoints.Count);
            GameObject trainClone = Instantiate(trainPrefab, 
                new Vector3(trafficSpawnPoints[spawnIndex].position.x,trafficSpawnPoints[spawnIndex].position.y + 0.2f, trafficSpawnPoints[spawnIndex].position.z),
                Quaternion.Euler(0, 180, 0));

            if (trafficSpawnPoints[spawnIndex].position == trainClone.transform.position)
            {
                Vector3 temp = trainClone.transform.position;
                
                temp.z += 20;
                
                //temp.z += trainClone.transform.position.z;
                
                trainClone.transform.position = temp;
                trainClone.transform.SetParent(comingParent);
            }
        }
        else
        {
            ///////
        }

        yield return new WaitForSeconds(Random.Range(0,10));

        StartCoroutine(TrainComing());

    }


    public void ClearTrainSpawnersList() 
    {
        trafficSpawnPoints.Clear();   
    }
}

