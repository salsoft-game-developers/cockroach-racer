﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton Class: GameManager

    public static GameManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        playerIndex = PlayerPrefs.GetInt("Bike Selected", 0);
        Player(playerIndex);

    }

    #endregion

    public GameObject[] collectiblePrefabs;
    public GameObject[] Spawners;

    public GameObject goingTrafficObjects;
    public GameObject playerGameObject;
    public GameObject playerHolder;
    public GameObject pathManager;
    public GameObject leftLaneForwardPrefab, rightLaneForwardPrefab;

    public Transform bombPos;
    public Transform playerPoint;

    public CollisionDetector collisionDetectorScript;

    public TrafficGenerator trafficGeneratorScript;

    public MosquitoSpawner mosquitoSpawner;

    public Material[] bodyMaterial;
    public Material[] tailMaterial;
    public Material[] wingsMaterial;

    public ParticleSystem speedEffect;

    public bool hasGameStarted;
    public bool isPaused;
    public bool mosquitoSpawn;
    public bool playerInSpeedSection;
    public bool playerInDiffuseBombSection;
    public bool playerInSewerage;
    public bool playerInTrainSection;
    public bool hasShieldActivated;
    public bool hasHeadStartActivated;
    public bool playerCrashed;
    public bool isReviveBtnPressed;
    public bool firstTutorial, mosquitoTutorial, speedTutorial, diffuseTutorial, shieldTutorial;

    public float fuelTimer;
    public float shieldTimer;
    public float headStartTimer;
    public float bombTimer;
    public float fuelAmount;

    public int shieldAmount;
    public int headStartAmount;
    public int cashBagAmount;
    public int firstTutorialIndex, mosquitoTutorialIndex, speedTutorialIndex, diffuseTutorialIndex, shieldTutorialIndex;
    public int playerIndex;

    private float timer;


    public void spawnMosquitoes()
    {
        mosquitoSpawn = true;
    }


    private void Start()
    {
        //Player();

        //ChangeSkin(PlayerPrefs.GetInt("SkinSelected"));

        hasGameStarted = false;

        firstTutorialIndex = PlayerPrefs.GetInt("Gameplay Tutorial", 0);
        mosquitoTutorialIndex = PlayerPrefs.GetInt("Mosquito Tutorial", 0);
        speedTutorialIndex = PlayerPrefs.GetInt("Speed Tutorial", 0);
        diffuseTutorialIndex = PlayerPrefs.GetInt("Diffuse Tutorial", 0);
        shieldTutorialIndex = PlayerPrefs.GetInt("Shield Tutorial", 0);

        if (firstTutorialIndex == 1)
        {
            firstTutorial = false;
        }

        if (mosquitoTutorialIndex == 1)
        {
            mosquitoTutorial = false;
        }

        if(speedTutorialIndex == 1)
        {
            speedTutorial = false;
        }

        if(diffuseTutorialIndex == 1)
        {
            diffuseTutorial = false;
        }

        if(shieldTutorialIndex == 1)
        {
            shieldTutorial = false;
        }

        shieldAmount = PlayerPrefs.GetInt("Shield", shieldAmount);
        headStartAmount = PlayerPrefs.GetInt("HeadStart", headStartAmount);
        
        fuelAmount = PlayerPrefs.GetFloat("Fuel", fuelAmount);
        fuelTimer = PlayerPrefs.GetFloat("FuelTimer", fuelTimer);
        shieldTimer = PlayerPrefs.GetFloat("ShieldTimer", shieldTimer);
        headStartTimer = PlayerPrefs.GetFloat("HeadShieldTimer", headStartTimer);

        timer = headStartTimer;
        mosquitoSpawn = false;

        firstTutorial = true;
    }

    public void ActivateHeadStart()
    {
        headStartAmount--;
        
        UIManager.Instance.headStartInGameTxt.text = "X" + headStartAmount.ToString("");
        
        PlayerPrefs.SetInt("HeadStart", headStartAmount);
        
        headStartTimer = timer;
        hasHeadStartActivated = true;
        
        UIManager.Instance.headStartBtn.SetActive(false);
    }

    public void ChangeSkin(int index)
    {
        //GameObject RagdollObject = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<CollisionDetector>().ragdollModel;

        //RagdollObject.SetActive(true);

        GameObject[] body = GameObject.FindGameObjectsWithTag("Body");
        GameObject[] tail = GameObject.FindGameObjectsWithTag("Tail");
        GameObject[] wings = GameObject.FindGameObjectsWithTag("Wings");

        for (int i = 0; i < body.Length; i++)
        {
            body[i].GetComponent<Renderer>().sharedMaterial = bodyMaterial[index];
        }

        for (int j = 0; j < tail.Length; j++)
        {
            tail[j].GetComponent<Renderer>().sharedMaterial = tailMaterial[index];
        }

        for (int k = 0; k < wings.Length; k++)
        {
           wings[k].GetComponent<Renderer>().sharedMaterial = wingsMaterial[index];
        }

        //RagdollObject.SetActive(false);
    }

    public GameObject Player(int PlayerIndex)
    {
        playerIndex = PlayerIndex;
        playerGameObject = GameObject.FindGameObjectWithTag("Player " + playerIndex);
        
        collisionDetectorScript = playerGameObject.GetComponent<CollisionDetector>();
        
        return playerGameObject;
    }

    public void ChangePlayer()
    {
        Player(playerIndex);

        playerHolder.GetComponent<HolderMovement>().ResetHolderTarget();

        Camera.main.GetComponent<BikeCamera>().ChangeCameraTargets();

        Camera.main.GetComponent<CameraFollow>().ChangeCameraTargets();

        //playerHolder.GetComponent<HolderMovement>().bike = playerGameObject.transform;

    }

    private void Update()
    {
        if (isReviveBtnPressed)
        {
            isReviveBtnPressed = false;
            collisionDetectorScript.ReviveBtnPressed();
        }
    }

    //    public bool gamePlaying;
    //    public bool onLeft = false;

    //    public bool tutorialPlaying;

    //    public Transform spawnRightGoing;
    //    public Transform spawnLeftComing;
    //    public Transform spawnLeftGoing;
    //    public Transform spawnRightComing;

    //    public GameObject rightGoingTarget;
    //    public GameObject leftComingTarget;
    //    public GameObject leftGoingTarget;
    //    public GameObject rightComingTarget;

    //    public GameObject[] pathManagers;

    //    [SerializeField] Material[] materials;

    //    [SerializeField] ParticleSystem[] particleSystems;

    //    [SerializeField] TrafficGenerator trafficGenerator;

    //    //[SerializeField] PlayerMovement player;


    //    public bool isOneWay;
    //    public bool isTwoWay;
    //    public bool isWrongWay;

    //    //[SerializeField] GameObject normalPathManager;
    //    //[SerializeField] GameObject snowyPathManager;
    //    //[SerializeField] GameObject rainyPathManager;

    //    public enum GameModes
    //    {
    //        OneWay = 0,
    //        TwoWay = 1,
    //        WrongWay = 2
    //    }

    //    public enum GameLocations
    //    {
    //        NormalMap = 0,
    //        SnowyMap = 1,
    //        RainyMap = 2,
    //        BeachMap = 3
    //    }

    //    //public GameModes gameMode;

    //    // Start is called before the first frame update
    //    void Start()
    //    {
    //        gamePlaying = false;
    //        pathManagers[0].SetActive(true);
    //        //player = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<PlayerMovement>();
    //    }

    //    public void SelectMode(GameModes gameMode)
    //    {

    //        switch (gameMode)
    //        {
    //            case GameModes.OneWay:
    //                OneWayMode();
    //                break;

    //            case GameModes.TwoWay:
    //                TwoWayMode();
    //                break;

    //            case GameModes.WrongWay:
    //                WrongWayMode();
    //                break;

    //        }
    //    }

    //    public void OneWayMode()
    //    {
    //        isOneWay = true;
    //        isTwoWay = false;
    //        isWrongWay = false;
    //    }

    //    public void TurnOneWayOn()
    //    {
    //        spawnLeftComing.gameObject.SetActive(false);
    //        spawnRightComing.gameObject.SetActive(false);

    //        spawnLeftGoing.gameObject.SetActive(true);
    //        spawnRightGoing.gameObject.SetActive(true);

    //        leftComingTarget.SetActive(false);
    //        rightComingTarget.SetActive(false);

    //        leftGoingTarget.SetActive(true);
    //        rightGoingTarget.SetActive(true);

    //        StartCoroutine(trafficGenerator.GoingRightTrafficGeneration());
    //        StartCoroutine(trafficGenerator.GoingLeftTrafficGeneration());
    //    }

    //    public void TwoWayMode()
    //    {
    //        isOneWay = false;
    //        isTwoWay = true;
    //        isWrongWay = false;
    //    }

    //    public void TurnTwoWayOn()
    //    {
    //        spawnLeftGoing.gameObject.SetActive(false);
    //        spawnRightComing.gameObject.SetActive(false);

    //        spawnLeftComing.gameObject.SetActive(true);
    //        spawnRightGoing.gameObject.SetActive(true);

    //        leftGoingTarget.SetActive(false);
    //        rightComingTarget.SetActive(false);

    //        leftComingTarget.SetActive(true);
    //        rightComingTarget.SetActive(true);

    //        StartCoroutine(trafficGenerator.GoingRightTrafficGeneration());
    //        StartCoroutine(trafficGenerator.ComingLeftTrafficGeneration());
    //    }

    //    public void WrongWayMode()
    //    {
    //        isOneWay = false;
    //        isTwoWay = false;
    //        isWrongWay = true;
    //    }

    //    public void TurnWrongWayOn()
    //    {
    //        spawnLeftGoing.gameObject.SetActive(false);
    //        spawnRightGoing.gameObject.SetActive(false);

    //        spawnLeftComing.gameObject.SetActive(true);
    //        spawnRightComing.gameObject.SetActive(true);

    //        leftGoingTarget.SetActive(false);
    //        rightGoingTarget.SetActive(false);

    //        leftComingTarget.SetActive(true);
    //        rightComingTarget.SetActive(true);

    //        StartCoroutine(trafficGenerator.ComingRightTrafficGeneration());
    //        StartCoroutine(trafficGenerator.ComingLeftTrafficGeneration());
    //    }

    //    public void SelectLocations(GameLocations gameLocation)
    //    {
    //        switch (gameLocation)
    //        {
    //            case GameLocations.NormalMap:
    //                ShowMap((int)GameLocations.NormalMap);
    //                break;

    //            case GameLocations.SnowyMap:
    //                ShowMap((int)GameLocations.SnowyMap);
    //                break;

    //            case GameLocations.RainyMap:
    //                ShowMap((int)GameLocations.RainyMap);
    //                break;

    //            case GameLocations.BeachMap:
    //                ShowMap((int)GameLocations.BeachMap);
    //                break;
    //        }
    //    }

    //    public void ShowMap(int pathIndex)
    //    {
    //        //if(pathIndex != 0) { player.wayPointsPlayer.Clear(); }

    //        for(int i = 0; i < pathManagers.Length; i++)
    //        {
    //            if(pathIndex == i)
    //            {
    //                pathManagers[i].SetActive(true);
    //                RenderSettings.skybox = materials[i];
    //                if(particleSystems[i] != null) { particleSystems[i].Play(); }

    //            }
    //            else
    //            {
    //                pathManagers[i].SetActive(false);
    //                if (particleSystems[i] != null) { particleSystems[i].Stop(); }
    //            }
    //        }
    //    } 


}
