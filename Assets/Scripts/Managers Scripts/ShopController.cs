﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ShopController : MonoBehaviour
{
    #region Singlton:ShopController

    public static ShopController Instance;

    void Awake()
    {
        if (Instance == null)
            Instance = this;

    }

    #endregion

    public GameObject bikesShop;
    public GameObject powerUpsShop;
    public GameObject skinsShop;

    [HideInInspector]
    public Shop bikesShopScript, powerUpsShopScript, skinsShopScript;
    

    private void Start()
    {
        bikesShopScript = bikesShop.GetComponent<Shop>();
        powerUpsShopScript = powerUpsShop.GetComponent<Shop>();
        skinsShopScript = skinsShop.GetComponent<Shop>();
    }

    public void AddCash(int amount)
    {
        //rewardAnimation.SetTrigger("Reward");

        /*UIManager.Instance.RewardText.text = "+ $" + amount.ToString();
        UIManager.Instance.rewardAnimation.SetTrigger("Reward");*/

        UIManager.Instance.totalMoney += amount;

        PlayerPrefs.SetFloat("Money", UIManager.Instance.totalMoney);
        UIManager.Instance.UpdateAllCoinsUIText();
        SoundManager.Instance.buySound.Play();
    }

    public void RemoveAdds()
    {
        AdsManager.Instance.adsDisabled = true;
        AdsManager.Instance.bannerScript.HideBannerAd();

        /*UIManager.Instance.RewardText.text = "ADS ARE DISABLED";
        UIManager.Instance.rewardAnimation.SetTrigger("Reward");*/

        SoundManager.Instance.buySound.Play();

        PlayerPrefs.SetInt("Ads Removed", 1);
    }
}

    
