﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.EventSystems;

//public class PlayerPosition : MonoBehaviour
//{

//    [SerializeField] Transform rightPos;
//    [SerializeField] Transform leftPos;

//    [SerializeField] GameObject brakeEffect;
//    [SerializeField] GameObject brakeLightEffect;

//    public Animator carAnimation;


//    [SerializeField] float speed = 10f;

//    [SerializeField] ParticleSystem explosionEffect;

//    public bool movingRight;
//    public bool movingLeft;

//    public Animator killingAnimator;

//    private void Start()
//    {
//        //PlayerPrefs.SetInt("tutorial", 0);
//    }

//    void Update()
//    {

//        if (GameManager.Instance.gamePlaying == true)
//        {

//#if UNITY_EDITOR

//            if (Input.GetButton("Fire1") && killingAnimator.enabled == false)
//            {
//                if (EventSystem.current.IsPointerOverGameObject())
//                    return;
//                Moveleft();
//                Debug.Log("onLeft");
//            }

//            else
//            {
//                MoveRight();
//            }

//#elif UNITY_ANDROID || UNITY_IPHONE

//            if (movingRight)
//            {

//                MoveRight();

//            }

//            else if (movingLeft)
//            {
//                Moveleft();

//            }


//            if (Input.touchCount > 0)
//            {
//                Touch touch = Input.GetTouch(0);
//                if(EventSystem.current.currentSelectedGameObject == null)
//                {

//                    if (touch.phase == TouchPhase.Began  && killingAnimator.enabled == false)
//                    {
//                        movingLeft = true;
//                        movingRight = false;
//                    }
//                    if (touch.phase == TouchPhase.Ended)
//                    {
//                        movingLeft = false;
//                        movingRight = true;
//                    }

//                }
                
//                ////if(touch.phase == TouchPhase.Ended)
//                ////{
//                ////    movingRight = true;

//                ////}

//                //GameManager.Instance.onLeft = true;
//            }

//            else
//            {
//                //   MoveRight();
//                //   GameManager.Instance.onLeft = false;
//            }
//#endif
//        }



//    }

//    void Moveleft()
//    {
//        Debug.Log("onleft");
//        GameManager.Instance.onLeft = true;

//        //UIManager.Instance.tutorial1.SetActive(false);

//        if (UIManager.Instance.t == 0)
//        {
//            Debug.Log("t2 is false");
//            UIManager.Instance.tutorial1.SetActive(false);
//            UIManager.Instance.laneTutorial.SetActive(false);
//            UIManager.Instance.tutorial2.SetActive(true);
//            GameManager.Instance.tutorialPlaying = false;
//            UIManager.Instance.t = 1;
//            PlayerPrefs.SetInt("tutorial", UIManager.Instance.t);
//        }


//        transform.position = Vector3.Lerp(this.transform.position, leftPos.position, speed * Time.deltaTime);
//        carAnimation.SetTrigger("LeftTurn");
//        //Debug.Log("onleft");

//        if (transform.position == leftPos.position)
//        {
//            movingLeft = false;
//        }

//    }

//    void MoveRight()
//    {
//        Debug.Log("onright");
//        GameManager.Instance.onLeft = false;
//        UIManager.Instance.tutorial2.SetActive(false);




//        transform.position = Vector3.Lerp(this.transform.position, rightPos.position, speed * Time.deltaTime);
//        carAnimation.SetTrigger("RightTurn");


//        StartCoroutine(BrakeEffect());

//        if (transform.position == rightPos.position)
//        {
//            movingRight = false;
//        }

//    }

//    private void OnTriggerEnter(Collider other)
//    {
//        if (other.CompareTag("NPC") || other.CompareTag("NPC2"))
//        {
//            carAnimation.SetTrigger("CarCrash");

//            StartCoroutine(PlayExplosion());
//            //SoundManager.Instance.blastSound.Play();
//            StartCoroutine(UIManager.Instance.GameoverDelayNPC());


//        }
//    }

//    IEnumerator BrakeEffect()
//    {
//        brakeLightEffect.SetActive(true);
//        brakeEffect.SetActive(true);
//        yield return new WaitForSeconds(1f);
//        brakeEffect.SetActive(false);
//        brakeLightEffect.SetActive(false);
//    }

//    //public void CarMoveSound()
//    //{
//    //    SoundManager.Instance.screechSound.Play();

//    //}

//    IEnumerator PlayExplosion()
//    {
//        explosionEffect.Play();
//        yield return new WaitForSeconds(0.25f);
//        explosionEffect.Play();
//        yield return new WaitForSeconds(1f);
//        explosionEffect.Stop();
//    }


//}
