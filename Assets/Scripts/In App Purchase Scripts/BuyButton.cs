﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyButton : MonoBehaviour
{
    public enum ItemType
    {
        Cash50,
        Cash100,
        NoAds,
        ShopItem
    }

    public ItemType itemType;
    public Text priceText;
    //public GameObject restoreBtn;

    private string defaultText;

    // Start is called before the first frame update
    void Start()
    {
        if(Application.platform != RuntimePlatform.IPhonePlayer && Application.platform != RuntimePlatform.OSXPlayer)
        {
            UIManager.Instance.restoreBtn.SetActive(false);
        }
        defaultText = priceText.text;
        StartCoroutine(LoadPriceRoutine());
    }

    public void ClickRestore()
    {
        IAPManager.Instance.RestorePurchases();
    }

    public void ClickBuy()
    {
        switch (itemType) 
        {
            case ItemType.Cash50:
                IAPManager.Instance.Buy50Cash();
                break;

            case ItemType.Cash100:
                IAPManager.Instance.Buy100Cash();
                break;
            
            case ItemType.NoAds:
                IAPManager.Instance.BuyNoAds();
                break;

            case ItemType.ShopItem:
                IAPManager.Instance.BuyShopItem();
                break;
        }
    } 

    private IEnumerator LoadPriceRoutine()
    {
        while(!IAPManager.Instance.IsInitialized())
            yield return null;

        string loadedPrice = "";

        switch(itemType)
        {
            case ItemType.Cash50:
                loadedPrice = IAPManager.Instance.GetProductPriceFromStore(IAPManager.Instance.CASH_50);
                break;

            case ItemType.Cash100:
                loadedPrice = IAPManager.Instance.GetProductPriceFromStore(IAPManager.Instance.CASH_100);
                break;

            case ItemType.NoAds:
                loadedPrice = IAPManager.Instance.GetProductPriceFromStore(IAPManager.Instance.NO_ADS);
                break;

            case ItemType.ShopItem:
                loadedPrice = IAPManager.Instance.GetProductPriceFromStore(IAPManager.Instance.SHOP_ITEM);
                break;
        }

        priceText.text = defaultText + " " + loadedPrice;
    }
}
