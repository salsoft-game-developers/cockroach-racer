﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class PolicePosition : MonoBehaviour
//{
//    public Transform rightPos;
//    public Transform leftPos;


//   // public bool onMiddle;

//    public bool movingRight;
//    public bool movingLeft;
//    public bool playerBool;

//    public bool isPlayerThere;
    
//    //public Animator crashAnimation;

//    // starting value for the Lerp
//    [SerializeField] float speed = 10f;

//    public float Timer;

//    [SerializeField] Animator policeAnimation;
//    [SerializeField] Animator PlayerAnim;
//    [SerializeField] Animator NewAnim;

//    [SerializeField] ParticleSystem SparkEffect;
//    [SerializeField] ParticleSystem explosionEffect;
    
//    [SerializeField] GameObject policeBrakeEffect;
//    [SerializeField] GameObject RedLight;
//    [SerializeField] GameObject BlueLight;

//    [SerializeField] GameObject playerHolder;
//    [SerializeField] GameObject policeHolder;
//    float slowSpeed = 5;

    

//    private void Start()
//    {
//        NewAnim.enabled = false;
//        //playerSpeed = PlayerAnim.GetComponent<PlayerMovement>();
//        //policeSpeed = this.GetComponentInParent<PoliceController>();

        
//        //onMiddle = true;
//        PoliceLights();
        
//        //StartCoroutine(SirenLights());
        
//    }

//    void Update()
//    {
//        if (!GameManager.Instance.gamePlaying)
//        {
//            return;
//        }
//        else
//        {
//            if (movingRight)
//            {
//                //Debug.Log("moving right");
//                MoveRight();
//                policeAnimation.SetTrigger("PturnRight");
//            }

//            else if (movingLeft)
//            {
//                Moveleft();
//                policeAnimation.SetTrigger("PturnLeft");
//            }

//            if (playerBool)
//            {
//                //Debug.Log("time : " + Timer);
//                Timer -= 1 * Time.deltaTime;
//                //Debug.Log("Bool" + Timer + " , " + testbool);
                
//                if (Timer <= 0 && testbool && isPlayerThere)
//                {
//                    NewAnim.enabled = true;
//                    Debug.Log("animator enabled" + Timer);
//                    testbool = false;
//                    playerBool = false;
//                    //movingLeft = true;
                    
//                    ////StartCoroutine(ClashEffect());

//                    //policeAnimation.SetTrigger("CrashingVan");
//                    //PlayerAnim.SetTrigger("CrashingEffect");

//                    NewAnim.SetTrigger("Crash");
//                    StartCoroutine(PlayExplosion());
//                    StartCoroutine(UIManager.Instance.GameoverDelayPolice());

//                }

//                else
//                {
//                    SparkEffect.Stop();
//                }
//            }
//        }
        
//    }

//    public bool testbool = true;
    
//    void Moveleft()
//    {
//        movingRight = false;
//        transform.position = Vector3.Lerp(this.transform.position, leftPos.position, speed * Time.deltaTime);
        
//        //policeAnimation.SetTrigger("PturnLeft");
//        //policeAnimation.SetTrigger("PturnRight");

//        StartCoroutine(PoliceBrakeEffect());

//        if (transform.position == leftPos.position)
//        {
//            movingLeft = false;
//        }
//    }

//    void MoveRight()
//    {
//        movingLeft = false;
//        transform.position = Vector3.Lerp(this.transform.position, rightPos.position, speed * Time.deltaTime);
//        //policeAnimation.SetTrigger("PturnRight");
//        //transform.position = rightPos.position;
//        if (transform.position == rightPos.position)
//        {
//            movingRight = false;
//        }

//    }

//    private void OnTriggerEnter(Collider other)
//    {
//        //if(onMiddle == true)
//        //{

//        if (other.CompareTag("NPC"))
//        {
//            movingRight = true;

//            if (other.gameObject.GetComponent<TrafficGoing>() == null)
//            {
//                other.gameObject.GetComponent<TrafficComing>().speed -= slowSpeed;
//            }
//            else
//            {
//                other.gameObject.GetComponent<TrafficGoing>().speed -= slowSpeed;
//            }
            
            
//            //Debug.Log("Inside NPC collider");

//            //MoveRight();
//            //onMiddle = false;
//        }

//        //if (other.CompareTag("NPC2"))
//        //{
//        //    policeHolder.GetComponent<PoliceController>().rightSpeed = policeHolder.GetComponent<PoliceController>().newRightSpeed;
//        //}

//        if (other.gameObject.tag == "Player")
//        {
//            isPlayerThere = true;

//            Timer = 2.5f;
//            //Debug.Log("Inside player collider");
//            movingRight = true;
           
//            //Debug.Log("Speed normal " + policeHolder.GetComponent<PoliceController>().rightSpeed);
            
//        }

//    }


//    private void OnTriggerStay(Collider other)
//    {
//        if (other.gameObject.tag == "Player")
//        {
//            // Debug.Log("Inside player collider");
//            playerBool = true;
//            isPlayerThere = true;
//            //policeHolder.GetComponent<PoliceController>().rightSpeed = playerHolder.GetComponent<PlayerMovement>().rightSpeed;
//        }
        
//    }

//    private void OnTriggerExit(Collider other)
//    {
//        if (other.gameObject.tag == "NPC" && isPlayerThere == false)
//        {
//            NewAnim.enabled = false;

//            //Debug.Log("NPC exit");
//            //movingRight = false;
//            movingLeft = true;
//            if(other.gameObject.GetComponent<TrafficGoing>() == null)
//            {
//                other.gameObject.GetComponent<TrafficComing>().speed += slowSpeed;
//            }
//            else
//            {
//                other.gameObject.GetComponent<TrafficGoing>().speed += slowSpeed;
//            }
           
//            //  policeHolder.GetComponent<PoliceController>().rightSpeed = policeHolder.GetComponent<PoliceController>().newRightSpeed;
//        }

//        if (other.gameObject.tag == "Player")
//        {
//           // Debug.Log("player exit");
//           // Debug.Log("new Speed " + policeHolder.GetComponent<PoliceController>().newRightSpeed);

//            //policeHolder.GetComponent<PoliceController>().rightSpeed = policeHolder.GetComponent<PoliceController>().newRightSpeed;
            
//            isPlayerThere = false;
//            playerBool = false;
            
//            //movingRight = false;
//            //movingLeft = true;
          
//        }
//    }


//    public IEnumerator ClashEffect() 
//    {
//        policeAnimation.SetTrigger("CrashingVan");
//        yield return new WaitForSeconds(.3f);
//        PlayerAnim.SetTrigger("CrashingEffect");

//    }


//    public void PoliceLights()
//    {

//        RedLight.SetActive(true);
//        BlueLight.SetActive(false);

//        StartCoroutine(SirenLights());
//    }

//    IEnumerator SirenLights()
//    {

//        yield return new WaitForSeconds(.5f);

//        RedLight.SetActive(false);
//        BlueLight.SetActive(true);

//        yield return new WaitForSeconds(.5f);

//        PoliceLights();

//    }

//    IEnumerator PoliceBrakeEffect()
//    {
//        policeBrakeEffect.SetActive(true);
//        yield return new WaitForSeconds(1f);
//        policeBrakeEffect.SetActive(false);
//    }
//    IEnumerator PlayExplosion()
//    {
//        yield return new WaitForSeconds(1.25f);
//        SparkEffect.Play();
//        explosionEffect.Play();
//        yield return new WaitForSeconds(0.25f);
//        explosionEffect.Play();
//        yield return new WaitForSeconds(1f);
//        explosionEffect.Stop();
//    }
//}
